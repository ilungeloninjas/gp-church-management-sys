﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class ChurchRequestRepository: RepositoryBase<ChurchRequest>, IChurchRequestRepository
        {
        public ChurchRequestRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
            {
            }           
        }
    public interface IChurchRequestRepository : IRepository<ChurchRequest>
    {
    }
}