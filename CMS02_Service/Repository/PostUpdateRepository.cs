﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class PostUpdateRepository : RepositoryBase<PostUpdate>, IPostUpdateRepository
    {
        public PostUpdateRepository(IDatabaseFactory databaseFactory): base(databaseFactory)
        {
        }
    }
    public interface IPostUpdateRepository : IRepository<PostUpdate>
    {
    }
}
