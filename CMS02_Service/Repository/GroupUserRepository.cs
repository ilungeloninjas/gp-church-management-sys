﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class ChurchUserRepository : RepositoryBase<ChurchUser>, IChurchUserRepository
    {
        public ChurchUserRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
        //public IEnumerable<User> SearchUserForGroup(string searchString, int groupId)
        //{
            
        //}
    }
    public interface IChurchUserRepository : IRepository<ChurchUser>
    {
        //IEnumerable<User> SearchUserForGroup(string searchString, int groupId);
    }
}
