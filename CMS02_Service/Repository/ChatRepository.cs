﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class ChatRepository : RepositoryBase<Chat>, IChatRepository
        {
        public ChatRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
            {
            }
        }
    public interface IChatRepository : IRepository<Chat>
    {    
    }
}