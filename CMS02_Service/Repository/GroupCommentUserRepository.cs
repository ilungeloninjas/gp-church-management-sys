﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class ChurchCommentUserRepository : RepositoryBase<ChurchCommentUser>, IChurchCommentUserRepository
    {
        public ChurchCommentUserRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
        
    }
    public interface IChurchCommentUserRepository : IRepository<ChurchCommentUser>
    {
        
    }
}
