﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class ChurchRepository : RepositoryBase<Church>, IChurchRepository
        {
        public ChurchRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
            {
            }           
        }
    public interface IChurchRepository : IRepository<Church>
    {
    }
   
}
