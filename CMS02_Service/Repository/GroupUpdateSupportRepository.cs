﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    class ChurchUpdateSupportRepository : RepositoryBase<ChurchUpdateSupport>, IChurchUpdateSupportRepository
    {
        public ChurchUpdateSupportRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface IChurchUpdateSupportRepository : IRepository<ChurchUpdateSupport>
    {
    }
}
