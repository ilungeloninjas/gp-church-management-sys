﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class ChurchUpdateUserRepository : RepositoryBase<ChurchUpdateUser>, IChurchUpdateUserRepository
    {
        public ChurchUpdateUserRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
        
    }
    public interface IChurchUpdateUserRepository : IRepository<ChurchUpdateUser>
    {
       
    }
}
