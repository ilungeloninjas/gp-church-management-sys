﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class AppointmentRepository : RepositoryBase<Appointment>, IAppointmentRepository
    {
        public AppointmentRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
            {
            }   
    }
    public interface IAppointmentRepository : IRepository<Appointment>
    {

    }
}
