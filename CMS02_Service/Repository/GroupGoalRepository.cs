﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class ChurchGoalRepository : RepositoryBase<ChurchGoal>, IChurchGoalRepository
    {
        public ChurchGoalRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface IChurchGoalRepository : IRepository<ChurchGoal>
    {
    }
}
