﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class AttendEventRepository : RepositoryBase<AttendEvent>, IAttendEventRepository
    {
        public AttendEventRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }
        
    }
    public interface IAttendEventRepository : IRepository<AttendEvent>
    {
        
    }
}
