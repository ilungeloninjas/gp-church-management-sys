﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class ChurchUpdateRepository : RepositoryBase<ChurchUpdate>, IChurchUpdateRepository
    {
        public ChurchUpdateRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface IChurchUpdateRepository : IRepository<ChurchUpdate>
    {
    }
}