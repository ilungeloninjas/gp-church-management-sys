﻿using System.Linq;
using CMS02_Data;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class AppointmenRepository
    {
        GodProsperityContext Db = new GodProsperityContext();
        public IQueryable<Appointment> Appointments
        {
            get { return Db.Appointments; }
        }

        public bool CreateAppointment(Appointment instance)
        {
            if (instance.id == 0)
            {
                Db.Appointments.Add(instance);
                Db.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateAppointment(Appointment instance)
        {
            var cache = Db.Appointments.FirstOrDefault(o => o.id == instance.id);
            if (cache != null)
            {
                Db.Entry(cache).CurrentValues.SetValues(instance);
                Db.SaveChanges();
                return true;
            }
            return false;
        }

        public bool RemoveAppointment(int id)
        {
            var instance = Db.Appointments.FirstOrDefault(o => o.id == id);
            if (instance != null)
            {
                Db.Appointments.Remove(instance);
                Db.SaveChanges();
                return true;
            }
            return false;
        }
    
    }
}
