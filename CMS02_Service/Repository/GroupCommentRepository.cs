﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;

namespace CMS02_Service.Repository
{
    public class ChurchCommentRepository : RepositoryBase<ChurchComment>, IChurchCommentRepository
    {
        public ChurchCommentRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface IChurchCommentRepository : IRepository<ChurchComment>
    {
    }
}