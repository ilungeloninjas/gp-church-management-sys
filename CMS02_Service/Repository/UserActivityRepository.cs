﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS02_Service.Repository
{
    public class UserActivityRepository : RepositoryBase<User_Activity>, IUserActivityRepository
    {
        public UserActivityRepository(IDatabaseFactory databaseFactory): base(databaseFactory)
        {

        } 
    }
    public interface IUserActivityRepository : IRepository<User_Activity>
    {
    }
}
