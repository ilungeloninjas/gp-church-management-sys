﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Web.Mvc;
//using AutoMapper;
//using CMS02.Controllers;
//using CMS02.ViewModels;
//using CMS02_BusinessLogic;
//using CMS02_Data.Infrastructure;
//using CMS02_Models.Models;
//using CMS02_Service.Repository;
//using Moq;
//using NUnit.Framework;

//namespace CMS02.Tests.Controllers
//{
//    [TestFixture]
//    public class SearchControllerTest
//    {
//        Mock<IGoalRepository> _goalRepository;
//        Mock<IUserRepository> _userRepository;
//        Mock<IChurchRepository> _groupRepository;
//        Mock<IFollowUserRepository> _followUserRepository;
//        Mock<IChurchUserRepository> _groupUserRepository;
//        Mock<IUserProfileRepository> _userProfileRepository;

//        IGoalBusiness _goalBusiness;
//        IChurchBusiness _groupBusiness;
//        IUserBusiness _userBusiness;

//        Mock<IUnitOfWork> _unitOfWork;

//        [SetUp]
//        public void SetUp()
//        {
//            _goalRepository = new Mock<IGoalRepository>();
//            _userRepository = new Mock<IUserRepository>();
//            _groupRepository = new Mock<IChurchRepository>();
//            _followUserRepository = new Mock<IFollowUserRepository>();
//            _groupUserRepository = new Mock<IChurchUserRepository>();
//            _userProfileRepository = new Mock<IUserProfileRepository>();
//            _unitOfWork = new Mock<IUnitOfWork>();

//            _goalBusiness = new GoalBusiness(_goalRepository.Object, _followUserRepository.Object, _unitOfWork.Object);
//            _groupBusiness = new ChurchBusiness(_groupRepository.Object, _followUserRepository.Object, _groupUserRepository.Object, _unitOfWork.Object);
//            _userBusiness = new UserBusiness(_userRepository.Object, _unitOfWork.Object, _userProfileRepository.Object);
//        }

//        [TearDown]
//        public void TearDown()
//        {
//        }

//        [Test]
//        public void Search_All()
//        {
//            IEnumerable<Goal> fakegoal = new List<Goal> {
//            new Goal{ GoalStatusId =1, GoalName ="a",GoalType = false},
//            new Goal{ GoalStatusId =1, GoalName ="abc",GoalType = false},
//            new Goal{ GoalStatusId =1, GoalName ="aedg",GoalType = false},

           
//          }.AsEnumerable();
//            _goalRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Goal, bool>>>())).Returns(fakegoal);

//            IEnumerable<ApplicationUser> fakeUser = new List<ApplicationUser> {            
//              new ApplicationUser{Activated=true,Email="user1@foo.com",FirstName="user1",LastName="user1",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user2@foo.com",FirstName="user2",LastName="user2",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user3@foo.com",FirstName="user3",LastName="user3",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user4@foo.com",FirstName="user4",LastName="user4",RoleId=0}
//          }.AsEnumerable();
//            _userRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(fakeUser);

//            IEnumerable<Church> fakeGroups = new List<Church> {
//            new Church { ChurchName = "Test1", Description="Test1Desc"},
//            new Church {  ChurchName= "Test2", Description="Test2Desc"},
//            new Church { ChurchName = "Test3", Description="Test3Desc"}
//          }.AsEnumerable();
//            _groupRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Group, bool>>>())).Returns(fakeGroups);

//            Mapper.CreateMap<Goal, GoalViewModel>();
//            Mapper.CreateMap<Group, GroupViewModel>();
//            SearchController controller = new SearchController(_goalBusiness, _userBusiness, _groupBusiness);
//            ViewResult result = controller.SearchAll("a") as ViewResult;
//            Assert.IsNotNull(result, "View Result is null");
//            Assert.IsInstanceOf(typeof(SearchViewModel),
//            result.ViewData.Model, "Wrong View Model");



//        }



//    }
//}
