﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Mail;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using CMS02.Controllers;
using CMS02.Mailers;
using CMS02.Tests.Helpers;
using CMS02.ViewModels;
using CMS02_BusinessLogic;
using CMS02_BusinessLogic.Authentication;
using CMS02_BusinessLogic.Models;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;
using Moq;
using Mvc.Mailer;
using NUnit.Framework;

namespace CMS02.Tests.Controllers
{
    [TestFixture]
    public class GoalControllerTest
    {
        Mock<IGoalRepository> _goalRepository;
        Mock<IFollowUserRepository> _followuserRepository;
        Mock<ISupportRepository> _supportRepository;
        Mock<IGoalStatusRepository> _goalStatusRepository;
        Mock<IFocusRepository> _focusRepository;
        Mock<IMetricRepository> _metricRepository;
        Mock<IUpdateRepository> _updateRepository;
        Mock<IUserRepository> _userRepository;
        Mock<ISupportRepository> _supportrepository;
        Mock<ISupportInvitationRepository> _supportInvitationrepository;
        Mock<ICommentRepository> _commentRepository;
        Mock<ICommentUserRepository> _commentUserRepository;
        Mock<ISecurityTokenRepository> _securityTokenrepository;
        Mock<IUserProfileRepository> _userProfileRepository;
        Mock<IUpdateSupportRepository> _updateSupportRepository;


        IGoalBusiness _goalBusiness;
        IFollowUserBusiness _followUserBusiness;
        IMetricBusiness _metricBusiness;
        IFocusBusiness _focusBusiness;
        IUpdateBusiness _updateBusiness;
        ISupportBusiness _supportBusiness;
        ICommentBusiness _commentBusiness;
        ISecurityTokenBusiness _securityTokenBusiness;
        IGoalStatusBusiness _goalStatusBusiness;
        IUserBusiness _userBusiness;
        ICommentUserBusiness _commentUserBusiness;
        ISupportInvitationBusiness _supportInvitationBusiness;
        IUserProfileBusiness _userProfileBusiness;
        IUpdateSupportBusiness _updateSupportBusiness;

        Mock<IUnitOfWork> _unitOfWork;
        Mock<IUserMailer> _userMailer;
        Mock<UserMailer> _userMailerMock;
        Mock<MailerBase> _mailerBase;

        Mock<ControllerContext> _controllerContext;
        Mock<IIdentity> _identity;
        Mock<IPrincipal> _principal;
        // Mock<HttpContext> httpContext;
        Mock<HttpContextBase> _contextBase;
        Mock<HttpRequestBase> _httpRequest;
        Mock<HttpResponseBase> _httpResponse;
        Mock<GenericPrincipal> _genericPrincipal;

        //Mock<>
        [SetUp]
        public void SetUp()
        {
            _goalRepository = new Mock<IGoalRepository>();
            _followuserRepository = new Mock<IFollowUserRepository>();
            _supportRepository = new Mock<ISupportRepository>();
            _goalStatusRepository = new Mock<IGoalStatusRepository>();
            _focusRepository = new Mock<IFocusRepository>();
            _metricRepository = new Mock<IMetricRepository>();
            _updateRepository = new Mock<IUpdateRepository>();
            _userRepository = new Mock<IUserRepository>();
            _supportrepository = new Mock<ISupportRepository>();
            _supportInvitationrepository = new Mock<ISupportInvitationRepository>();
            _commentRepository = new Mock<ICommentRepository>();
            _commentUserRepository = new Mock<ICommentUserRepository>();
            _securityTokenrepository = new Mock<ISecurityTokenRepository>();
            _userProfileRepository = new Mock<IUserProfileRepository>();
            _updateSupportRepository = new Mock<IUpdateSupportRepository>();

            _userMailer = new Mock<IUserMailer>();
            _userMailerMock = new Mock<UserMailer>();
            _mailerBase = new Mock<MailerBase>();
            _unitOfWork = new Mock<IUnitOfWork>();

            _goalBusiness = new GoalBusiness(_goalRepository.Object, _followuserRepository.Object, _unitOfWork.Object);
            _supportBusiness = new SupportBusiness(_supportRepository.Object, _followuserRepository.Object, _unitOfWork.Object);
            _goalStatusBusiness = new GoalStatusBusiness(_goalStatusRepository.Object, _unitOfWork.Object);
            _focusBusiness = new FocusBusiness(_focusRepository.Object, _unitOfWork.Object);
            _metricBusiness = new MetricBusiness(_metricRepository.Object, _unitOfWork.Object);
            _updateBusiness = new UpdateBusiness(_updateRepository.Object, _goalRepository.Object, _unitOfWork.Object, _followuserRepository.Object);
            _userBusiness = new UserBusiness(_userRepository.Object, _unitOfWork.Object, _userProfileRepository.Object);
            _supportBusiness = new SupportBusiness(_supportrepository.Object, _followuserRepository.Object, _unitOfWork.Object);
            _supportInvitationBusiness = new SupportInvitationBusiness(_supportInvitationrepository.Object, _unitOfWork.Object);
            _commentBusiness = new CommentBusiness(_commentRepository.Object, _commentUserRepository.Object, _unitOfWork.Object, _followuserRepository.Object);
            _commentUserBusiness = new CommentUserBusiness(_commentUserRepository.Object, _userRepository.Object, _unitOfWork.Object);
            _securityTokenBusiness = new SecurityTokenBusiness(_securityTokenrepository.Object, _unitOfWork.Object);
            _userProfileBusiness = new UserProfileBusiness(_userProfileRepository.Object, _unitOfWork.Object);
            _updateSupportBusiness = new UpdateSupportBusiness(_updateSupportRepository.Object, _unitOfWork.Object);

            MailerBase.IsTestModeEnabled = true;
            _userMailerMock.CallBase = true;

            _controllerContext = new Mock<ControllerContext>();
            _contextBase = new Mock<HttpContextBase>();
            _httpRequest = new Mock<HttpRequestBase>();
            _httpResponse = new Mock<HttpResponseBase>();
            _genericPrincipal = new Mock<GenericPrincipal>();


            _identity = new Mock<IIdentity>();
            _principal = new Mock<IPrincipal>();
        }
        [TearDown]
        public void TearDown()
        {
            TestSmtpClient.SentMails.Clear();
        }



        [Test]
        public void Index()
        {

            MemoryUser user = new MemoryUser("adarsh");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };

            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());


            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            Goal goal = new Goal()
            {
                GoalId = 1,
                GoalName = "t",
                GoalStatusId = 1,
                Desc = "x",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(1)

            };
            _goalRepository.Setup(x => x.GetById(1)).Returns(goal);
            IEnumerable<GoalStatus> fake = new List<GoalStatus> {
            new GoalStatus { GoalStatusId =1, GoalStatusType ="Inprogress"},
            new GoalStatus { GoalStatusId =2, GoalStatusType ="OnHold"},
         
          }.AsEnumerable();
            _goalStatusRepository.Setup(x => x.GetAll()).Returns(fake);

            Mapper.CreateMap<Goal, GoalViewModel>();

            ViewResult result = controller.Index(1) as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(typeof(GoalViewModel), result.ViewData.Model, "WrongType");
            var data = result.ViewData.Model as GoalViewModel;
            Assert.AreEqual("t", data.GoalName);
        }

        [Test]
        public void MyGoal_Test()
        {
            MemoryUser user = new MemoryUser("adarsh");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());

            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");

            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);
            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());
            var formsAuthentication = new DefaultFormsAuthentication();
            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);
            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            IEnumerable<Goal> fake = new List<Goal> 
            {
            new Goal { GoalId=1,GoalName="Test1"},
            new Goal { GoalId=2,GoalName="Test2"},
            new Goal { GoalId=3,GoalName="Test3"},
            new Goal { GoalId=4,GoalName="Test4"},           
         
          }.AsEnumerable();
            _goalRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Goal, bool>>>())).Returns(fake);


            _contextBase.Setup(x => x.User.Identity).Returns(_identity.Object);
            Mapper.CreateMap<Goal, GoalViewModel>();
            ViewResult result = controller.MyGoal() as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(typeof(IEnumerable<GoalViewModel>), result.ViewData.Model, "Wrong model");
            var data = result.ViewData.Model as IEnumerable<GoalViewModel>;
            Assert.AreEqual(4, data.Count(), "not matching");
        }


        [Test]
        public void Create_Goal_Get_ReturnsView()
        {

            IEnumerable<Focus> fakeFocus = new List<Focus> 
            {
            new Focus { FocusId = 1, FocusName="Test1",ChurchId = 1},
             new Focus { FocusId = 2, FocusName="Test2",ChurchId = 1},
            new Focus { FocusId = 3, FocusName="Test3",ChurchId = 1}
          }.AsEnumerable();
            _focusRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Focus, bool>>>())).Returns(fakeFocus);

            IEnumerable<Metric> fakeMatrices = new List<Metric> 
            {
                new Metric{MetricId=1, Type="Test1"},
                new Metric{MetricId=2,Type="Test2"},
                new Metric{MetricId=3,Type="Test3"}
            }.AsEnumerable();

            _metricRepository.Setup(x => x.GetAll()).Returns(fakeMatrices);
            GoalFormModel goal = new GoalFormModel();
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            PartialViewResult result = controller.Create() as PartialViewResult;
            Assert.IsNotNull(result, "View Result is null");
            Assert.IsInstanceOf(typeof(GoalFormModel),
                result.ViewData.Model, "Wrong View Model");


        }
        [Test]
        public void Create_Goal()
        {
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            // Act
            Mapper.CreateMap<GoalFormModel, Goal>();


            GoalFormModel goal = new GoalFormModel()
            {
                GoalName = "t",

                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(1),
                Desc = "t",

                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",


            };
            var result = (RedirectToRouteResult)controller.Create(goal);
            Assert.AreEqual("Index", result.RouteValues["action"]);


        }
        [Test]
        public void Edit_Goal_Get_View()
        {

            Goal goal = new Goal()
            {
                GoalId = 1,
                GoalName = "t",
                Desc = "t",
                GoalStatusId = 1,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(1),
            };
            _goalRepository.Setup(x => x.GetById(1)).Returns(goal);
            IEnumerable<Focus> fakeFocus = new List<Focus> 
            {
            new Focus { FocusId = 1, FocusName="Test1",ChurchId = 1},
             new Focus { FocusId = 2, FocusName="Test2",ChurchId = 1},
            new Focus { FocusId = 3, FocusName="Test3",ChurchId = 1}
          }.AsEnumerable();
            _focusRepository.Setup(x => x.GetMany(p => p.ChurchId.Equals(1))).Returns(fakeFocus);

            IEnumerable<Metric> fakeMatrices = new List<Metric> 
            {
                new Metric{MetricId=1, Type="Test1"},
                new Metric{MetricId=2,Type="Test2"},
                new Metric{MetricId=3,Type="Test3"}
            }.AsEnumerable();

            _metricRepository.Setup(x => x.GetAll()).Returns(fakeMatrices);

            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            Mapper.CreateMap<Goal, GoalFormModel>();
            ViewResult result = controller.Edit(1) as ViewResult;
            Assert.IsNotNull(result, "View Result is null");
            Assert.IsInstanceOf(typeof(GoalFormModel),
                result.ViewData.Model, "Wrong View Model");
            var data = result.ViewData.Model as GoalFormModel;
            Assert.AreEqual("t", data.Desc);
        }

        [Test]
        public void Edit_Goal_Post()
        {

            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            Mapper.CreateMap<GoalFormModel, Goal>();


            GoalFormModel goal = new GoalFormModel()
            {
                GoalName = "t",
                GoalId = 1,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(1),
                Desc = "t",
                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"

            };
            var result = (RedirectToRouteResult)controller.Edit(goal);
            Assert.AreEqual("Index", result.RouteValues["action"]);



        }

        [Test]
        public void Goal_Status_Post_Test()
        {
            GoalStatus status = new GoalStatus()
            {
                GoalStatusId = 1,
                GoalStatusType = "InProgress"
            };
            Goal goal = new Goal()
            {
                GoalId = 1,
                GoalStatus = status,
                GoalStatusId = 1
            };
            _goalRepository.Setup(x => x.GetById(1)).Returns(goal);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            string result = controller.GoalStatus(1, 1) as string;
            Assert.AreEqual("InProgress", result);
        }

        [Test]
        public void Delete_Goal_Get_ReturnsView()
        {


            Goal fake = new Goal()
            {
                GoalId = 1,
                GoalName = "test",
                Desc = "test",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(1),
                GoalStatusId = 1,


            };


            _goalRepository.Setup(x => x.GetById(1)).Returns(fake);

            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            ViewResult result = controller.Delete(1) as ViewResult;
            Assert.IsNotNull(result, "View Result is null");
            Assert.IsInstanceOf(typeof(Goal),
                 result.ViewData.Model, "Wrong View Model");
            var group = result.ViewData.Model as Goal;
            Assert.AreEqual("test", group.Desc, "Got wrong Focus Description");

        }

        [Test]
        public void Delete_Goal_Post()
        {

            Goal goal = new Goal()
            {
                GoalId = 1,
                GoalName = "t",
                Desc = "t"

            };
            _goalRepository.Setup(x => x.GetById(1)).Returns(goal);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            var result = controller.DeleteConfirmed(1) as RedirectToRouteResult;
            Assert.AreEqual("Index", result.RouteValues["action"]);

        }

        [Test]
        public void MyGoals_test()
        {
            MemoryUser user = new MemoryUser("adarsh");
            ApplicationUser applicationUser = GetApplicationUser();

            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());



            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            IEnumerable<Goal> fake = new List<Goal> 
            {
            new Goal { GoalId=1,GoalName="Test1",UserId ="402bd590-fdc7-49ad-9728-40efbfe512ec",},
            new Goal { GoalId=2,GoalName="Test2",UserId ="402bd590-fdc7-49ad-9728-40efbfe512ec",},
            new Goal { GoalId=3,GoalName="Test3",UserId ="402bd590-fdc7-49ad-9728-40efbfe512ec",},
            new Goal { GoalId=4,GoalName="Test4",UserId ="402bd590-fdc7-49ad-9728-40efbfe512ec",}          
         
          }.AsEnumerable();
            _goalRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Goal, bool>>>())).Returns(fake);

            PartialViewResult result = controller.MyGoals() as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(IEnumerable<Goal>),
                result.ViewData.Model, "Wrong View Model");


        }
        [Test]
        public void Goals_Following_Test()
        {
            MemoryUser user = new MemoryUser("adarsh");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());




            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);


            PartialViewResult result = controller.GoalsFollowing() as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(IEnumerable<Goal>), result.ViewData.Model, "Wrong View Model");
        }

        [Test]
        public void Display_Updates_Test()
        {
            MemoryUser user = new MemoryUser("adarsh");
            ApplicationUser applicationUser = GetApplicationUser();

            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());



            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            Metric fakeMetric = new Metric()
            {
                MetricId = 1,
                Type = "%"
            };
            Goal goal = new Goal()
            {
                Metric = fakeMetric,
                Target = 100
            };
            _goalRepository.Setup(x => x.GetById(2)).Returns(goal);


            IEnumerable<Update> updt = new List<Update> {            
            new Update { UpdateId =1, Updatemsg = "t1", GoalId = 1},
             new Update { UpdateId =2, Updatemsg = "t2", GoalId=2 },
              new Update { UpdateId =3, Updatemsg = "t3",GoalId=2},
            
          }.AsEnumerable();
            _updateRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Update, bool>>>())).Returns(updt);
            Mapper.CreateMap<Update, UpdateViewModel>();
            //GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, SupportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            PartialViewResult rslt = controller.DisplayUpdates(2) as PartialViewResult;
            Assert.IsNotNull(rslt);
            Assert.IsInstanceOf(typeof(UpdateListViewModel),
           rslt.ViewData.Model, "Wrong View Model");


        }
        [Test]
        public void Supporters_List_Test()
        {
            IEnumerable<ApplicationUser> fakeUser = new List<ApplicationUser> {            
              new ApplicationUser{Activated=true,Email="user1@foo.com",FirstName="user1",LastName="user1",RoleId=0},
              new ApplicationUser{Activated=true,Email="user2@foo.com",FirstName="user2",LastName="user2",RoleId=0},
              new ApplicationUser{Activated=true,Email="user3@foo.com",FirstName="user3",LastName="user3",RoleId=0},
              new ApplicationUser{Activated=true,Email="user4@foo.com",FirstName="user4",LastName="user4",RoleId=0}
          }.AsEnumerable();
            _userRepository.Setup(x => x.GetAll()).Returns(fakeUser);
            IEnumerable<Support> fake = new List<Support> {            
            new Support { SupportId =1, GoalId = 1, UserId ="402bd590-fdc7-49ad-9728-40efbfe512ec"},
            new Support { SupportId =2, GoalId = 1, UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"},
            new Support { SupportId =3, GoalId = 1, UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"},
            new Support { SupportId =4, GoalId = 1, UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"},
          
          }.AsEnumerable();
            _supportRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Support, bool>>>())).Returns(fake);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            ViewResult result = controller.Supporters(1) as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(GoalSupporterViewModel), result.ViewData.Model, "Wrong View Model");

        }
        [Test]
        public void Save_Update_Post()
        {
            MemoryUser user = new MemoryUser("adarsh");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());



            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            //GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, SupportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            Mapper.CreateMap<UpdateFormModel, Update>();
            Mapper.CreateMap<Update, UpdateViewModel>();

            Metric fakeMetric = new Metric()
            {
                MetricId = 1,
                Type = "%"
            };
            Goal goal = new Goal()
            {
                Metric = fakeMetric,
                Target = 100,
                GoalId = 1
            };
            _goalRepository.Setup(x => x.GetById(1)).Returns(goal);


            IEnumerable<Update> updt = new List<Update> {            
            new Update { UpdateId =1, Updatemsg = "t1",GoalId =1,Status=5},
             new Update { UpdateId =2, Updatemsg = "t2",GoalId =1,Status=6},
              new Update { UpdateId =3, Updatemsg = "t3",GoalId =2,Status=2},
            
          }.AsEnumerable();

            _updateRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Update, bool>>>())).Returns(updt);
            UpdateFormModel mock = new UpdateFormModel();
            mock.Updatemsg = "mock";
            mock.GoalId = 1;
            mock.Status=9;
            PartialViewResult result = controller.SaveUpdate(mock) as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(UpdateListViewModel), result.ViewData.Model, "Wrong View Model");
        }
        [Test]
        public void Save_Update_Update_Mandatory_Test()
        {
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            // The MVC pipeline doesn't run, so binding and validation don't run. 
            controller.ModelState.AddModelError("", "mock error message");
            UpdateFormModel update = new UpdateFormModel();
            update.Updatemsg = string.Empty;
            var result = controller.SaveUpdate(update) as RedirectToRouteResult;

            Assert.IsNull(result);
        }

        [Test]
        public void Invite_User()
        {

            MemoryUser user = new MemoryUser("adarsh");
            ApplicationUser applicationUser = GetApplicationUser();

            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = user.UserName,
                RoleName = Enum.GetName(typeof(UserRoles), 0)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());

            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);

            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);



            var userId = "402bd590-fdc7-49ad-9728-40efbfe512ec";
            var id = 1;
            PartialViewResult rslt = controller.InviteUser(id, userId) as PartialViewResult;
            Assert.IsNotNull(rslt);
            Assert.IsInstanceOf(typeof(ApplicationUser),
              rslt.ViewData.Model, "Wrong View Model");
            var userView = rslt.ViewData.Model as ApplicationUser;
            Assert.AreEqual("adarsh@foo.com", userView.Email);

        }

        [Test]
        public void DisplayComments()
        {
            IEnumerable<Comment> cmnt = new List<Comment> {            

            new Comment { CommentId =1, UpdateId = 1,CommentText="x"},
            new Comment { CommentId =2, UpdateId = 1,CommentText="y"},
            new Comment { CommentId =3, UpdateId = 1,CommentText="z"},
             
            
          }.AsEnumerable();

            _commentRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Comment, bool>>>())).Returns(cmnt);
            CommentUser cmtuser = new CommentUser()
            {
                CommentId = 1,
                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
                CommentUserId = 1
            };
            _commentUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<CommentUser, bool>>>())).Returns(cmtuser);
            ApplicationUser applicationUser = GetApplicationUser();
            _userRepository.Setup(x => x.GetById("402bd590-fdc7-49ad-9728-40efbfe512ec")).Returns(applicationUser);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            Mapper.CreateMap<Comment, CommentsViewModel>();
            PartialViewResult rslt = controller.DisplayComments(1) as PartialViewResult;
            Assert.IsNotNull(rslt, "View Result is null");
            Assert.IsInstanceOf(typeof(IEnumerable<CommentsViewModel>),
             rslt.ViewData.Model, "Wrong View Model");
            var cmntsView = rslt.ViewData.Model as IEnumerable<CommentsViewModel>;
            Assert.AreEqual(3, cmntsView.Count(), "Got wrong number of Comments");
        }
        [Test]
        public void SaveComment()
        {
            MemoryUser user = new MemoryUser("adarsh");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());



            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");

            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);

            // Act          
            CommentFormModel cmnt = new CommentFormModel();
            Mapper.CreateMap<CommentFormModel, Comment>();
            cmnt.CommentText = "Mock";
            var result = controller.SaveComment(cmnt) as RedirectToRouteResult;
            // Assert 
            Assert.AreEqual("DisplayComments", result.RouteValues["action"]);
        }

        [Test]
        public void Display_Comment_Count_Test()
        {
            IEnumerable<Comment> cmnt = new List<Comment> {            

            new Comment { CommentId =1, UpdateId = 1,CommentText="x"},
            new Comment { CommentId =2, UpdateId = 1,CommentText="y"},
            new Comment { CommentId =3, UpdateId = 1,CommentText="z"},
             
            
          }.AsEnumerable();

            _commentRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Comment, bool>>>())).Returns(cmnt);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            JsonResult count = controller.DisplayCommentCount(1) as JsonResult;
            Assert.IsNotNull(count);
            Assert.AreEqual(3, count.Data);

        }

        [Test]
        public void SearchGoalSearch_User_Which_Support_The_Goal()
        {
            MemoryUser user = new MemoryUser("adarsh");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());



            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            IEnumerable<ApplicationUser> fakeUser = new List<ApplicationUser> {            
          new ApplicationUser{Activated=true,Email="user1@foo.com",FirstName="user1",LastName="user1",RoleId=0},
              new ApplicationUser{Activated=true,Email="user2@foo.com",FirstName="user2",LastName="user2",RoleId=0},
              new ApplicationUser{Activated=true,Email="user3@foo.com",FirstName="user3",LastName="user3",RoleId=0},
              new ApplicationUser{Activated=true,Email="user4@foo.com",FirstName="user4",LastName="user4",RoleId=0}
          }.AsEnumerable();
            _userRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(fakeUser);


            var searchString = "e";
            JsonResult result = controller.SearchUser(searchString, 1);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(JsonResult), result);

        }

        [Test]
        public void Create_Support_Test()
        {
            MemoryUser user = new MemoryUser("adarsh");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());



            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");

            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);

            //Act
            var rslt = controller.SupportGoalNow(1) as RedirectToRouteResult;
            Assert.AreEqual("Index", rslt.RouteValues["action"]);
        }
        [Test]
        public void Support_Invite_View_Returns_Test()
        {
            Goal goal = new Goal()
            {
                GoalStatusId = 1,
                GoalId = 1
            };
            _goalRepository.Setup(x => x.GetById(1)).Returns(goal);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            PartialViewResult result = controller.SupportInvitation(1) as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(Goal),
           result.ViewData.Model, "Wrong View Model");

        }

        [Test]
        public void Invite_Email_Post()
        {

            MemoryUser user = new MemoryUser("adarsh");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());

            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);

            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);



            var mailMessage = new MvcMailMessage();
            Guid goalIdToken = Guid.NewGuid();


            string email = "a@gmail.com";

            // userMailerMock.Setup(mailer => mailer.PopulateBody(It.IsAny<MvcMailMessage>(), "SupportGoal", null));
            _mailerBase.Setup(x => x.PopulateBody(It.IsAny<MailMessage>(), "SupportGoal", null));

            _userMailer.Setup(x => x.Support(email, goalIdToken)).Returns(mailMessage);



            InviteEmailFormModel inviteEmail = new InviteEmailFormModel();
            inviteEmail.Email = "a@gmail.com";
            inviteEmail.GrouporGoalId = 1;
            // string result = controller.InviteEmail(inviteEmail) as string;
            //Assert.AreEqual("")
        }


        [Test]
        public void Get_GoalReport_Test()
        {
            Goal goal = new Goal()
            {
                GoalStatusId = 1,
                GoalId = 1
            };
            _goalRepository.Setup(x => x.GetById(1)).Returns(goal);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            JsonResult reslt = controller.GetGoalReport(1) as JsonResult;
            Assert.IsNotNull(reslt);

        }


        [Test]
        public void ListOfGoals()
        {
            // Arrange      
            IEnumerable<Goal> fake = new List<Goal> {
            new Goal { GoalName = "Test1", Desc="Test1Desc"},
            new Goal { GoalName = "Test2", Desc="Test2Desc"},
          
          }.AsEnumerable();
            _goalRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Goal, bool>>>())).Returns(fake);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            // Act
            ViewResult result = controller.ListOfGoals() as ViewResult;
            // Assert
            Assert.IsNotNull(result, "View Result is null");
            Assert.IsInstanceOf(typeof(IEnumerable<Goal>), result.ViewData.Model, "Wrong View Model");
            var gol = result.ViewData.Model as IEnumerable<Goal>;
            Assert.AreEqual(2, gol.Count(), "Got wrong number of Goals");
        }

        [Test]
        public void Goals_List()
        {
            IEnumerable<Goal> fake = new List<Goal> {
            new Goal { GoalName = "Test1", Desc="Test1Desc"},
            new Goal { GoalName = "Test2", Desc="Test2Desc"},
          
          }.AsEnumerable();
            _goalRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Goal, bool>>>())).Returns(fake);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            PartialViewResult result = controller.Goalslist(0, 0) as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(IEnumerable<Goal>), result.ViewData.Model, "Wrong View Model");
            var gol = result.ViewData.Model as IEnumerable<Goal>;
            Assert.AreEqual(2, gol.Count(), "Got wrong number of Goals");
        }

        [Test]
        public void Edit_Update_Get_View()
        {

            Update update = new Update()
            {
                UpdateId = 1,
                Updatemsg = "abc",
                GoalId = 1,

            };
            _updateRepository.Setup(x => x.GetById(1)).Returns(update);

            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            Mapper.CreateMap<Update, UpdateFormModel>();
            PartialViewResult result = controller.EditUpdate(1) as PartialViewResult;
            Assert.IsNotNull(result, "View Result is null");
            Assert.IsInstanceOf(typeof(UpdateFormModel),
                result.ViewData.Model, "Wrong View Model");
            var data = result.ViewData.Model as UpdateFormModel;
            Assert.AreEqual("abc", data.Updatemsg);
        }


        [Test]
        public void Edit_Update_Post()
        {
            MemoryUser user = new MemoryUser("adarsh");
            ApplicationUser applicationUser = GetApplicationUser();

            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());



            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);

            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            //GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, SupportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            Mapper.CreateMap<UpdateFormModel, Update>();
            Mapper.CreateMap<Update, UpdateViewModel>();

            Metric fakeMetric = new Metric()
            {
                MetricId = 1,
                Type = "%"
            };
            Goal goal = new Goal()
            {
                Metric = fakeMetric,
                Target = 100,
                GoalId = 1
            };
            _goalRepository.Setup(x => x.GetById(1)).Returns(goal);


            IEnumerable<Update> updt = new List<Update> {            
            new Update { UpdateId =1, Updatemsg = "t1",GoalId =1},
             new Update { UpdateId =2, Updatemsg = "t2",GoalId =1},
              new Update { UpdateId =3, Updatemsg = "t3",GoalId =2},
            
          }.AsEnumerable();
            _updateRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Update, bool>>>())).Returns(updt);
            UpdateFormModel mock = new UpdateFormModel();
            mock.Updatemsg = "mock";
            mock.GoalId = 1;
            mock.Status = 34;
            PartialViewResult result = controller.EditUpdate(mock) as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(UpdateListViewModel),
            result.ViewData.Model, "Wrong View Model");
        }

        [Test]
        public void Delete_Update_Get_ReturnsView()
        {

            Update update = new Update()
            {
                UpdateId = 1,
                Updatemsg = "abc",
                GoalId = 1,

            };
            _updateRepository.Setup(x => x.GetById(1)).Returns(update);

            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            PartialViewResult result = controller.DeleteUpdate(1) as PartialViewResult;
            Assert.IsNotNull(result, "View Result is null");
            Assert.IsInstanceOf(typeof(Update),
                 result.ViewData.Model, "Wrong View Model");
            var group = result.ViewData.Model as Update;
            Assert.AreEqual("abc", group.Updatemsg, "Got wrong message");

        }

        [Test]
        public void Delete_Update_Post()
        {

            Update update = new Update()
            {
                UpdateId = 1,
                Updatemsg = "abc",
                GoalId = 1,

            };
            _updateRepository.Setup(x => x.GetById(1)).Returns(update);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            var result = controller.DeleteConfirmedUpdate(1) as RedirectToRouteResult;
            Assert.AreEqual("Index", result.RouteValues["action"]);

        }

        [Test]
        public void Update_Supporters_Count()
        {
            IEnumerable<UpdateSupport> updtsprt = new List<UpdateSupport> {            

            new UpdateSupport { UpdateSupportId =1, UpdateId = 1,UserId =  "402bd590-fdc7-49ad-9728-40efbfe512ec"},
            new UpdateSupport { UpdateSupportId =1, UpdateId = 1,UserId =  "402bd590-fdc7-49ad-9728-40efbfe512ec"},
            new UpdateSupport { UpdateSupportId =1, UpdateId = 1,UserId =  "402bd590-fdc7-49ad-9728-40efbfe512ec"},
          
             
            
          }.AsEnumerable();

            _updateSupportRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<UpdateSupport, bool>>>())).Returns(updtsprt);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            int count = controller.NoOfSupports(1);
            Assert.IsNotNull(count);
            Assert.AreEqual("3", count.ToString());

        }

        [Test]
        public void Display_Update_Supporters_Count()
        {
            IEnumerable<UpdateSupport> updtsprt = new List<UpdateSupport> {            

            new UpdateSupport { UpdateSupportId =1, UpdateId = 1,UserId =  "402bd590-fdc7-49ad-9728-40efbfe512ec"},
            new UpdateSupport { UpdateSupportId =1, UpdateId = 1,UserId =  "402bd590-fdc7-49ad-9728-40efbfe512ed"},
            new UpdateSupport { UpdateSupportId =1, UpdateId = 1,UserId =  "402bd590-fdc7-49ad-9728-40efbfe512ef"},
          
             
            
          }.AsEnumerable();

            _updateSupportRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<UpdateSupport, bool>>>())).Returns(updtsprt);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            JsonResult count = controller.DisplayUpdateSupportCount(1) as JsonResult;
            Assert.IsNotNull(count);
            Assert.AreEqual(3, count.Data);

        }

        [Test]
        public void Supporters_Of_Updates_List()
        {
            IEnumerable<ApplicationUser> fakeUser = new List<ApplicationUser> {            
            new ApplicationUser{Activated=true,Email="user1@foo.com",FirstName="user1",LastName="user1",RoleId=0},
              new ApplicationUser{Activated=true,Email="user2@foo.com",FirstName="user2",LastName="user2",RoleId=0},
              new ApplicationUser{Activated=true,Email="user3@foo.com",FirstName="user3",LastName="user3",RoleId=0},
              new ApplicationUser{Activated=true,Email="user4@foo.com",FirstName="user4",LastName="user4",RoleId=0}
          }.AsEnumerable();
            _userRepository.Setup(x => x.GetAll()).Returns(fakeUser);
            IEnumerable<UpdateSupport> fake = new List<UpdateSupport> {            
            new UpdateSupport { UpdateSupportId =1, UpdateId = 1, UserId ="402bd590-fdc7-49ad-9728-40efbfe512ec"},
            new UpdateSupport { UpdateSupportId =2, UpdateId = 1, UserId ="402bd590-fdc7-49ad-9728-40efbfe512ed"},
            new UpdateSupport { UpdateSupportId =3, UpdateId = 1, UserId ="402bd590-fdc7-49ad-9728-40efbfe512ef"},
          
          }.AsEnumerable();
            _updateSupportRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<UpdateSupport, bool>>>())).Returns(fake);
            GoalController controller = new GoalController(_goalBusiness, _metricBusiness, _focusBusiness, _supportBusiness, _updateBusiness, _commentBusiness, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, _commentUserBusiness, _updateSupportBusiness);
            PartialViewResult result = controller.SupportersOfUpdate(1) as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(UpdateSupportersViewModel), result.ViewData.Model, "Wrong View Model");

        }

        public ApplicationUser GetApplicationUser()
        {
            ApplicationUser applicationUser = new ApplicationUser()
            {
                Activated = true,
                Email = "adarsh@foo.com",
                FirstName = "Adarsh",
                LastName = "Vikraman",
                UserName = "adarsh",
                RoleId = 0,
                Id = "402bd590-fdc7-49ad-9728-40efbfe512ec",
                DateCreated = DateTime.Now,
                LastLoginTime = DateTime.Now,
                ProfilePicUrl = null,
            };
            return applicationUser;
        }
    }
}
