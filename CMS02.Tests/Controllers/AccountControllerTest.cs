﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.SessionState;
using AutoMapper;
using CMS02.Controllers;
using CMS02.Mailers;
using CMS02.Models;
using CMS02.Tests.Helpers;
using CMS02.ViewModels;
using CMS02_BusinessLogic;
using CMS02_BusinessLogic.Authentication;
using CMS02_BusinessLogic.Models;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Moq;
using Mvc.Mailer;
using NUnit.Framework;

namespace CMS02.Tests.Controllers
{
    [TestFixture()]
    public class AccountControllerTest
    {
        Mock<IUserRepository> _userRepository;
        Mock<IUserProfileRepository> _userProfileRepository;
        Mock<IFollowRequestRepository> _followRequestRepository;
        Mock<IFollowUserRepository> _followUserRepository;
        Mock<ISecurityTokenRepository> _securityTokenRepository;
        Mock<IUnitOfWork> _unitOfWork;
        Mock<ControllerContext> _controllerContext;
        Mock<IIdentity> _identity;
        Mock<IPrincipal> _principal;
        Mock<HttpContext> _httpContext;
        Mock<HttpContextBase> _contextBase;
        Mock<HttpRequestBase> _httpRequest;
        Mock<HttpResponseBase> _httpResponse;
        Mock<HttpSessionStateBase> _httpSession;
        Mock<GenericPrincipal> _genericPrincipal;

        Mock<TempDataDictionary> _tempData;
        Mock<HttpPostedFileBase> _file;
        Mock<Stream> _stream;
        Mock<IFormsAuthentication> _authentication;

        IUserBusiness _userBusiness;
        IUserProfileBusiness _userProfileBusiness;
        IGoalBusiness _goalBusiness;
        IUpdateBusiness _updateBusiness;
        ICommentBusiness _commentBusiness;
        IFollowRequestBusiness _followRequestBusiness;
        IFollowUserBusiness _followUserBusiness;
        ISecurityTokenBusiness _securityTokenBusiness;
        IUserMailer _userMailer = new UserMailer();
        Mock<AccountController> _accountController;


        [SetUp]
        public void SetUp()
        {
            _userRepository = new Mock<IUserRepository>();
            _userProfileRepository = new Mock<IUserProfileRepository>();
            _followRequestRepository = new Mock<IFollowRequestRepository>();
            _followUserRepository = new Mock<IFollowUserRepository>();
            _securityTokenRepository = new Mock<ISecurityTokenRepository>();


            _unitOfWork = new Mock<IUnitOfWork>();

            _userBusiness = new UserBusiness(_userRepository.Object, _unitOfWork.Object, _userProfileRepository.Object);
            _userProfileBusiness = new UserProfileBusiness(_userProfileRepository.Object, _unitOfWork.Object);
            _followRequestBusiness = new FollowRequestBusiness(_followRequestRepository.Object, _unitOfWork.Object);
            _followUserBusiness = new FollowUserBusiness(_followUserRepository.Object, _unitOfWork.Object);
            _securityTokenBusiness = new SecurityTokenBusiness(_securityTokenRepository.Object, _unitOfWork.Object);

            _controllerContext = new Mock<ControllerContext>();
            _contextBase = new Mock<HttpContextBase>();
            _httpRequest = new Mock<HttpRequestBase>();
            _httpResponse = new Mock<HttpResponseBase>();
            _genericPrincipal = new Mock<GenericPrincipal>();
            _httpSession = new Mock<HttpSessionStateBase>();
            _authentication = new Mock<IFormsAuthentication>();


            _identity = new Mock<IIdentity>();
            _principal = new Mock<IPrincipal>();
            _tempData = new Mock<TempDataDictionary>();
            _file = new Mock<HttpPostedFileBase>();
            _stream = new Mock<Stream>();
            _accountController = new Mock<AccountController>();

        }

        [TearDown]
        public void TearDown()
        {
            TestSmtpClient.SentMails.Clear();
        }

        [Test]
        public void SearchUser()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            IEnumerable<ApplicationUser> fake = new List<ApplicationUser> 
            {
             new ApplicationUser{Activated=true,Email="user1@foo.com",FirstName="user1",LastName="user1",RoleId=0},
              new ApplicationUser{Activated=true,Email="user2@foo.com",FirstName="user2",LastName="user2",RoleId=0},
              new ApplicationUser{Activated=true,Email="user3@foo.com",FirstName="user3",LastName="user3",RoleId=0},
              new ApplicationUser{Activated=true,Email="user4@foo.com",FirstName="user4",LastName="user4",RoleId=0}
          }.AsEnumerable();
            _userRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(fake);
            AccountController contr = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness, userManager);
            IEnumerable<ApplicationUser> result = contr.SearchUser("u") as IEnumerable<ApplicationUser>;
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count(), "not matching");
        }

        [Test]
        public void Image_Upload_GetView()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            MemoryUser user = new MemoryUser("Lindokuhle");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());
            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness,userManager);
            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;
            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);
            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());
            var formsAuthentication = new DefaultFormsAuthentication();
            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);
            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];
            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };
            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);
            PartialViewResult result = controller.ImageUpload() as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(typeof(UploadImageViewModel), result.ViewData.Model, "Wrong model");
            var data = result.ViewData.Model as UploadImageViewModel;
            Assert.AreEqual(null, data.LocalPath, "not matching");
        }

        [Test]
        public void Upload_Image_Post()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            UploadImageViewModel image = new UploadImageViewModel()
            {
                IsFile = true,
                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
                LocalPath = "dddd"
            };
            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness,userManager);
            ViewResult result = controller.UploadImage(image) as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(typeof(UploadImageViewModel), result.ViewData.Model, "WrongType");
            Assert.AreEqual("ImageUpload", result.ViewName);
        }

        [Test]
        public void UserProfile()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            MemoryUser user = new MemoryUser("Lindokuhle");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());           
            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);
            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness,userManager);
            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;
            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);
            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());
            var formsAuthentication = new DefaultFormsAuthentication();
            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);
            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];
            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };
            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            UserProfile prfil = new UserProfile()
            {
                FirstName = "Lindokuhle",
                LastName = "Lindokuhle",
                DateOfBirth = DateTime.Now,
                Gender = true,
                Address = "a",
                City = "a",
                State = "a",
                Country = "a",
                ZipCode = 2344545,
                ContactNo = 1223344556,
                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"
            };
            _userProfileRepository.Setup(x => x.Get(It.IsAny<Expression<Func<UserProfile, bool>>>())).Returns(prfil);
            IEnumerable<FollowRequest> fake = new List<FollowRequest> {
            new FollowRequest { FollowRequestId =1, FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec", ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed"},
            new FollowRequest { FollowRequestId =2, FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec", ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ee"},
            };
            _followRequestRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<FollowRequest, bool>>>())).Returns(fake);
            IEnumerable<FollowUser> fakeuser = new List<FollowUser> {
            new FollowUser {FollowUserId =1, Accepted = false,FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec", ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed"},
            new FollowUser {FollowUserId =2, Accepted = false,FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec", ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ee" },           
            };
            _followUserRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<FollowUser, bool>>>())).Returns(fakeuser);
            ViewResult result = controller.UserProfile("402bd590-fdc7-49ad-9728-40efbfe512ec") as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(typeof(UserProfileViewModel), result.ViewData.Model, "WrongType");
            var data = result.ViewData.Model as UserProfileViewModel;
            Assert.AreEqual("Lindokuhle", data.UserName);
        }

        [Test]
        public void Edit_Basic_Info()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            MemoryUser user = new MemoryUser("Lindokuhle");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());
            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness,userManager);
            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;
            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);
            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());
            var formsAuthentication = new DefaultFormsAuthentication();
            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);
            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];
            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };
            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            UserProfile userProfile = new UserProfile()
            {
                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
                FirstName = "Lindokuhle",
                LastName = "Mkhwanazi",
                Email = "lin@jaz.com",
            };
            _userProfileRepository.Setup(x => x.Get(It.IsAny<Expression<Func<UserProfile, bool>>>())).Returns(userProfile);
            Mapper.CreateMap<UserProfile, UserProfileFormModel>();
            PartialViewResult result = controller.EditBasicInfo() as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(typeof(UserProfileFormModel), result.ViewData.Model, "WrongType");
            var data = result.ViewData.Model as UserProfileFormModel;
            Assert.AreEqual("lin@jazz.com", data.Email);
        }

        [Test]
        public void Edit_Personal_Info()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            MemoryUser user = new MemoryUser("Lindokuhle");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());
            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness,userManager);
            _principal.SetupGet(x => x.Identity.Name).Returns("Lindokuhle");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;
            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);
            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());
            var formsAuthentication = new DefaultFormsAuthentication();
            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);
            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];
            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };
            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            UserProfile grpuser = new UserProfile()
            {
                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
                Address = "t",
                City = "t",
                State = "asr",
            };
            _userProfileRepository.Setup(x => x.Get(It.IsAny<Expression<Func<UserProfile, bool>>>())).Returns(grpuser);
            Mapper.CreateMap<UserProfile, UserProfileFormModel>();

            PartialViewResult result = controller.EditPersonalInfo() as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(typeof(UserProfileFormModel), result.ViewData.Model, "WrongType");
            var data = result.ViewData.Model as UserProfileFormModel;
            Assert.AreEqual("t", data.Address);
        }

        [Test]
        public void Editprofile_Post()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            ApplicationUser applicationUser = GetApplicationUser();
            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);
            UserProfileFormModel profile = new UserProfileFormModel();

            Mapper.CreateMap<UserProfileFormModel, UserProfile>();
            profile.FirstName = "Lindokuhle";

            AccountController contr = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness,userManager);
            var result = contr.EditProfile(profile) as RedirectToRouteResult;

            Assert.AreEqual("UserProfile", result.RouteValues["action"]);
        }

        [Test]
        public void Follow_Request()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            MemoryUser user = new MemoryUser("Lindokuhle");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());



            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness,userManager);
            _principal.SetupGet(x => x.Identity.Name).Returns("Lindokuhle");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);

            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);
            Mapper.CreateMap<FollowRequestFormModel, FollowRequest>();


            var result = controller.FollowRequest("402bd590-fdc7-49ad-9728-40efbfe512ec") as RedirectToRouteResult;
            Assert.AreEqual("UserProfile", result.RouteValues["action"]);
        }

        [Test]
        public void Accept_Request()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            ApplicationUser applicationUser = GetApplicationUser();
            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);
            AccountController contr = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness,userManager);
            var result = contr.AcceptRequest("402bd590-fdc7-49ad-9728-40efbfe512ed","402bd590-fdc7-49ad-9728-40efbfe512ec") as RedirectToRouteResult;
            Assert.AreEqual("Index", result.RouteValues["action"]);

        }

        [Test]
        public void Delete_Follow_Request()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            FollowRequest request = new FollowRequest()
            {
                FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
                ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed",

            };
            _followRequestRepository.Setup(x => x.Get(It.IsAny<Expression<Func<FollowRequest, bool>>>())).Returns(request);
            AccountController contr = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness,userManager);
            var result = contr.RejectRequest("402bd590-fdc7-49ad-9728-40efbfe512ed","402bd590-fdc7-49ad-9728-40efbfe512ec") as RedirectToRouteResult;
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }
        [Test]
        public void UnFollow()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            MemoryUser user = new MemoryUser("Lindokuhle");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());



            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness,userManager);

            _principal.SetupGet(x => x.Identity.Name).Returns("Lindokuhle");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            FollowUser flwuser = new FollowUser()
            {
                FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
                ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed",

            };
            _followUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<FollowUser, bool>>>())).Returns(flwuser);

            var result = controller.Unfollow("402bd590-fdc7-49ad-9728-40efbfe512ed") as RedirectToRouteResult;
            Assert.AreEqual("UserProfile", result.RouteValues["action"]);
        }

        [Test]
        public void Followers_List()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            MemoryUser user = new MemoryUser("Lindokuhle");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());



            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness,userManager);

            _principal.SetupGet(x => x.Identity.Name).Returns("Lindokuhle");
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            string[] userRoles = { goalsetterUser.RoleName };

            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
            IEnumerable<FollowUser> fakeuser = new List<FollowUser> {
            new FollowUser {FollowUserId =1, Accepted = false,FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed", ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",},
            new FollowUser {FollowUserId =2, Accepted = false,FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ee", ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec" },

            };
            _followUserRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<FollowUser, bool>>>())).Returns(fakeuser);

            Mapper.CreateMap<ApplicationUser, FollowersViewModel>();


            ViewResult result = controller.Followers() as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(typeof(IEnumerable<FollowersViewModel>), result.ViewData.Model, "WrongType");
            var data = result.ViewData.Model as IEnumerable<FollowersViewModel>;
            Assert.AreEqual(2, data.Count());
        }


        [Test]
        public void Followings_list()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            MemoryUser user = new MemoryUser("Lindokuhle");
            ApplicationUser applicationUser = GetApplicationUser();
            var userContext = new UserInfo
            {
                UserId = user.Id,
                DisplayName = user.UserName,
                UserIdentifier = applicationUser.Email,
                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
            };
            var testTicket = new FormsAuthenticationTicket(
                1,
                user.Id,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userContext.ToString());



            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness,userManager);

            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            controller.ControllerContext = _controllerContext.Object;

            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

            var formsAuthentication = new DefaultFormsAuthentication();



            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

            var ticket = formsAuthentication.Decrypt(authCookie.Value);
            var goalsetterUser = new GpUser(ticket);
            _principal.Setup(x => x.Identity).Returns(goalsetterUser);

            IEnumerable<FollowUser> fakeuser = new List<FollowUser> {
            new FollowUser {FollowUserId =1, Accepted = false,FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec", ToUserId ="402bd590-fdc7-49ad-9728-40efbfe512ed",},
            new FollowUser {FollowUserId =2, Accepted = false,FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec", ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ee" },

            };
            _followUserRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<FollowUser, bool>>>())).Returns(fakeuser);

            Mapper.CreateMap<ApplicationUser, FollowersViewModel>();


            ViewResult result = controller.Followings() as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(typeof(IEnumerable<FollowingViewModel>), result.ViewData.Model, "WrongType");
            var data = result.ViewData.Model as IEnumerable<FollowingViewModel>;
            Assert.AreEqual(2, data.Count());
        }

        [Test]
        public void Login_Get_View_If_Guid_Is_Null()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            Guid goalIdToken = Guid.NewGuid();
            _controllerContext.SetupGet(p => p.HttpContext.Request.QueryString).Returns(new NameValueCollection());
            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness, userManager);
            controller.ControllerContext = _controllerContext.Object;
            ViewResult rslt = controller.Login("abcd") as ViewResult;
            Assert.IsNotNull(rslt);

        }

        [Test]
        public void Login_Get_View_If_Guid_Is_NotNull()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            //mocking QueryString
            var querystring = new NameValueCollection { { "guid", "got_value" } };
            var querystring1 = new NameValueCollection { { "reg", "value" } };
            Guid goalIdToken = Guid.NewGuid();
            // Guid 
            _controllerContext.SetupGet(p => p.HttpContext.Request.QueryString).Returns(querystring);
            //controllerContext.SetupGet(p => p.HttpContext.Request.QueryString).Returns(querystring1);
            _controllerContext.SetupGet(p => p.HttpContext.Session).Returns(_httpSession.Object);
            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness, userManager);
            controller.ControllerContext = _controllerContext.Object;

            var httprequest = new HttpRequest("", "http://localhost/", "");
            var stringWriter = new StringWriter();
            var httpResponce = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httprequest, httpResponce);
            // Mocking HttpContext.Current
            var sessionContainer = new HttpSessionStateContainer("id",
                                    new SessionStateItemCollection(),
                                    new HttpStaticObjectsCollection(),
                                    10,
                                    true,
                                    HttpCookieMode.AutoDetect,
                                    SessionStateMode.InProc,
                                    false);

            httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
                        BindingFlags.NonPublic | BindingFlags.Instance,
                        null, CallingConventions.Standard,
                        new[] { typeof(HttpSessionStateContainer) },
                        null)
                        .Invoke(new object[] { sessionContainer });

            HttpContext.Current = httpContext;

            ViewResult rslt = controller.Login("abcd") as ViewResult;
            Assert.IsNotNull(rslt);
        }

        [Test()]
        public void LoginTest()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());

            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness, userManager);
            var mockAuthenticationManager = new Mock<IAuthenticationManager>();
            mockAuthenticationManager.Setup(am => am.SignOut());
            mockAuthenticationManager.Setup(am => am.SignIn());
            controller.AuthenticationManager = mockAuthenticationManager.Object;
            ApplicationUser applicationUser = GetApplicationUser();
            userManager.CreateAsync(applicationUser, "Pass@12");
            var result = controller.Login(new LoginViewModel { UserName = "Lindokuhle", Password = "Pass@12", RememberMe = false }, "abcd").Result;
            Assert.IsNotNull(result);
            var addedUser = userManager.FindByName("Lindokuhle");
            Assert.IsNotNull(addedUser);
            Assert.AreEqual("adarsh", addedUser.UserName);
        }

        [Test]
        public void LogOff()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            var mockAuthenticationManager = new Mock<IAuthenticationManager>();
            mockAuthenticationManager.Setup(am => am.SignOut());
            mockAuthenticationManager.Setup(am => am.SignIn());
            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness, userManager);
            controller.AuthenticationManager = mockAuthenticationManager.Object;
            var httprequest = new HttpRequest("", "http://localhost/", "");
            var stringWriter = new StringWriter();
            var httpResponce = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httprequest, httpResponce);
            var sessionContainer = new HttpSessionStateContainer("id",
                                    new SessionStateItemCollection(),
                                    new HttpStaticObjectsCollection(),
                                    10,
                                    true,
                                    HttpCookieMode.AutoDetect,
                                    SessionStateMode.InProc,
                                    false);
            httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
                        BindingFlags.NonPublic | BindingFlags.Instance,
                        null, CallingConventions.Standard,
                        new[] { typeof(HttpSessionStateContainer) },
                        null)
                        .Invoke(new object[] { sessionContainer });
            HttpContext.Current = httpContext;

            var result = controller.LogOff() as RedirectToRouteResult;
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }

        [Test]
        public void Register_Get_Returns_View()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness, userManager);
            ViewResult rslt = controller.Register() as ViewResult;
            Assert.IsNotNull(rslt);
        }

        [Test()]
        public void RegisterTest()
        {
            var userManager = new UserManager<ApplicationUser>(new TestUserStore());
            AccountController controller = new AccountController(_userBusiness, _userProfileBusiness, _goalBusiness, _updateBusiness, _commentBusiness, _followRequestBusiness, _followUserBusiness, _securityTokenBusiness, userManager);
            var mockAuthenticationManager = new Mock<IAuthenticationManager>();
            mockAuthenticationManager.Setup(am => am.SignOut());
            mockAuthenticationManager.Setup(am => am.SignIn());
            controller.AuthenticationManager = mockAuthenticationManager.Object;
            var httprequest = new HttpRequest("", "http://localhost/", "");
            var stringWriter = new StringWriter();
            var httpResponce = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httprequest, httpResponce);

            var sessionContainer = new HttpSessionStateContainer("id",
                                    new SessionStateItemCollection(),
                                    new HttpStaticObjectsCollection(),
                                    10,
                                    true,
                                    HttpCookieMode.AutoDetect,
                                    SessionStateMode.InProc,
                                    false);
            httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
                        BindingFlags.NonPublic | BindingFlags.Instance,
                        null, CallingConventions.Standard,
                        new[] { typeof(HttpSessionStateContainer) },
                        null)
                        .Invoke(new object[] { sessionContainer });
            HttpContext.Current = httpContext;
            var result =
                controller.Register(new RegisterViewModel
                {
                    UserName = "Lindokuhle",
                    Password = "Pass@12",
                    ConfirmPassword = "Pass@12"
                }).Result;
            Assert.IsNotNull(result);
            var addedUser = userManager.FindByName("Lindokuhle");
            Assert.IsNotNull(addedUser);
            Assert.AreEqual("adarsh", addedUser.UserName);
        }

         public ApplicationUser GetApplicationUser()
        {
          ApplicationUser applicationUser = new ApplicationUser()
            {
                Activated = true,
                Email = "lin@jazz.com",
                FirstName = "Lindokuhle",
                LastName = "Mkhwanazi",
                UserName = "Lindokuhle",
                RoleId = 0,
                Id = "402bd590-fdc7-49ad-9728-40efbfe512ec",
                DateCreated = DateTime.Now,
                LastLoginTime = DateTime.Now,
                ProfilePicUrl = null,
            };
          return applicationUser;
        }
    }
}
