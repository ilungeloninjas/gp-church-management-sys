﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Security.Principal;
//using System.Web;
//using System.Web.Mvc;
//using System.Web.Security;
//using AutoMapper;
//using CMS02.Controllers;
//using CMS02.Tests.Helpers;
//using CMS02.ViewModels;
//using CMS02_BusinessLogic;
//using CMS02_BusinessLogic.Authentication;
//using CMS02_BusinessLogic.Models;
//using CMS02_Data.Infrastructure;
//using CMS02_Models.Models;
//using CMS02_Service.Repository;
//using Moq;
//using NUnit.Framework;

//namespace CMS02.Tests.Controllers
//{
//    [TestFixture]
//    public class GroupControllerTest
//    {
//        //Mock<IIdentity> iIdentity;
//        Mock<IGroupRepository> _groupRepository;
//        Mock<IFollowUserRepository> _followUserRepository;
//        Mock<IUserRepository> _userRepository;
//        Mock<IGroupUserRepository> _groupUserRepository;
//        Mock<IFocusRepository> _focusRepository;
//        Mock<IMetricRepository> _metricRepository;
//        Mock<IUnitOfWork> _unitOfWork;
//        Mock<IGroupGoalRepository> _groupGoalRepository;
//        Mock<ICommentRepository> _commentRepository;
//        Mock<IGroupUpdateRepository> _groupUdateRepository;
//        Mock<IGroupUpdateUserRepository> _updateUserRepository;
//        Mock<IGroupCommentRepository> _groupCommentRepository;
//        Mock<IGroupCommentUserRepository> _groupCommentUserRepository;
//        Mock<IGroupInvitationRepository> _groupInvitationRepository;
//        Mock<IGroupRequestRepository> _groupRequestRepository;
//        Mock<IGoalStatusRepository> _goalStatusRepository;
//        Mock<IUserProfileRepository> _userProfileRepository;
//        Mock<IGroupUpdateSupportRepository> _groupUpdateSupportRepository;
//        Mock<IGroupUpdateUserRepository> _groupUpdateUserRepository;
//        //Mock<CreateGroupFormModel> group;
//        //Mock<FocusFormModel> focus;
//        //Mock<GroupViewModel> groupView;


//        IGroupBusiness _groupBusiness;
//        IGroupInvitationBusiness _groupInvitationBusiness;
//        IUserBusiness _userBusiness;
//        IGroupUserBusiness _groupUserBusiness;
//        IMetricBusiness _metricBusiness;
//        IFocusBusiness _focusBusiness;
//        IGroupGoalBusiness _groupgoalBusiness;
//        ISecurityTokenBusiness _securityTokenBusiness;
//        IGroupUpdateBusiness _groupUpdateBusiness;
//        IGroupCommentBusiness _groupCommentBusiness;
//        IGoalStatusBusiness _goalStatusBusiness;
//        IGroupRequestBusiness _groupRequestBusiness;
//        IFollowUserBusiness _followUserBusiness;
//        IGroupCommentUserBusiness _groupCommentUserBusiness;
//        IGroupUpdateUserBusiness _updateUserBusiness;
//        IUserProfileBusiness _userProfileBusiness;
//        IGroupUpdateSupportBusiness _groupUpdateSupportBusiness;
//        IGroupUpdateUserBusiness _groupUpdateUserBusiness;


//        Mock<ControllerContext> _controllerContext;
//        Mock<IIdentity> _identity;
//        Mock<IPrincipal> _principal;
//        Mock<HttpContext> _httpContext;
//        Mock<HttpContextBase> _contextBase;
//        Mock<HttpRequestBase> _httpRequest;
//        Mock<HttpResponseBase> _httpResponse;
//        Mock<GenericPrincipal> _genericPrincipal;


//        [SetUp]
//        public void SetUp()
//        {
//            _groupRepository = new Mock<IGroupRepository>();
//            _followUserRepository = new Mock<IFollowUserRepository>();
//            _groupUserRepository = new Mock<IGroupUserRepository>();
//            _focusRepository = new Mock<IFocusRepository>();
//            _commentRepository = new Mock<ICommentRepository>();
//            _groupGoalRepository = new Mock<IGroupGoalRepository>();
//            _metricRepository = new Mock<IMetricRepository>();
//            _userRepository = new Mock<IUserRepository>();
//            _groupUdateRepository = new Mock<IGroupUpdateRepository>();
//            _updateUserRepository = new Mock<IGroupUpdateUserRepository>();
//            _groupCommentRepository = new Mock<IGroupCommentRepository>();
//            _groupCommentUserRepository = new Mock<IGroupCommentUserRepository>();
//            _groupInvitationRepository = new Mock<IGroupInvitationRepository>();
//            _groupRequestRepository = new Mock<IGroupRequestRepository>();
//            _goalStatusRepository = new Mock<IGoalStatusRepository>();
//            _userProfileRepository = new Mock<IUserProfileRepository>();
//            _groupUpdateSupportRepository = new Mock<IGroupUpdateSupportRepository>();
//            _groupUpdateUserRepository = new Mock<IGroupUpdateUserRepository>();

//            _unitOfWork = new Mock<IUnitOfWork>();
//            _controllerContext = new Mock<ControllerContext>();
//            _contextBase = new Mock<HttpContextBase>();
//            // httpContext = new Mock<HttpContext>();
//            _httpRequest = new Mock<HttpRequestBase>();
//            _httpResponse = new Mock<HttpResponseBase>();
//            _genericPrincipal = new Mock<GenericPrincipal>();


//            _identity = new Mock<IIdentity>();
//            _principal = new Mock<IPrincipal>();



//            _groupBusiness = new ChurchBusiness(_groupRepository.Object, _followUserRepository.Object, _groupUserRepository.Object, _unitOfWork.Object);
//            _focusBusiness = new FocusBusiness(_focusRepository.Object, _unitOfWork.Object);
//            _metricBusiness = new MetricBusiness(_metricRepository.Object, _unitOfWork.Object);
//            _groupgoalBusiness = new ChurchGoalBusiness(_groupGoalRepository.Object, _unitOfWork.Object);
//            _groupUserBusiness = new ChurchUserBusiness(_groupUserRepository.Object, _userRepository.Object, _unitOfWork.Object);
//            _groupUpdateBusiness = new ChurchUpdateBusiness(_groupUdateRepository.Object, _updateUserRepository.Object, _groupGoalRepository.Object, _unitOfWork.Object);
//            _groupCommentBusiness = new ChurchCommentBusiness(_groupCommentRepository.Object, _groupCommentUserRepository.Object, _groupUdateRepository.Object, _unitOfWork.Object);
//            _userBusiness = new UserBusiness(_userRepository.Object, _unitOfWork.Object, _userProfileRepository.Object);
//            _groupInvitationBusiness = new ChurchInvitationBusiness(_groupInvitationRepository.Object, _unitOfWork.Object);
//            _groupRequestBusiness = new ChurchRequestBusiness(_groupRequestRepository.Object, _unitOfWork.Object);
//            _groupCommentUserBusiness = new ChurchCommentUserBusiness(_groupCommentUserRepository.Object, _unitOfWork.Object, _userRepository.Object);
//            _goalStatusBusiness = new GoalStatusBusiness(_goalStatusRepository.Object, _unitOfWork.Object);
//            _userProfileBusiness = new UserProfileBusiness(_userProfileRepository.Object, _unitOfWork.Object);
//            _groupUpdateSupportBusiness = new ChurchUpdateSupportBusiness(_groupUpdateSupportRepository.Object, _unitOfWork.Object);
//            _groupUpdateUserBusiness = new ChurchUpdateUserBusiness(_groupUpdateUserRepository.Object, _unitOfWork.Object, _userRepository.Object);
//        }
//        [TearDown]
//        public void TearDown()
//        {
//        }

//        [Test]
//        public void Index()
//        {

//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };

//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());

//            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);


//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);


//            GroupUser grpUser = new GroupUser()
//            {
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
//                Admin = true
//            };
//            _groupUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupUser, bool>>>())).Returns(grpUser);

//            Group group = new Group() { GroupId = 1, GroupName = "Test", Description = "test" };
//            _groupRepository.Setup(x => x.GetById(1)).Returns(group);

//            IEnumerable<GroupGoal> fakegoal = new List<GroupGoal> {
//            new GroupGoal{ GroupId = 1, Description="Test1Desc",GroupGoalId =1,GroupUser = grpUser},
//             new GroupGoal{ GroupId = 1, Description="Test1Desc",GroupGoalId =1,GroupUser = grpUser},
          
          
//          }.AsEnumerable();
//            _groupGoalRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<GroupGoal, bool>>>())).Returns(fakegoal);

//            IEnumerable<Focus> fakeFocus = new List<Focus> 
//            {
//            new Focus { FocusId = 1, FocusName="Test1",GroupId = 1},
//             new Focus { FocusId = 2, FocusName="Test2",GroupId = 1},
//            new Focus { FocusId = 3, FocusName="Test3",GroupId = 2}
//          }.AsEnumerable();
//            _focusRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Focus, bool>>>())).Returns(fakeFocus);

//            IEnumerable<ApplicationUser> fakeUser = new List<ApplicationUser> {            
//              new ApplicationUser{Activated=true,Email="user1@foo.com",FirstName="user1",LastName="user1",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user2@foo.com",FirstName="user2",LastName="user2",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user3@foo.com",FirstName="user3",LastName="user3",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user4@foo.com",FirstName="user4",LastName="user4",RoleId=0}
//          }.AsEnumerable();
//            _userRepository.Setup(x => x.GetAll()).Returns(fakeUser);



//            Mapper.CreateMap<Group, GroupViewModel>();
//            Mapper.CreateMap<GroupGoal, GroupGoalViewModel>();

//            ViewResult result = controller.Index(1) as ViewResult;
//            Assert.IsNotNull(result);
//            Assert.AreEqual("Index", result.ViewName);
//            Assert.IsInstanceOfType(typeof(GroupViewModel),
//                result.ViewData.Model, "WrongType");
//        }


//        [Test]
//        public void Group_Goal_Page_View()
//        {
//            //Arrange 
//            GroupUser grpUser = new GroupUser()
//            {
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"
//            };
//            GroupGoal goal = new GroupGoal()
//            {
//                GroupGoalId = 1,
//                GoalName = "t",
//                GoalStatusId = 1,
//                Description = "x",
//                StartDate = DateTime.Now,
//                EndDate = DateTime.Now.AddDays(1),
//                GroupUser = grpUser

//            };
//            _groupGoalRepository.Setup(x => x.GetById(1)).Returns(goal);

//            ApplicationUser user = new ApplicationUser()
//            {
//                Id = "402bd590-fdc7-49ad-9728-40efbfe512ec"
//            };
//            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(user);
//            IEnumerable<GoalStatus> fake = new List<GoalStatus> {
//            new GoalStatus { GoalStatusId =1, GoalStatusType ="Inprogress"},
//            new GoalStatus { GoalStatusId =2, GoalStatusType ="OnHold"},
         
//          }.AsEnumerable();
//            _goalStatusRepository.Setup(x => x.GetAll()).Returns(fake);

//            Mapper.CreateMap<GroupGoal, GroupGoalViewModel>();
//            //Act
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            ViewResult result = controller.GroupGoal(1) as ViewResult;

//            //Assert
//            Assert.IsNotNull(result);
//            Assert.IsInstanceOfType(typeof(GroupGoalViewModel), result.ViewData.Model, "WrongType");
//            var data = result.ViewData.Model as GroupGoalViewModel;
//            Assert.AreEqual("t", data.GoalName);
//        }

//        [Test]
//        public void Create_Group_Get_ReturnsView()
//        {
//            //Arrange
//            GroupFormModel group = new GroupFormModel();
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            //Act
//            PartialViewResult result = controller.CreateGroup() as PartialViewResult;
//            //Assert
//            Assert.IsNotNull(result);

//        }

//        [Test]
//        public void Group_name_Required()
//        {
//            // Arrange 
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());


//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);


//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");

//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);


//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);



//            // The MVC pipeline doesn't run, so binding and validation don't run. 
//            controller.ModelState.AddModelError("", "mock error message");

//            // Act          
//            Mapper.CreateMap<GroupFormModel, Group>();
//            GroupFormModel group = new GroupFormModel()
//            {
//                GroupName = string.Empty,
//                Description = "Test"

//            };


//            var result = controller.CreateGroup(group) as ViewResult;

//            // Assert - check that we are passing an invalid model to the view
//            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
//            Assert.AreEqual("CreateGroup", result.ViewName);

//        }

//        [Test]
//        public void Group_Description_Required()
//        {
//            //Arrange

//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());



//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");

//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);


//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);


//            // The MVC pipeline doesn't run, so binding and validation don't run. 
//            controller.ModelState.AddModelError("", "mock error message");


//            // Act          

//            Mapper.CreateMap<GroupFormModel, Group>();
//            GroupFormModel group = new GroupFormModel();
//            group.Description = string.Empty;
//            var result = (ViewResult)controller.CreateGroup(group);
//            // Assert - check that we are passing an invalid model to the view
//            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
//            Assert.AreEqual("CreateGroup", result.ViewName);

//        }

//        [Test]
//        public void Create_Group()
//        {

//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());



//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);
//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());
//            var formsAuthentication = new DefaultFormsAuthentication();
//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);
//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];
//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };
//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);




//            // Act
//            Mapper.CreateMap<GroupFormModel, Group>();
//            GroupFormModel group = new GroupFormModel()
//            {
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
//                CreatedDate = DateTime.Now,
//                Description = "Mock",
//                GroupName = "Mock",
//                GroupId = 1,

//            };
//            var result = (RedirectToRouteResult)controller.CreateGroup(group);

//            // Assert
//            Assert.AreEqual("Index", result.RouteValues["action"]);



//        }

//        [Test]
//        public void Focus_Name_Mandatory()
//        {
//            // Arrange                    
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            // The MVC pipeline doesn't run, so binding and validation don't run. 
//            controller.ModelState.AddModelError("", "mock error message");
//            // Act          

//            Mapper.CreateMap<FocusFormModel, Focus>();
//            FocusFormModel focus = new FocusFormModel();
//            focus.FocusName = string.Empty;
//            var result = (ViewResult)controller.CreateFocus(focus);

//            // Assert - check that we are passing an invalid model to the view
//            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
//            Assert.AreEqual("CreateFocus", result.ViewName);
//        }

//        [Test]
//        public void Focus_Description_Mandatory()
//        {
//            // Arrange                    
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            // The MVC pipeline doesn't run, so binding and validation don't run. 
//            controller.ModelState.AddModelError("", "mock error message");

//            // Act          
//            Mapper.CreateMap<FocusFormModel, Focus>();
//            FocusFormModel focus = new FocusFormModel();
//            focus.Description = string.Empty;
//            var result = (ViewResult)controller.CreateFocus(focus);

//            // Assert - check that we are passing an invalid model to the view
//            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
//            Assert.AreEqual("CreateFocus", result.ViewName);
//        }

//        [Test]
//        public void Create_Focus()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);
//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());
//            var formsAuthentication = new DefaultFormsAuthentication();
//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);
//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];
//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);




//            FocusFormModel focus = new FocusFormModel();
//            focus.GroupId = 1;
//            focus.FocusName = "t";
//            focus.Description = "t";
//            Focus mock = new Focus()
//            {
//                FocusName = "x",
//                FocusId = 2,
//                Description = "t",
//                GroupId = 1
//            };

//            //Create Mapping 
//            Mapper.CreateMap<FocusFormModel, Focus>();


//            // Act
//            var result = (RedirectToRouteResult)controller.CreateFocus(focus);
//            Assert.AreEqual("Focus", result.RouteValues["action"]);


//        }


//        [Test]
//        public void Focus()
//        {
//            Focus mock = new Focus()
//            {
//                FocusName = "x",
//                FocusId = 1,
//                Description = "t",
//                GroupId = 1
//            };
//            _focusRepository.Setup(x => x.GetById(1)).Returns(mock);

//            GroupUser grpUser = new GroupUser()
//            {
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
//                Admin = true
//            };
//            _groupUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupUser, bool>>>())).Returns(grpUser);

//            IEnumerable<GroupGoal> fakegoal = new List<GroupGoal> {
//            new GroupGoal{ GroupId = 1, Description="Test1Desc",GroupGoalId =1,FocusId =1,GroupUser = grpUser},
//             new GroupGoal{ GroupId = 1, Description="Test1Desc",GroupGoalId =2,FocusId = 2,GroupUser = grpUser},
          
          
//          }.AsEnumerable();
//            _groupGoalRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<GroupGoal, bool>>>())).Returns(fakegoal);

//            Mapper.CreateMap<Focus, FocusViewModel>();
//            Mapper.CreateMap<GroupGoal, GroupGoalViewModel>();


//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());

//            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);


//            IEnumerable<ApplicationUser> fakeUser = new List<ApplicationUser> {            
//              new ApplicationUser{Activated=true,Email="user1@foo.com",FirstName="user1",LastName="user1",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user2@foo.com",FirstName="user2",LastName="user2",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user3@foo.com",FirstName="user3",LastName="user3",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user4@foo.com",FirstName="user4",LastName="user4",RoleId=0}
//          }.AsEnumerable();
//            _userRepository.Setup(x => x.GetAll()).Returns(fakeUser);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            _principal.SetupGet(x => x.Identity.Name).Returns("abc");
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);
//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());
//            var formsAuthentication = new DefaultFormsAuthentication();
//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);
//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];
//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);

//            ViewResult result = controller.Focus(1) as ViewResult;
//            Assert.IsNotNull(result);
//            Assert.AreEqual("Focus", result.ViewName);


//        }

//        [Test]
//        public void Create_Focus_Get_ReturnsView()
//        {
//            //Arrange

//            Group group = new Group() { GroupId = 1, GroupName = "Test", Description = "test" };
//            _groupRepository.Setup(x => x.GetById(1)).Returns(group);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            //Act
//            ActionResult result = controller.CreateFocus(1) as ActionResult;
//            //Assert
//            Assert.IsNotNull(result);

//        }

//        [Test]
//        public void Create_Goal_Get_ReturnsView()
//        {

//            IEnumerable<Focus> fakeFocus = new List<Focus> 
//            {
//            new Focus { FocusId = 1, FocusName="Test1",GroupId = 1},
//             new Focus { FocusId = 2, FocusName="Test2",GroupId = 1},
//            new Focus { FocusId = 3, FocusName="Test3",GroupId = 1}
//          }.AsEnumerable();
//            _focusRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<Focus, bool>>>())).Returns(fakeFocus);

//            IEnumerable<Metric> fakeMatrices = new List<Metric> 
//            {
//                new Metric{MetricId=1, Type="Test1"},
//                new Metric{MetricId=2,Type="Test2"},
//                new Metric{MetricId=3,Type="Test3"}
//            }.AsEnumerable();

//            _metricRepository.Setup(x => x.GetAll()).Returns(fakeMatrices);
//            GroupGoalFormModel goal = new GroupGoalFormModel();
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            ViewResult result = controller.CreateGoal(1) as ViewResult;

//            Assert.IsNotNull(result, "View Result is null");
//            Assert.IsInstanceOf(typeof(GroupGoalFormModel),
//                result.ViewData.Model, "Wrong View Model");


//        }

//        [Test]
//        public void Create_Goal()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
//            GroupUser grpuser = new GroupUser()
//            {
//                GroupId = 1,
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"
//            };
//            _groupUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupUser, bool>>>())).Returns(grpuser);


//            // Act
//            Mapper.CreateMap<GroupGoalFormModel, GroupGoal>();

//            GroupGoalFormModel goal = new GroupGoalFormModel()
//            {
//                GoalName = "t",
//                GroupGoalId = 1,
//                StartDate = DateTime.Now,
//                EndDate = DateTime.Now.AddDays(1),
//                Description = "t",
//                GroupId = 1,
//                GroupUserId = 1,


//            };
//            var result = (RedirectToRouteResult)controller.CreateGoal(goal);


//            Assert.AreEqual("Index", result.RouteValues["action"]);


//        }
//        [Test]
//        public void Edit_Group_Get_ReturnsView()
//        {


//            Group group = new Group() { GroupId = 1, GroupName = "Test", Description = "test" };
//            _groupRepository.Setup(x => x.GetById(1)).Returns(group);
//            Mapper.CreateMap<Group, GroupFormModel>();
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);


//            ViewResult actual = controller.EditGroup(1) as ViewResult;


//            Assert.IsNotNull(actual, "View Result is null");
//            Assert.IsInstanceOf(typeof(GroupFormModel),
//                actual.ViewData.Model, "Wrong View Model");
//        }

//        [Test]
//        public void Edit_Focus_Get_ReturnsView()
//        {


//            Focus focus = new Focus() { FocusId = 1, FocusName = "Test", Description = "test" };
//            _focusRepository.Setup(x => x.GetById(1)).Returns(focus);
//            Mapper.CreateMap<Focus, FocusFormModel>();
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            ViewResult actual = controller.EditFocus(1) as ViewResult;
//            Assert.IsNotNull(actual, "View Result is null");
//            Assert.IsInstanceOf(typeof(FocusFormModel),
//                actual.ViewData.Model, "Wrong View Model");
//        }

//        [Test]
//        public void Delete_Focus_Get_ReturnsView()
//        {
//            Focus fake = new Focus()
//            {
//                FocusId = 1,
//                FocusName = "test",
//                Description = "test"
//            };
//            _focusRepository.Setup(x => x.GetById(1)).Returns(fake);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            ViewResult result = controller.DeleteFocus(1) as ViewResult;

//            Assert.IsNotNull(result, "View Result is null");
//            Assert.IsInstanceOf(typeof(Focus),
//                 result.ViewData.Model, "Wrong View Model");
//            var focus = result.ViewData.Model as Focus;
//            Assert.AreEqual("test", focus.Description, "Got wrong Focus Description");

//        }

//        [Test]
//        public void Delete_Group_Get_ReturnsView()
//        {
//            Group fake = new Group()
//            {
//                GroupId = 1,
//                GroupName = "test",
//                Description = "test"
//            };
//            _groupRepository.Setup(x => x.GetById(1)).Returns(fake);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            ViewResult result = controller.DeleteGroup(1) as ViewResult;
//            Assert.IsNotNull(result, "View Result is null");
//            Assert.IsInstanceOf(typeof(Group),
//                 result.ViewData.Model, "Wrong View Model");
//            var group = result.ViewData.Model as Group;
//            Assert.AreEqual("test", group.Description, "Got wrong Focus Description");

//        }
//        [Test]
//        public void Delete_Goal_Get_ReturnsView()
//        {


//            GroupGoal fake = new GroupGoal()
//            {
//                GroupGoalId = 1,
//                GoalName = "test",
//                Description = "test",
//                GroupId = 1,
//                StartDate = DateTime.Now,
//                EndDate = DateTime.Now.AddDays(1),
//                GoalStatusId = 1,
//                GroupUserId = 2

//            };


//            _groupGoalRepository.Setup(x => x.GetById(1)).Returns(fake);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            ViewResult result = controller.DeleteGoal(1) as ViewResult;
//            Assert.IsNotNull(result, "View Result is null");
//            Assert.IsInstanceOf(typeof(GroupGoal),
//                 result.ViewData.Model, "Wrong View Model");
//            var group = result.ViewData.Model as GroupGoal;
//            Assert.AreEqual("test", group.Description, "Got wrong Focus Description");

//        }

//        [Test]
//        public void Delete_Focus_Post()
//        {
//            Focus fake = new Focus()
//            {
//                FocusId = 1,
//                FocusName = "test",
//                Description = "test"
//            };

//            _focusRepository.Setup(x => x.GetById(1)).Returns(fake);
//            Assert.IsNotNull(fake);
//            Assert.AreEqual("test", fake.FocusName);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            var result = controller.DeleteConfirmedFocus(1) as RedirectToRouteResult;
//            Assert.AreEqual("Index", result.RouteValues["action"]);


//        }
//        [Test]
//        public void Delete_Group_Post()
//        {
//            Group fake = new Group()
//            {
//                GroupId = 1,
//                GroupName = "test",
//                Description = "test"
//            };
//            _groupRepository.Setup(x => x.GetById(fake.GroupId)).Returns(fake);


//            Assert.IsNotNull(fake);
//            Assert.AreEqual(1, fake.GroupId);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            var result = controller.DeleteConfirmedGroup(1) as RedirectToRouteResult;
//            Assert.AreEqual("Index", result.RouteValues["action"]);

//        }
//        [Test]
//        public void ListOfGroups_Test()
//        {
//            // Arrange      
//            IEnumerable<Group> fakeCategories = new List<Group> {
//            new Group { GroupName = "Test1", Description="Test1Desc"},
//            new Group {  GroupName= "Test2", Description="Test2Desc"},
//            new Group { GroupName = "Test3", Description="Test3Desc"}
//          }.AsEnumerable();
//            _groupRepository.Setup(x => x.GetAll()).Returns(fakeCategories);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            // Act
//            ViewResult result = controller.ListOfGroups() as ViewResult;
//            // Assert
//            Assert.IsNotNull(result, "View Result is null");
//            Assert.IsInstanceOf(typeof(IEnumerable<Group>),
//            result.ViewData.Model, "Wrong View Model");
//            var grp = result.ViewData.Model as IEnumerable<Group>;
//            Assert.AreEqual(3, grp.Count(), "Got wrong number of Groups");
//        }

//        [Test]
//        public void Joined_User_Test()
//        {
//            IEnumerable<ApplicationUser> fakeUser = new List<ApplicationUser> {            
//              new ApplicationUser{Activated=true,Email="user1@foo.com",FirstName="user1",LastName="user1",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user2@foo.com",FirstName="user2",LastName="user2",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user3@foo.com",FirstName="user3",LastName="user3",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user4@foo.com",FirstName="user4",LastName="user4",RoleId=0}
//          }.AsEnumerable();
//            _userRepository.Setup(x => x.GetAll()).Returns(fakeUser);


//            IEnumerable<GroupUser> fakeGroupUser = new List<GroupUser> {            
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=1,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=false,GroupId=1,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512ed"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=false,GroupId=1,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512ee"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=1,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512ef"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=1,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512eg"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=2,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512eh"}
           
//          }.AsEnumerable();
//            _groupUserRepository.Setup(x => x.GetAll()).Returns(fakeGroupUser);


//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            // Act
//            ViewResult result = controller.JoinedUsers(1) as ViewResult;
//            Assert.IsNotNull(result, "View Result is null");
//            Assert.IsInstanceOf(typeof(GroupMemberViewModel),
//            result.ViewData.Model, "Wrong View Model");




//        }

//        [Test]
//        public void Edit_Group_Post()
//        {
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            // Act
//            Mapper.CreateMap<GroupFormModel, Group>();
//            GroupFormModel group = new GroupFormModel()
//            {
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
//                CreatedDate = DateTime.Now,
//                Description = "Mock",
//                GroupName = "Mock",
//                GroupId = 1

//            };
//            var result = (RedirectToRouteResult)controller.EditGroup(group);
//            Assert.AreEqual("Index", result.RouteValues["action"]);

//        }

//        [Test]
//        public void Edit_Focus_Post()
//        {
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            // Act
//            Mapper.CreateMap<FocusFormModel, Focus>();
//            FocusFormModel group = new FocusFormModel()
//            {
//                FocusId = 1,
//                FocusName = "test",
//                Description = "test",
//                GroupId = 1


//            };

//            Group grp = new Group()
//            {
//                GroupId = 1,
//                GroupName = "t",
//                Description = "t"
//            };
//            _groupRepository.Setup(x => x.GetById(1)).Returns(grp);
//            var result = (RedirectToRouteResult)controller.EditFocus(group);
//            Assert.AreEqual("Index", result.RouteValues["action"]);

//        }
//        [Test]
//        public void Edit_Goal_Get_View()
//        {
//            GroupUser user = new GroupUser()
//            {
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
//                GroupId = 1,
//                GroupUserId = 1,
//                Admin = false

//            };
//            GroupGoal goal = new GroupGoal()
//            {
//                GroupGoalId = 1,
//                GroupId = 1,
//                GoalName = "t",
//                Description = "t",
//                GoalStatusId = 1,
//                GroupUserId = 1,
//                GroupUser = user,
//                StartDate = DateTime.Now,
//                EndDate = DateTime.Now.AddDays(1),
//            };
//            _groupGoalRepository.Setup(x => x.GetById(1)).Returns(goal);
//            IEnumerable<Focus> fakeFocus = new List<Focus> 
//            {
//            new Focus { FocusId = 1, FocusName="Test1",GroupId = 1},
//             new Focus { FocusId = 2, FocusName="Test2",GroupId = 1},
//            new Focus { FocusId = 3, FocusName="Test3",GroupId = 1}
//          }.AsEnumerable();
//            _focusRepository.Setup(x => x.GetMany(p => p.GroupId.Equals(1))).Returns(fakeFocus);

//            IEnumerable<Metric> fakeMatrices = new List<Metric> 
//            {
//                new Metric{MetricId=1, Type="Test1"},
//                new Metric{MetricId=2,Type="Test2"},
//                new Metric{MetricId=3,Type="Test3"}
//            }.AsEnumerable();

//            _metricRepository.Setup(x => x.GetAll()).Returns(fakeMatrices);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            Mapper.CreateMap<GroupGoal, GroupGoalFormModel>();
//            ViewResult result = controller.EditGoal(1) as ViewResult;
//            Assert.IsNotNull(result, "View Result is null");
//            Assert.IsInstanceOf(typeof(GroupGoalFormModel),
//                result.ViewData.Model, "Wrong View Model");
//        }

//        [Test]
//        public void Edit_Goal_Post()
//        {

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            Mapper.CreateMap<GroupGoalFormModel, GroupGoal>();

//            GroupGoalFormModel goal = new GroupGoalFormModel()
//            {
//                GoalName = "t",
//                GroupGoalId = 1,
//                StartDate = DateTime.Now,
//                EndDate = DateTime.Now.AddDays(1),
//                Description = "t",
//                GroupId = 1,
//                GroupUserId = 1

//            };
//            var result = (RedirectToRouteResult)controller.EditGoal(goal);
//            Assert.AreEqual("GroupGoal", result.RouteValues["action"]);



//        }

//        [Test]
//        public void Members_View()
//        {

//            IEnumerable<ApplicationUser> fakeUser = new List<ApplicationUser> {            
//              new ApplicationUser{Activated=true,Email="user1@foo.com",FirstName="user1",LastName="user1",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user2@foo.com",FirstName="user2",LastName="user2",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user3@foo.com",FirstName="user3",LastName="user3",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user4@foo.com",FirstName="user4",LastName="user4",RoleId=0}
//          }.AsEnumerable();
//            _userRepository.Setup(x => x.GetAll()).Returns(fakeUser);


//            IEnumerable<GroupUser> fakeGroupUser = new List<GroupUser> {            
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=1,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=false,GroupId=1,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512ed"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=false,GroupId=1,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512ee"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=1,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512ef"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=1,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512eg"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=2,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512eh"}
           
//          }.AsEnumerable();
//            _groupUserRepository.Setup(x => x.GetAll()).Returns(fakeGroupUser);


//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            // Act
//            ViewResult result = controller.Members(1) as ViewResult;
//            Assert.IsNotNull(result, "View Result is null");
//            Assert.IsInstanceOf(typeof(GroupMemberViewModel),
//            result.ViewData.Model, "Wrong View Model");

//        }

//        [Test]
//        public void Delete_Member()
//        {
//            GroupUser user = new GroupUser()
//            {
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
//                GroupUserId = 1,
//                GroupId = 1
//            };
//            _groupUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupUser, bool>>>())).Returns(user);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            var result = controller.DeleteMember("402bd590-fdc7-49ad-9728-40efbfe512ec", 1) as RedirectToRouteResult;
//            Assert.AreEqual("Index", result.RouteValues["action"]);
//        }

//        [Test]
//        public void Delete_Goal_Post()
//        {
//            GroupUser user = new GroupUser()
//            {
//                GroupId = 1
//            };
//            GroupGoal goal = new GroupGoal()
//            {
//                GroupGoalId = 1,
//                GroupId = 1,
//                GoalName = "t",
//                Description = "t",
//                GroupUser = user
//            };
//            _groupGoalRepository.Setup(x => x.GetById(1)).Returns(goal);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            var result = controller.DeleteConfirmed(1) as RedirectToRouteResult;
//            Assert.AreEqual("Index", result.RouteValues["action"]);

//        }

//        [Test]
//        public void Save_Update_Update_Mandatory_Test()
//        {
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            // The MVC pipeline doesn't run, so binding and validation don't run. 
//            controller.ModelState.AddModelError("", "mock error message");
//            GroupUpdateFormModel update = new GroupUpdateFormModel();
//            update.Updatemsg = string.Empty;
//            var result = controller.SaveUpdate(update) as RedirectToRouteResult;

//            Assert.IsNull(result);
//        }

//        [Test]
//        public void Save_Update_Post()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());

//            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");            //identity.Setup(x=>x.)
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);

//            Mapper.CreateMap<GroupUpdateFormModel, GroupUpdate>();

//            Metric fakeMetric = new Metric()
//            {
//                MetricId = 1,
//                Type = "%"
//            };
//            GroupGoal goal = new GroupGoal()
//            {
//                Metric = fakeMetric,
//                Target = 100,
//                GroupGoalId = 1
//            };
//            _groupGoalRepository.Setup(x => x.GetById(1)).Returns(goal);


//            IEnumerable<GroupUpdate> updt = new List<GroupUpdate> {            
//            new GroupUpdate { GroupUpdateId =1, Updatemsg = "t1",GroupGoalId =1},
//             new GroupUpdate { GroupUpdateId =2, Updatemsg = "t2",GroupGoalId =1},
//              new GroupUpdate { GroupUpdateId =3, Updatemsg = "t3",GroupGoalId =2},
            
//          }.AsEnumerable();
//            _groupUdateRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<GroupUpdate, bool>>>())).Returns(updt);

//            GroupUser grpuser = new GroupUser()
//            {
//                GroupUserId = 1,
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"
//            };
//            _groupUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupUser, bool>>>())).Returns(grpuser);

//            GroupUpdateUser updtuser = new GroupUpdateUser()
//            {
//                GroupUpdateId = 1,
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"
//            };
//            _groupUpdateUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupUpdateUser, bool>>>())).Returns(updtuser);

//            Mapper.CreateMap<GroupUpdate, GroupUpdateViewModel>();
//            GroupUpdateFormModel mock = new GroupUpdateFormModel();
//            mock.GroupId = 1;
//            mock.Updatemsg = "mock";
//            mock.GroupGoalId = 1;
//            PartialViewResult result = controller.SaveUpdate(mock) as PartialViewResult;
//            Assert.IsNotNull(result);
//            Assert.IsInstanceOf(typeof(GroupUpdateListViewModel),
//            result.ViewData.Model, "Wrong View Model");


//        }

//        [Test]
//        public void Display_Updates()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());

//            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");            //identity.Setup(x=>x.)
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
//            Metric fakeMetric = new Metric()
//            {
//                MetricId = 1,
//                Type = "%"
//            };
//            GroupGoal goal = new GroupGoal()
//            {
//                Metric = fakeMetric,
//                Target = 100
//            };
//            _groupGoalRepository.Setup(x => x.GetById(1)).Returns(goal);

//            IEnumerable<GroupUpdate> updt = new List<GroupUpdate> {            
//            new GroupUpdate { GroupUpdateId =1, Updatemsg = "t1", GroupGoalId = 1},
//             new GroupUpdate { GroupUpdateId =2, Updatemsg = "t2", GroupGoalId=2 },
//              new GroupUpdate { GroupUpdateId =3, Updatemsg = "t3", GroupGoalId=2},
            
//          }.AsEnumerable();
//            _groupUdateRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<GroupUpdate, bool>>>())).Returns(updt);

//            GroupUser grpuser = new GroupUser()
//            {
//                GroupUserId = 1,
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"
//            };
//            _groupUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupUser, bool>>>())).Returns(grpuser);

//            GroupUpdateUser updtuser = new GroupUpdateUser()
//            {
//                GroupUpdateId = 1,
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"
//            };
//            _groupUpdateUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupUpdateUser, bool>>>())).Returns(updtuser);

//            Mapper.CreateMap<GroupUpdate, GroupUpdateViewModel>();
//            // GroupController controller = new GroupController(_groupBusiness, GroupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupGoalBusiness, GroupInvitationBusiness, _securityTokenBusiness, _groupupdateBusiness, _groupcommentBusiness, _goalStatusBusiness, groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            PartialViewResult rslt = controller.DisplayUpdates(1) as PartialViewResult;
//            Assert.IsNotNull(rslt);
//            Assert.IsInstanceOf(typeof(GroupUpdateListViewModel),
//           rslt.ViewData.Model, "Wrong View Model");


//        }

//        [Test]
//        public void DisplayComments()
//        {
//            IEnumerable<ChurchComment> cmnt = new List<ChurchComment> {            

//            new ChurchComment { GroupCommentId =1, GroupUpdateId = 1,CommentText="x"},
//            new ChurchComment { GroupCommentId =2, GroupUpdateId = 1,CommentText="y"},
//            new ChurchComment { GroupCommentId =3, GroupUpdateId = 1,CommentText="z"},
             
            
//          }.AsEnumerable();

//            _groupCommentRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<ChurchComment, bool>>>())).Returns(cmnt);

//            GroupCommentUser gcuser = new GroupCommentUser()
//            {
//                GroupCommentId = 1,
//                GroupCommentUserId = 1,
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"
//            };
//            _groupCommentUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupCommentUser, bool>>>())).Returns(gcuser);
//            ApplicationUser applicationUser = GetApplicationUser();
//            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            Mapper.CreateMap<ChurchComment, GroupCommentsViewModel>();
//            PartialViewResult rslt = controller.DisplayComments(1) as PartialViewResult;
//            Assert.IsNotNull(rslt, "View Result is null");
//            Assert.IsInstanceOf(typeof(IEnumerable<GroupCommentsViewModel>),
//             rslt.ViewData.Model, "Wrong View Model");
//            var cmntsView = rslt.ViewData.Model as IEnumerable<GroupCommentsViewModel>;
//            Assert.AreEqual(3, cmntsView.Count(), "Got wrong number of Comments");
//        }

//        [Test]
//        public void SaveComment()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());



//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
//            // Arrange                    


//            // Act          
//            GroupCommentFormModel cmnt = new GroupCommentFormModel();
//            Mapper.CreateMap<GroupCommentFormModel, ChurchComment>();
//            cmnt.CommentText = "Mock";
//            var result = controller.SaveComment(cmnt) as RedirectToRouteResult;
//            // Assert 
//            Assert.AreEqual("DisplayComments", result.RouteValues["action"]);
//        }
//        [Test]
//        public void DisplayCommentCount()
//        {
//            IEnumerable<ChurchComment> cmnt = new List<ChurchComment> {            

//            new ChurchComment { GroupCommentId =1, GroupUpdateId = 1,CommentText="x"},
//            new ChurchComment { GroupCommentId =2, GroupUpdateId = 1,CommentText="y"},
//            new ChurchComment { GroupCommentId =3, GroupUpdateId = 1,CommentText="z"},
             
            
//          }.AsEnumerable();

//            _groupCommentRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<ChurchComment, bool>>>())).Returns(cmnt);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            JsonResult count = controller.DisplayCommentCount(1) as JsonResult;
//            Assert.IsNotNull(count);
//            Assert.AreEqual(3, count.Data);

//        }
//        [Test]
//        public void SearchUserForGroup()
//        {

//            IEnumerable<ApplicationUser> fakeUser = new List<ApplicationUser> {            
//              new ApplicationUser{Activated=true,Email="user1@foo.com",FirstName="user1",LastName="user1",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user2@foo.com",FirstName="user2",LastName="user2",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user3@foo.com",FirstName="user3",LastName="user3",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user4@foo.com",FirstName="user4",LastName="user4",RoleId=0}
//          }.AsEnumerable();
//            _userRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(fakeUser);

//            Group grp = new Group()
//            {
//                GroupId = 1
//            };
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            var searchString = "e";
//            JsonResult result = controller.SearchUserForGroup(searchString, 1);
//            Assert.IsNotNull(result);
//            Assert.IsInstanceOf(typeof(JsonResult), result);



//        }
//        [Test]
//        public void Invite_User()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());
//            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();
//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);


//            var userId = "402bd590-fdc7-49ad-9728-40efbfe512ec";
//            var id = 1;
//            PartialViewResult rslt = controller.InviteUser(id, userId) as PartialViewResult;

//            Assert.IsNotNull(rslt);
//            Assert.IsInstanceOf(typeof(ApplicationUser),
//              rslt.ViewData.Model, "Wrong View Model");
//            var userView = rslt.ViewData.Model as ApplicationUser;
//            Assert.AreEqual("adarsh@foo.com", userView.Email);

//        }
//        [Test]
//        public void Invite_Users()
//        {
//            Mapper.CreateMap<Group, GroupViewModel>();
//            Group grp = new Group()
//            {
//                GroupId = 1,
//                GroupName = "t",
//                Description = "t"
//            };
//            _groupRepository.Setup(x => x.GetById(1)).Returns(grp);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            ViewResult reslt = controller.InviteUsers(1) as ViewResult;
//            Assert.IsNotNull(reslt);
//            Assert.IsInstanceOf(typeof(GroupViewModel),
//               reslt.ViewData.Model, "Wrong View Model");
//            //var grpview = reslt.ViewData.Model as GroupViewModel;
//            //Assert.AreEqual("t", grpview.GroupName);

//        }
//        [Test]
//        public void Users_List()
//        {

//            IEnumerable<GroupUser> fakeGroupUser = new List<GroupUser> {            
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=1,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=false,GroupId=1,GroupUserId=2,UserId="402bd590-fdc7-49ad-9728-40efbfe512ed"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=false,GroupId=1,GroupUserId=3,UserId="402bd590-fdc7-49ad-9728-40efbfe512ee"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=1,GroupUserId=4,UserId="402bd590-fdc7-49ad-9728-40efbfe512ef"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=1,GroupUserId=5,UserId="402bd590-fdc7-49ad-9728-40efbfe512eg"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=2,GroupUserId=6,UserId="402bd590-fdc7-49ad-9728-40efbfe512eh"}
           
//          }.AsEnumerable();
//            _groupUserRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<GroupUser, bool>>>())).Returns(fakeGroupUser);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            ViewResult result = controller.UsersList(2) as ViewResult;
//            Assert.IsNotNull(result);
//            Assert.IsInstanceOfType(typeof(List<GroupUser>), result.ViewData.Model, "Wrong Model");
//            var grpusr = result.ViewData.Model as List<GroupUser>;
//            Assert.AreEqual(6, grpusr.Count(), "Got wrong number of GroupUser");


//        }
//        [Test]
//        public void Join_Group_Get_Action()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());





//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            //principal.SetupGet(x=>x.Get(((SocialGoalUser)(User.Identity)).UserId).Callback(1));

//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
//            //identity.Setup(x=>x.)
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);

//            var result = controller.JoinGroup(1) as RedirectToRouteResult;
//            Assert.AreEqual("Index", result.RouteValues["action"]);

//        }

//        [Test]
//        public void Group_Join_Request_Post_Action_Test()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());





//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            //principal.SetupGet(x=>x.Get(((SocialGoalUser)(User.Identity)).UserId).Callback(1));

//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
//            //identity.Setup(x=>x.)
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
//            Mapper.CreateMap<GroupRequestFormModel, GroupRequest>();

//            var rslt = controller.GroupJoinRequest(1) as RedirectToRouteResult;
//            Assert.AreEqual("Index", rslt.RouteValues["action"]);

//        }

//        [Test]
//        public void Goal_Status_Post_Test()
//        {
//            GoalStatus status = new GoalStatus()
//            {
//                GoalStatusId = 1,
//                GoalStatusType = "InProgress"
//            };
//            GroupGoal goal = new GroupGoal()
//            {
//                GroupId = 1,
//                GroupGoalId = 1,
//                GoalStatusId = 1,
//                GoalStatus = status

//            };
//            _groupGoalRepository.Setup(x => x.GetById(1)).Returns(goal);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            string result = controller.GoalStatus(1, 1) as string;
//            Assert.AreEqual("InProgress", result);
//        }

//        [Test]
//        public void Get_GoalReport_Test()
//        {
//            GroupGoal goal = new GroupGoal()
//            {
//                GoalStatusId = 1,
//                GroupGoalId = 1
//            };
//            _groupGoalRepository.Setup(x => x.GetById(1)).Returns(goal);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            JsonResult reslt = controller.GetGoalReport(1) as JsonResult;
//            Assert.IsNotNull(reslt);

//        }

//        [Test]
//        public void Groups_List_Test()
//        {
//            IEnumerable<Group> fake = new List<Group> {
//            new Group{ GroupName = "Test1", Description="Test1Desc"},
//            new Group { GroupName = "Test2", Description="Test2Desc"},
          
//          }.AsEnumerable();
//            _groupRepository.Setup(x => x.GetAll()).Returns(fake);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            PartialViewResult result = controller.Groupslist(0) as PartialViewResult;
//            Assert.IsNotNull(result);
//            Assert.IsInstanceOf(typeof(IEnumerable<Group>), result.ViewData.Model, "Wrong View Model");
//            var gol = result.ViewData.Model as IEnumerable<Group>;
//            Assert.AreEqual(2, gol.Count(), "Got wrong number of Groups");
//        }
//        [Test]
//        public void Accept_Group_Join_Request_Post_Action()
//        {
//            GroupInvitation invitation = new GroupInvitation()
//            {
//                ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
//                GroupId = 1
//            };
//            _groupInvitationRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupInvitation, bool>>>())).Returns(invitation);

//            GroupRequest request = new GroupRequest()
//            {
//                GroupId = 1,
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"
//            };
//            _groupRequestRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupRequest, bool>>>())).Returns(request);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            var result = controller.AcceptRequest(1, "402bd590-fdc7-49ad-9728-40efbfe512ec") as RedirectToRouteResult;
//            Assert.AreEqual("ShowAllRequests", result.RouteValues["action"]);
//        }

//        [Test]
//        public void Group_Join_Request_Reject_Post_Action_Test()
//        {
//            GroupRequest request = new GroupRequest()
//            {
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",
//                GroupId = 1
//            };
//            _groupRequestRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupRequest, bool>>>())).Returns(request);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            var result = controller.RejectRequest(1, "402bd590-fdc7-49ad-9728-40efbfe512ec") as RedirectToRouteResult;
//            Assert.AreEqual("ShowAllRequests", result.RouteValues["action"]);
//        }

//        [Test]
//        public void Show_All_Request_View()
//        {
//            IEnumerable<GroupRequest> request = new List<GroupRequest> {            
//            new GroupRequest {GroupId =1,Accepted = false},
//            new GroupRequest {GroupId =1,Accepted = false},
//            new GroupRequest {GroupId =1,Accepted = false},
            
//          }.AsEnumerable();
//            _groupRequestRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<GroupRequest, bool>>>())).Returns(request);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            Mapper.CreateMap<GroupRequest, GroupRequestViewModel>();
//            ViewResult result = controller.ShowAllRequests(1) as ViewResult;
//            Assert.IsNotNull(result);
//            Assert.AreEqual("_RequestsView", result.ViewName);
//            Assert.IsInstanceOf(typeof(IEnumerable<GroupRequestViewModel>), result.ViewData.Model, "Wrong View Model");
//            var gol = result.ViewData.Model as IEnumerable<GroupRequestViewModel>;
//            Assert.AreEqual(3, gol.Count(), "Got wrong number of Groups");


//        }

//        [Test]
//        public void Search_Goal_For_Assigning()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());

//            //userRepository.Setup(x => x.GetById(1)).Returns(user);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");            //identity.Setup(x=>x.)
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);

//            IEnumerable<GroupUser> fakeGroupUser = new List<GroupUser> {            
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=1,GroupUserId=1,UserId="402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=false,GroupId=1,GroupUserId=2,UserId="402bd590-fdc7-49ad-9728-40efbfe512ed"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=false,GroupId=1,GroupUserId=3,UserId="402bd590-fdc7-49ad-9728-40efbfe512ee"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=1,GroupUserId=4,UserId="402bd590-fdc7-49ad-9728-40efbfe512ef"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=1,GroupUserId=5,UserId="402bd590-fdc7-49ad-9728-40efbfe512eg"},
//            new GroupUser {AddedDate=DateTime.Now,Admin=true,GroupId=2,GroupUserId=6,UserId="402bd590-fdc7-49ad-9728-40efbfe512eh"}
           
//          }.AsEnumerable();
//            _groupUserRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<GroupUser, bool>>>())).Returns(fakeGroupUser);


//            IEnumerable<ApplicationUser> fakeUser = new List<ApplicationUser> {            
//              new ApplicationUser{Activated=true,Email="user1@foo.com",FirstName="user1",LastName="user1",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user2@foo.com",FirstName="user2",LastName="user2",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user3@foo.com",FirstName="user3",LastName="user3",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user4@foo.com",FirstName="user4",LastName="user4",RoleId=0}
//          }.AsEnumerable();
//            _userRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(fakeUser);

//            PartialViewResult result = controller.SearchMemberForGoalAssigning(1) as PartialViewResult;
//            Assert.IsNotNull(result);
//            Assert.AreEqual("_MembersToSearch", result.ViewName);
//            Assert.IsInstanceOf(typeof(IEnumerable<MemberViewModel>), result.ViewData.Model, "Wrong View Model");
//        }

//        [Test]
//        public void Edit_Update_Get_View()
//        {

//            GroupUpdate update = new GroupUpdate()
//            {
//                GroupUpdateId = 1,
//                Updatemsg = "abc",
//                GroupGoalId = 1,

//            };
//            _groupUdateRepository.Setup(x => x.GetById(1)).Returns(update);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            Mapper.CreateMap<GroupUpdate, GroupUpdateFormModel>();
//            PartialViewResult result = controller.EditUpdate(1) as PartialViewResult;
//            Assert.IsNotNull(result, "View Result is null");
//            Assert.IsInstanceOf(typeof(GroupUpdateFormModel),
//                result.ViewData.Model, "Wrong View Model");
//            var data = result.ViewData.Model as GroupUpdateFormModel;
//            Assert.AreEqual("abc", data.Updatemsg);
//        }


//        [Test]
//        public void Edit_Update_Post()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());


//            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);

//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();
//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
//            //GoalController controller = new GoalController(goalService, _metricBusiness, _focusBusiness, SupportBusiness, updateService, commentService, _userBusiness, _securityTokenBusiness, _supportInvitationBusiness, _goalStatusBusiness, commentUserService, updateSupportService);
//            Mapper.CreateMap<GroupUpdateFormModel, GroupUpdate>();
//            Mapper.CreateMap<GroupUpdate, GroupUpdateViewModel>();

//            Metric fakeMetric = new Metric()
//            {
//                MetricId = 1,
//                Type = "%"
//            };
//            GroupGoal goal = new GroupGoal()
//            {
//                Metric = fakeMetric,
//                Target = 100,
//                GroupGoalId = 1
//            };
//            _groupGoalRepository.Setup(x => x.GetById(1)).Returns(goal);


//            IEnumerable<GroupUpdate> updt = new List<GroupUpdate> {            
//            new GroupUpdate { GroupUpdateId =1, Updatemsg = "t1",GroupGoalId =1},
//             new GroupUpdate { GroupUpdateId =2, Updatemsg = "t2",GroupGoalId =1},
//              new GroupUpdate { GroupUpdateId =3, Updatemsg = "t3",GroupGoalId =2},
            
//          }.AsEnumerable();
//            _groupUdateRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<GroupUpdate, bool>>>())).Returns(updt);

//            GroupUser grpuser = new GroupUser()
//            {
//                GroupUserId = 1,
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"
//            };
//            _groupUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupUser, bool>>>())).Returns(grpuser);

//            GroupUpdateUser updtuser = new GroupUpdateUser()
//            {
//                GroupUpdateId = 1,
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"
//            };
//            _groupUpdateUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupUpdateUser, bool>>>())).Returns(updtuser);
//            GroupUpdateFormModel mock = new GroupUpdateFormModel();
//            mock.Updatemsg = "mock";
//            mock.GroupGoalId = 1;
//            mock.Status = 34;
//            PartialViewResult result = controller.EditUpdate(mock) as PartialViewResult;
//            Assert.IsNotNull(result);
//            Assert.IsInstanceOf(typeof(GroupUpdateListViewModel),
//            result.ViewData.Model, "Wrong View Model");
//        }

//        [Test]
//        public void Delete_Update_Get_ReturnsView()
//        {

//            GroupUpdate update = new GroupUpdate()
//            {
//                GroupUpdateId = 1,
//                Updatemsg = "abc",
//                GroupGoalId = 1,

//            };
//            _groupUdateRepository.Setup(x => x.GetById(1)).Returns(update);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            PartialViewResult result = controller.DeleteUpdate(1) as PartialViewResult;
//            Assert.IsNotNull(result, "View Result is null");
//            Assert.IsInstanceOf(typeof(GroupUpdate),
//                 result.ViewData.Model, "Wrong View Model");
//            var group = result.ViewData.Model as GroupUpdate;
//            Assert.AreEqual("abc", group.Updatemsg, "Got wrong message");

//        }

//        [Test]
//        public void Delete_Update_Post()
//        {

//            GroupUpdate update = new GroupUpdate()
//            {
//                GroupUpdateId = 1,
//                Updatemsg = "abc",
//                GroupGoalId = 1,

//            };
//            _groupUdateRepository.Setup(x => x.GetById(1)).Returns(update);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            var result = controller.DeleteConfirmedUpdate(1) as RedirectToRouteResult;
//            Assert.AreEqual("GroupGoal", result.RouteValues["action"]);

//        }


//        [Test]
//        public void Update_Supporters_Count()
//        {
//            IEnumerable<GroupUpdateSupport> updtsprt = new List<GroupUpdateSupport> {            

//            new GroupUpdateSupport { GroupUpdateSupportId =1, GroupUpdateId = 1,GroupUserId = 1},
//            new GroupUpdateSupport { GroupUpdateSupportId =2, GroupUpdateId = 1,GroupUserId = 2},
//            new GroupUpdateSupport { GroupUpdateSupportId =3, GroupUpdateId = 1,GroupUserId = 3},
          
             
            
//          }.AsEnumerable();

//            _groupUpdateSupportRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<GroupUpdateSupport, bool>>>())).Returns(updtsprt);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            int count = controller.NoOfSupports(1);
//            Assert.IsNotNull(count);
//            Assert.AreEqual("3", count.ToString());

//        }

//        [Test]
//        public void Display_Update_Supporters_Count()
//        {
//            IEnumerable<GroupUpdateSupport> updtsprt = new List<GroupUpdateSupport> {            

//            new GroupUpdateSupport { GroupUpdateSupportId =1, GroupUpdateId = 1,GroupUserId = 1},
//            new GroupUpdateSupport { GroupUpdateSupportId =2, GroupUpdateId = 1,GroupUserId = 2},
//            new GroupUpdateSupport { GroupUpdateSupportId =3, GroupUpdateId = 1,GroupUserId = 3},
          
             
            
//          }.AsEnumerable();

//            _groupUpdateSupportRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<GroupUpdateSupport, bool>>>())).Returns(updtsprt);
//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            JsonResult count = controller.DisplayUpdateSupportCount(1) as JsonResult;
//            Assert.IsNotNull(count);
//            Assert.AreEqual(3, count.Data);

//        }

//        [Test]
//        public void Supporters_Of_Updates_List()
//        {

//            IEnumerable<ApplicationUser> fakeUser = new List<ApplicationUser> {            
//              new ApplicationUser{Activated=true,Email="user1@foo.com",FirstName="user1",LastName="user1",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user2@foo.com",FirstName="user2",LastName="user2",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user3@foo.com",FirstName="user3",LastName="user3",RoleId=0},
//              new ApplicationUser{Activated=true,Email="user4@foo.com",FirstName="user4",LastName="user4",RoleId=0}
//          }.AsEnumerable();
//            _userRepository.Setup(x => x.GetAll()).Returns(fakeUser);
//            IEnumerable<GroupUpdateSupport> fake = new List<GroupUpdateSupport> {            
//            new GroupUpdateSupport { GroupUpdateSupportId =1, GroupUpdateId = 1, GroupUserId = 1},
//            new GroupUpdateSupport { GroupUpdateSupportId =2, GroupUpdateId = 1, GroupUserId = 2},
//            new GroupUpdateSupport { GroupUpdateSupportId =3, GroupUpdateId = 1, GroupUserId = 3},
          
//          }.AsEnumerable();
//            _groupUpdateSupportRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<GroupUpdateSupport, bool>>>())).Returns(fake);

//            GroupUser grpuser = new GroupUser()
//            {
//                GroupUserId = 1,
//                UserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"
//            };
//            _groupUserRepository.Setup(x => x.Get(It.IsAny<Expression<Func<GroupUser, bool>>>())).Returns(grpuser);

//            ChurchController controller = new ChurchController(_groupBusiness, _groupUserBusiness, _userBusiness, _metricBusiness, _focusBusiness, _groupgoalBusiness, _groupInvitationBusiness, _securityTokenBusiness, _groupUpdateBusiness, _groupCommentBusiness, _goalStatusBusiness, _groupRequestBusiness, _followUserBusiness, _groupCommentUserBusiness, _groupUpdateSupportBusiness, _groupUpdateUserBusiness);
//            PartialViewResult result = controller.SupportersOfUpdate(1) as PartialViewResult;
//            Assert.IsNotNull(result);
//            Assert.IsInstanceOf(typeof(GroupUpdateSupportersViewModel), result.ViewData.Model, "Wrong View Model");
//        }

//        public ApplicationUser GetApplicationUser()
//        {
//            ApplicationUser applicationUser = new ApplicationUser()
//            {
//                Activated = true,
//                Email = "adarsh@foo.com",
//                FirstName = "Adarsh",
//                LastName = "Vikraman",
//                UserName = "adarsh",
//                RoleId = 0,
//                Id = "402bd590-fdc7-49ad-9728-40efbfe512ec",
//                DateCreated = DateTime.Now,
//                LastLoginTime = DateTime.Now,
//                ProfilePicUrl = null,
//            };
//            return applicationUser;
//        }
//    }


//}
