﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Security.Principal;
//using System.Web;
//using System.Web.Mvc;
//using System.Web.Security;
//using AutoMapper;
//using CMS02.Controllers;
//using CMS02.Tests.Helpers;
//using CMS02.ViewModels;
//using CMS02_BusinessLogic;
//using CMS02_BusinessLogic.Authentication;
//using CMS02_BusinessLogic.Models;
//using CMS02_Data.Infrastructure;
//using CMS02_Models.Models;
//using CMS02_Service.Repository;
//using Moq;
//using NUnit.Framework;

//namespace CMS02.Tests.Controllers
//{
//    [TestFixture]
//    public class NotificationControllerTest
//    {

//        Mock<IChurchInvitationRepository> _groupInvitationRepository;
//        Mock<ISupportInvitationRepository> _supportInvitationRepository;
//        Mock<IFollowRequestRepository> _followRequestRepository;
//        Mock<IUserRepository> _userRepository;
//        Mock<IUserProfileRepository> _userProfileRepository;

//        Mock<IUnitOfWork> _unitOfWork;

//        IGoalBusiness _goalBusiness;
//        IUpdateBusiness _updateBusiness;
//        ICommentBusiness _commentBusiness;
//        IChurchInvitationBusiness _groupInvitationBusiness;
//        ISupportInvitationBusiness _supportInvitationBusiness;
//        IFollowRequestBusiness _followRequestBusiness;
//        IUserBusiness _userBusiness;

//        Mock<ControllerContext> _controllerContext;
//        Mock<IIdentity> _identity;
//        Mock<IPrincipal> _principal;
//        Mock<HttpContextBase> _contextBase;
//        Mock<HttpRequestBase> _httpRequest;
//        Mock<HttpResponseBase> _httpResponse;
//        Mock<GenericPrincipal> _genericPrincipal;

//        [SetUp]
//        public void SetUp()
//        {
//            _groupInvitationRepository = new Mock<IGroupInvitationRepository>();
//            _supportInvitationRepository = new Mock<ISupportInvitationRepository>();
//            _followRequestRepository = new Mock<IFollowRequestRepository>();
//            _userRepository = new Mock<IUserRepository>();
//            _userProfileRepository = new Mock<IUserProfileRepository>();


//            _unitOfWork = new Mock<IUnitOfWork>();

//            _controllerContext = new Mock<ControllerContext>();
//            _contextBase = new Mock<HttpContextBase>();
//            _principal = new Mock<IPrincipal>();
//            _identity = new Mock<IIdentity>();
//            _httpRequest = new Mock<HttpRequestBase>();
//            _httpResponse = new Mock<HttpResponseBase>();
//            _genericPrincipal = new Mock<GenericPrincipal>();

//            _groupInvitationBusiness = new ChurchInvitationBusiness(_groupInvitationRepository.Object, _unitOfWork.Object);
//            _supportInvitationBusiness = new SupportInvitationBusiness(_supportInvitationRepository.Object, _unitOfWork.Object);
//            _followRequestBusiness = new FollowRequestBusiness(_followRequestRepository.Object, _unitOfWork.Object);
//            _userBusiness = new UserBusiness(_userRepository.Object, _unitOfWork.Object, _userProfileRepository.Object);
//        }

//        [TearDown]
//        public void TearDown()
//        {
//        }
//        [Test]
//        public void Index_If_AjaxRequest_IsNull()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());

//            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);

//            NotificationController controller = new NotificationController(_goalBusiness, _updateBusiness, _commentBusiness, _groupInvitationBusiness, _supportInvitationBusiness, _followRequestBusiness, _userBusiness);


//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
//            IEnumerable<GroupInvitation> groupInvitation = new List<GroupInvitation> {
//            new GroupInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed", FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new GroupInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed",},
//            new GroupInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ee"},
           
           
//          }.AsEnumerable();

//            _groupInvitationRepository.Setup(x => x.GetAll()).Returns(groupInvitation);

//            IEnumerable<SupportInvitation> supportInvitation = new List<SupportInvitation> {
//            new SupportInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new SupportInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId ="402bd590-fdc7-49ad-9728-40efbfe512ed"},
//            new SupportInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId ="402bd590-fdc7-49ad-9728-40efbfe512ee"},
           
           
//          }.AsEnumerable();

//            _supportInvitationRepository.Setup(x => x.GetAll()).Returns(supportInvitation);

//            IEnumerable<FollowRequest> followInvitation = new List<FollowRequest> {
//            new FollowRequest{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new FollowRequest{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId ="402bd590-fdc7-49ad-9728-40efbfe512ed"},
//            new FollowRequest{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ee"},
           
           
//          }.AsEnumerable();

//            _followRequestRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<FollowRequest, bool>>>())).Returns(followInvitation);
//            Mapper.CreateMap<GroupInvitation, NotificationViewModel>();
//            Mapper.CreateMap<SupportInvitation, NotificationViewModel>();
//            Mapper.CreateMap<FollowRequest, NotificationViewModel>();
//            ViewResult result = controller.Index(1) as ViewResult;
//            Assert.IsNotNull(result);
//        }

//        [Test]
//        public void Index_If_AjaxRequest_Is_NotNull()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());

//            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);

//            NotificationController controller = new NotificationController(_goalBusiness, _updateBusiness, _commentBusiness, _groupInvitationBusiness, _supportInvitationBusiness, _followRequestBusiness, _userBusiness);


//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            _controllerContext.SetupGet(s => s.HttpContext.Request["X-Requested-With"]).Returns("XMLHttpRequest");
//            controller.ControllerContext = _controllerContext.Object;


//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
//            IEnumerable<GroupInvitation> groupInvitation = new List<GroupInvitation> {
//            new GroupInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed", FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new GroupInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed",},
//            new GroupInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ee"},
           
           
//          }.AsEnumerable();

//            _groupInvitationRepository.Setup(x => x.GetAll()).Returns(groupInvitation);

//            IEnumerable<SupportInvitation> supportInvitation = new List<SupportInvitation> {
//            new SupportInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new SupportInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId ="402bd590-fdc7-49ad-9728-40efbfe512ed"},
//            new SupportInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ee"},
           
           
//          }.AsEnumerable();

//            _supportInvitationRepository.Setup(x => x.GetAll()).Returns(supportInvitation);

//            IEnumerable<FollowRequest> followInvitation = new List<FollowRequest> {
//            new FollowRequest{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new FollowRequest{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId ="402bd590-fdc7-49ad-9728-40efbfe512ed"},
//            new FollowRequest{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ee"},
           
           
//          }.AsEnumerable();

//            _followRequestRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<FollowRequest, bool>>>())).Returns(followInvitation);
//            Mapper.CreateMap<GroupInvitation, NotificationViewModel>();
//            Mapper.CreateMap<SupportInvitation, NotificationViewModel>();
//            Mapper.CreateMap<FollowRequest, NotificationViewModel>();
//            PartialViewResult result = controller.Index(1) as PartialViewResult;
//            Assert.IsNotNull(result);
//            Assert.AreEqual("_NotificationList", result.ViewName);
//        }

//        [Test]
//        public void Get_All_Notifications()
//        {
//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());

//            _userRepository.Setup(x => x.Get(It.IsAny<Expression<Func<ApplicationUser, bool>>>())).Returns(applicationUser);

//            NotificationController controller = new NotificationController(_goalBusiness, _updateBusiness, _commentBusiness, _groupInvitationBusiness, _supportInvitationBusiness, _followRequestBusiness, _userBusiness);


//            _principal.SetupGet(x => x.Identity.Name).Returns("adarsh");
//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            string[] userRoles = { goalsetterUser.RoleName };

//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);
//            IEnumerable<GroupInvitation> groupInvitation = new List<GroupInvitation> {
//            new GroupInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed", FromUserId ="402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new GroupInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed",},
//            new GroupInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ee"},
           
           
//          }.AsEnumerable();

//            _groupInvitationRepository.Setup(x => x.GetAll()).Returns(groupInvitation);

//            IEnumerable<SupportInvitation> supportInvitation = new List<SupportInvitation> {
//            new SupportInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new SupportInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId ="402bd590-fdc7-49ad-9728-40efbfe512ed"},
//            new SupportInvitation{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ee"},
           
           
//          }.AsEnumerable();

//            _supportInvitationRepository.Setup(x => x.GetAll()).Returns(supportInvitation);

//            IEnumerable<FollowRequest> followInvitation = new List<FollowRequest> {
//            new FollowRequest{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ed",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec"},
//            new FollowRequest{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId ="402bd590-fdc7-49ad-9728-40efbfe512ed"},
//            new FollowRequest{ Accepted=false,ToUserId = "402bd590-fdc7-49ad-9728-40efbfe512ec",FromUserId = "402bd590-fdc7-49ad-9728-40efbfe512ee"},
           
           
//          }.AsEnumerable();

//            _followRequestRepository.Setup(x => x.GetMany(It.IsAny<Expression<Func<FollowRequest, bool>>>())).Returns(followInvitation);
//            Mapper.CreateMap<GroupInvitation, NotificationViewModel>();
//            Mapper.CreateMap<SupportInvitation, NotificationViewModel>();
//            Mapper.CreateMap<FollowRequest, NotificationViewModel>();
//            IEnumerable<NotificationViewModel> result = controller.GetNotifications(5, 5) as IEnumerable<NotificationViewModel>;
//            Assert.IsNotNull(result);
//        }

//        public ApplicationUser GetApplicationUser()
//        {
//            ApplicationUser applicationUser = new ApplicationUser()
//            {
//                Activated = true,
//                Email = "adarsh@foo.com",
//                FirstName = "Adarsh",
//                LastName = "Vikraman",
//                UserName = "adarsh",
//                RoleId = 0,
//                Id = "402bd590-fdc7-49ad-9728-40efbfe512ec",
//                DateCreated = DateTime.Now,
//                LastLoginTime = DateTime.Now,
//                ProfilePicUrl = null,
//            };
//            return applicationUser;
//        }


//    }
//}
