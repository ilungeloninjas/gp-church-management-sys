﻿//using System;
//using System.IO;
//using System.Linq.Expressions;
//using System.Reflection;
//using System.Security.Principal;
//using System.Web;
//using System.Web.Mvc;
//using System.Web.Security;
//using System.Web.SessionState;
//using CMS02.Controllers;
//using CMS02.Tests.Helpers;
//using CMS02_BusinessLogic;
//using CMS02_BusinessLogic.Authentication;
//using CMS02_BusinessLogic.Models;
//using CMS02_Data.Infrastructure;
//using CMS02_Models.Models;
//using CMS02_Service.Repository;
//using Moq;
//using NUnit.Framework;

//namespace CMS02.Tests.Controllers
//{
//    [TestFixture]
//    public class EmailRequestControllerTest
//    {
//        Mock<ISecurityTokenRepository> _securityTokenRepository;
//        Mock<IChurchUserRepository> _groupUserRepository;
//        Mock<ISupportRepository> _supportRepository;
//        Mock<IChurchInvitationRepository> _groupInvitationRepository;
//        Mock<IUserRepository> _userRepository;
//        Mock<IFollowUserRepository> _followUserRepository;
//        Mock<UserProfileRepository> _userProfileRepository;


//        Mock<IUnitOfWork> _unitOfWork;

//        Mock<TempDataDictionary> _tempData;
//        Mock<ControllerContext> _controllerContext;
//        Mock<IIdentity> _identity;
//        Mock<IPrincipal> _principal;
//        Mock<HttpContextBase> _contextBase;
//        Mock<HttpRequestBase> _httpRequest;
//        Mock<HttpResponseBase> _httpResponse;
//        Mock<GenericPrincipal> _genericPrincipal;

//        public ISecurityTokenBusiness SecurityTokenBusiness;
//        public IChurchUserBusiness ChurchUserBusiness;
//        public ISupportBusiness SupportBusiness;
//        public IChurchInvitationBusiness ChurchInvitationBusiness;
//        public IUserBusiness UserBusiness;


//        [SetUp]
//        public void SetUp()
//        {
//            _securityTokenRepository = new Mock<ISecurityTokenRepository>();
//            _supportRepository = new Mock<ISupportRepository>();
//            _groupInvitationRepository = new Mock<IChurchInvitationRepository>();
//            _groupUserRepository = new Mock<IChurchUserRepository>();
//            _userRepository = new Mock<IUserRepository>();
//            _followUserRepository = new Mock<IFollowUserRepository>();
//            //userProfileRepository=new Mock<UserProfileRepository>();

//            _unitOfWork = new Mock<IUnitOfWork>();

//            _tempData = new Mock<TempDataDictionary>();
//            _controllerContext = new Mock<ControllerContext>();
//            _contextBase = new Mock<HttpContextBase>();
//            _principal = new Mock<IPrincipal>();
//            _identity = new Mock<IIdentity>();
//            _httpRequest = new Mock<HttpRequestBase>();
//            _httpResponse = new Mock<HttpResponseBase>();
//            _genericPrincipal = new Mock<GenericPrincipal>();

//            SecurityTokenBusiness = new SecurityTokenBusiness(_securityTokenRepository.Object, _unitOfWork.Object);
//            GroupInvitationBusiness = new ChurchInvitationBusiness(_groupInvitationRepository.Object, _unitOfWork.Object);
//            GroupUserBusiness = new ChurchUserBusiness(_groupUserRepository.Object, _userRepository.Object, _unitOfWork.Object);
//            SupportBusiness = new SupportBusiness(_supportRepository.Object, _followUserRepository.Object, _unitOfWork.Object);
//            // _userBusiness = new UserService(userRepository.Object, unitOfWork.Object, userProfileRepository.Object);
//        }
//        [TearDown]
//        public void TearDown()
//        {

//        }

//        [Test]
//        public void Add_GroupUser()
//        {
//            Guid guidToken = Guid.NewGuid();
//            SecurityToken token = new SecurityToken()
//            {
//                SecurityTokenId = 1,
//                Token = guidToken,
//                ActualId = 1
//            };

//            _securityTokenRepository.Setup(x => x.Get(It.IsAny<Expression<Func<SecurityToken, bool>>>())).Returns(token);

//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());



//            EmailRequestController controller = new EmailRequestController(SecurityTokenBusiness, GroupUserBusiness, SupportBusiness, GroupInvitationBusiness);

//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);

//            var httprequest = new HttpRequest("", "http://yoursite/", "");
//            var stringWriter = new StringWriter();
//            var httpResponce = new HttpResponse(stringWriter);
//            var httpContext = new HttpContext(httprequest, httpResponce);

//            var sessionContainer = new HttpSessionStateContainer("id",
//                                    new SessionStateItemCollection(),
//                                    new HttpStaticObjectsCollection(),
//                                    10,
//                                    true,
//                                    HttpCookieMode.AutoDetect,
//                                    SessionStateMode.InProc,
//                                    false);

//            httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
//                        BindingFlags.NonPublic | BindingFlags.Instance,
//                        null, CallingConventions.Standard,
//                        new[] { typeof(HttpSessionStateContainer) },
//                        null)
//                        .Invoke(new object[] { sessionContainer });

//            HttpContext.Current = httpContext;
//            //HttpContext.Current.Request.Session["somevalue"];

//            controller.TempData = _tempData.Object;
//            controller.TempData["grToken"] = guidToken;
//            var result = controller.AddGroupUser() as RedirectToRouteResult;
//            Assert.AreEqual("Index", result.RouteValues["action"]);
//        }

//        [Test]
//        public void Add_Support_ToGoal()
//        {

//            Guid guidToken = Guid.NewGuid();
//            SecurityToken token = new SecurityToken()
//            {
//                SecurityTokenId = 1,
//                Token = guidToken,
//                ActualId = 1
//            };

//            _securityTokenRepository.Setup(x => x.Get(It.IsAny<Expression<Func<SecurityToken, bool>>>())).Returns(token);

//            MemoryUser user = new MemoryUser("adarsh");
//            ApplicationUser applicationUser = GetApplicationUser();
//            var userContext = new UserInfo
//            {
//                UserId = user.Id,
//                DisplayName = user.UserName,
//                UserIdentifier = applicationUser.Email,
//                RoleName = Enum.GetName(typeof(UserRoles), applicationUser.RoleId)
//            };
//            var testTicket = new FormsAuthenticationTicket(
//                1,
//                user.Id,
//                DateTime.Now,
//                DateTime.Now.Add(FormsAuthentication.Timeout),
//                false,
//                userContext.ToString());



//            EmailRequestController controller = new EmailRequestController(SecurityTokenBusiness, GroupUserBusiness, SupportBusiness, GroupInvitationBusiness);

//            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(_principal.Object);
//            _controllerContext.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
//            controller.ControllerContext = _controllerContext.Object;

//            _contextBase.SetupGet(x => x.Request).Returns(_httpRequest.Object);
//            _contextBase.SetupGet(x => x.Response).Returns(_httpResponse.Object);
//            _genericPrincipal.Setup(x => x.Identity).Returns(_identity.Object);

//            _contextBase.SetupGet(a => a.Response.Cookies).Returns(new HttpCookieCollection());

//            var formsAuthentication = new DefaultFormsAuthentication();



//            formsAuthentication.SetAuthCookie(_contextBase.Object, testTicket);

//            HttpCookie authCookie = _contextBase.Object.Response.Cookies[FormsAuthentication.FormsCookieName];

//            var ticket = formsAuthentication.Decrypt(authCookie.Value);
//            var goalsetterUser = new GpUser(ticket);
//            _principal.Setup(x => x.Identity).Returns(goalsetterUser);

//            var httprequest = new HttpRequest("", "http://yoursite/", "");
//            var stringWriter = new StringWriter();
//            var httpResponce = new HttpResponse(stringWriter);
//            var httpContext = new HttpContext(httprequest, httpResponce);

//            var sessionContainer = new HttpSessionStateContainer("id",
//                                    new SessionStateItemCollection(),
//                                    new HttpStaticObjectsCollection(),
//                                    10,
//                                    true,
//                                    HttpCookieMode.AutoDetect,
//                                    SessionStateMode.InProc,
//                                    false);

//            httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
//                        BindingFlags.NonPublic | BindingFlags.Instance,
//                        null, CallingConventions.Standard,
//                        new[] { typeof(HttpSessionStateContainer) },
//                        null)
//                        .Invoke(new object[] { sessionContainer });

//            HttpContext.Current = httpContext;

//            controller.TempData = _tempData.Object;
//            controller.TempData["goToken"] = guidToken;
//            var result = controller.AddSupportToGoal() as RedirectToRouteResult;
//            Assert.AreEqual("Index", result.RouteValues["action"]);
//        }

//        public ApplicationUser GetApplicationUser()
//        {
//            ApplicationUser applicationUser = new ApplicationUser()
//            {
//                Activated = true,
//                Email = "adarsh@foo.com",
//                FirstName = "Adarsh",
//                LastName = "Vikraman",
//                UserName = "adarsh",
//                RoleId = 0,
//                Id = "402bd590-fdc7-49ad-9728-40efbfe512ec",
//                DateCreated = DateTime.Now,
//                LastLoginTime = DateTime.Now,
//                ProfilePicUrl = null,
//            };
//            return applicationUser;
//        }
//    }
//}
