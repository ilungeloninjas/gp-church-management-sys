using System;
using CMS02_Models.Models;
using Mvc.Mailer;

namespace CMS02.Mailers
{ 
    public class UserMailer : MailerBase, IUserMailer     
    {
        public UserMailer() :
            base()
        {
            MasterName = "_Layout";
        }

        public virtual MvcMailMessage Welcome()
        {
            var mailMessage = new MvcMailMessage { Subject = "Welcome" };
            PopulateBody(mailMessage, viewName: "Welcome");
            return mailMessage;
        }


        public virtual MvcMailMessage Invite(string email, Guid groupIdToken)
        {
            var mailMessage = new MvcMailMessage { Subject = "Invite" };
            mailMessage.To.Add(email);
            ViewBag.group = "gr:" + groupIdToken;
            PopulateBody(mailMessage, viewName: "Invite");
            return mailMessage;
        }

        public virtual MvcMailMessage Support(string email, Guid goalIdToken)
        {
            var mailMessage = new MvcMailMessage { Subject = "Support My Event" };
            mailMessage.To.Add(email);
            ViewBag.goal = "go:" + goalIdToken;
            PopulateBody(mailMessage, viewName: "SupportEvent");
            return mailMessage;
        }

        public virtual MvcMailMessage ResetPassword(string email, Guid passwordResetToken)
        {
            var mailMessage = new MvcMailMessage { Subject = "Reset Password" };
            mailMessage.To.Add(email);
            ViewBag.token = "pwreset:" + passwordResetToken;
            PopulateBody(mailMessage,viewName:"PasswordReset");
            return mailMessage;
        }

        public virtual MvcMailMessage InviteNewUser(string email,Guid registrationToken)
        {
            var mailMessage = new MvcMailMessage { Subject = "Invitation to God's Prosperity" };
            mailMessage.To.Add(email);
            ViewBag.token = "reg:" + registrationToken;
            PopulateBody(mailMessage, viewName: "InviteNewUser");
            return mailMessage;
        }
        public virtual MvcMailMessage AttendNotification(string email)
        {
            var mailMessage = new MvcMailMessage { Subject = "Your Registration Has been A Success" };
            mailMessage.To.Add(email);
            PopulateBody(mailMessage, viewName: "AttendEventNotification");

            return mailMessage;
        }
    }
}