﻿using System;
using System.Collections.Generic;
using System.Linq;
using CMS02.ViewModels;
using CMS02_BusinessLogic;
using CMS02_Models.Models;

namespace CMS02.Services
{
    public class CreateNotificationList
    {

        internal IEnumerable<NotificationsViewModel> GetNotifications(string userId, IGoalBusiness goalBusiness, ICommentBusiness commentBusiness, IUpdateBusiness updateBusiness, ISupportBusiness supportBusiness, IUserBusiness userBusiness, IChurchBusiness groupBusiness, IChurchUserBusiness groupUserSevice, IChurchGoalBusiness groupGoalBusiness, IChurchCommentBusiness groupcommentBusiness, IChurchUpdateBusiness groupupdateBusiness, IFollowUserBusiness followUserBusiness, IChurchCommentUserBusiness groupCommentUserBusiness, ICommentUserBusiness commentUserBusiness, IGroupUpdateUserBusiness groupUpdateUserBusiness)
        {
            var goals = goalBusiness.GetTop20GoalsofFollowing(userId);
            var comments = commentBusiness.GetTop20CommentsOfPublicGoalFollwing(userId);
            var updates = updateBusiness.GetTop20UpdatesOfFollowing(userId);
            var groupusers = groupUserSevice.GetTop20GroupsUsers(userId);
            var supports = supportBusiness.GetTop20SupportsOfFollowings(userId);
            var groupgoals = groupGoalBusiness.GetTop20GroupsGoals(userId, groupUserSevice);
            var groupupdates = groupupdateBusiness.GetTop20Updates(userId, groupUserSevice);
            var groupComments = groupcommentBusiness.GetTop20CommentsOfPublicGoals(userId, groupUserSevice);
            var followers = followUserBusiness.GetTop20Followers(userId);
           // var groups = _groupBusiness.GetTop20Groups(groupids);

            return (from g in goals
                    select new NotificationsViewModel()
                    {
                        GoalId = g.GoalId,
                        UserId = g.UserId,
                        CreatedDate = g.CreatedDate,
                        UserName = g.User.UserName,
                        GoalName = g.GoalName,
                        Desc = g.Desc,
                        ProfilePicUrl = g.User.ProfilePicUrl,
                        Goal = g,
                        NotificationDate = DateCalculation(g.CreatedDate),
                        NotificationType = (int)NotificationType.CreatedGoal
                    })
                    .Concat(from c in comments
                            select new NotificationsViewModel()
                            {
                                CommentId = c.CommentId,
                                CreatedDate = c.CommentDate,                                
                                CommentText = c.CommentText,
                                UpdateId = c.UpdateId,
                                GoalName = c.Update.Goal.GoalName,
                                GoalId = c.Update.GoalId,
                                NumberOfComments = commentBusiness.GetCommentsByUpdate(c.UpdateId).Count(),
                                Updatemsg = updateBusiness.GetUpdate(c.UpdateId).Updatemsg,
                                NotificationDate = DateCalculation(c.CommentDate),
                                UserName =commentUserBusiness.GetUser(c.CommentId).UserName,
                                UserId = commentUserBusiness.GetUser(c.CommentId).Id,
                                ProfilePicUrl = commentUserBusiness.GetUser(c.CommentId).ProfilePicUrl,

                                NotificationType = (int)NotificationType.CommentedOnUpdate
                            })
                              .Concat(from u in updates
                                      select new NotificationsViewModel()
                                      {
                                          Update = u,
                                          UpdateId = u.UpdateId,
                                          GoalId = u.GoalId,
                                          ProfilePicUrl = u.Goal.User.ProfilePicUrl,
                                          GoalName = u.Goal.GoalName,
                                          Updatemsg = u.Updatemsg,
                                          UserId = u.Goal.UserId,
                                          UserName = u.Goal.User.UserName,
                                          NumberOfComments = commentBusiness.GetCommentsByUpdate(u.UpdateId).Count(),
                                          CreatedDate = u.UpdateDate,
                                          NotificationDate = DateCalculation(u.UpdateDate),
                                          NotificationType = (int)NotificationType.UpdatedGoal
                                      })
                                         .Concat(from gr in groupgoals
                                                 select new NotificationsViewModel()
                                                 {

                                                     ChurchGoalId = gr.ChurchGoalId,
                                                     CreatedDate = gr.CreatedDate,

                                                     UserId = gr.ChurchUser.UserId,
                                                     UserName = userBusiness.GetUser(gr.ChurchUser.UserId).UserName,
                                                     ProfilePicUrl = userBusiness.GetUser(gr.ChurchUser.UserId).ProfilePicUrl,

                                                     ChurchName = gr.Church.ChurchName,
                                                     ChurchGoalName = gr.GoalName,
                                                     ChurchId = gr.ChurchId,
                                                     NotificationDate = DateCalculation(gr.CreatedDate),
                                                     NotificationType = (int)NotificationType.CreateGroup

                                                 })
                                                .Concat(from gu in groupusers
                                                        select new NotificationsViewModel()
                                                        {
                                                            ChurchUser = gu,
                                                            ChurchUserId = gu.ChurchUserId,
                                                            
                                                            UserId = gu.UserId,
                                                            UserName =userBusiness.GetUser(gu.UserId).UserName,
                                                            ProfilePicUrl = userBusiness.GetUser(gu.UserId).ProfilePicUrl,

                                                            ChurchName = groupBusiness.GetGroup(gu.ChurchId).ChurchName,
                                                            ChurchId = gu.ChurchId,
                                                            CreatedDate = gu.AddedDate,
                                                            NotificationDate = DateCalculation(gu.AddedDate),
                                                            NotificationType = (int)NotificationType.JoinGroup
                                                        })
                                                            .Concat(from s in supports
                                                                    select new NotificationsViewModel()
                                                                    {
                                                                        Support = s,                                                                        
                                                                        SupportId = s.SupportId,
                                                                        GoalName = s.Goal.GoalName,

                                                                        ProfilePicUrl = userBusiness.GetUser(s.UserId).ProfilePicUrl,                                                                        
                                                                        UserName =userBusiness.GetUser(s.UserId).UserName,
                                                                        UserId = s.UserId,                                                                        
                                                                        CreatedDate = s.SupportedDate,
                                                                        GoalId = s.GoalId,
                                                                        NotificationDate = DateCalculation(s.SupportedDate),
                                                                        NotificationType = (int)NotificationType.SupportGoal
                                                                    })
                                                                      .Concat(from gu in groupupdates
                                                                              select new NotificationsViewModel()
                                                                              {
                                                                                  ChurchUpdate = gu,
                                                                                  ChurchUpdateId = gu.ChurchUpdateId,
                                                                                  ChurchUpdatemsg = gu.Updatemsg,

                                                                                  UserId = groupUpdateUserBusiness.GetGroupUpdateUser(gu.ChurchUpdateId).Id,
                                                                                  ProfilePicUrl = groupUpdateUserBusiness.GetGroupUpdateUser(gu.ChurchUpdateId).ProfilePicUrl,
                                                                                  UserName = groupUpdateUserBusiness.GetGroupUpdateUser(gu.ChurchUpdateId).UserName,
                                                                                  NotificationDate = DateCalculation(gu.UpdateDate),
                                                                                  CreatedDate = gu.UpdateDate,
                                                                                  ChurchGoalId = gu.ChurchGoalId,
                                                                                  ChurchId = gu.ChurchGoal.ChurchId,
                                                                                  ChurchGoalName = gu.ChurchGoal.GoalName,
                                                                                  ChurchName = gu.ChurchGoal.Church.ChurchName,
                                                                                  NotificationType = (int)NotificationType.UpdatedGroupgoal

                                                                              })
                                                                              .Concat(from gc in groupComments
                                                                                      select new NotificationsViewModel()
                                                                                      {

                                                                                          ChurchCommentId = gc.ChurchCommentId,
                                                                                          ChurchCommentText = gc.CommentText,
                                                                                          ChurchUpdateId = gc.ChurchUpdateId,

                                                                                          ChurchGoalId = gc.ChurchUpdate.ChurchGoalId,
                                                                                          ChurchGoalName = gc.ChurchUpdate.ChurchGoal.GoalName,

                                                                                          UserId = groupCommentUserBusiness.GetGroupCommentUser(gc.ChurchCommentId).Id,
                                                                                          UserName = groupCommentUserBusiness.GetGroupCommentUser(gc.ChurchCommentId).UserName,
                                                                                          ProfilePicUrl = groupCommentUserBusiness.GetGroupCommentUser(gc.ChurchCommentId).ProfilePicUrl,

                                                                                          ChurchUpdatemsg = gc.ChurchUpdate.Updatemsg,
                                                                                          ChurchId = gc.ChurchUpdate.ChurchGoal.ChurchUser.ChurchId,
                                                                                          ChurchName = gc.ChurchUpdate.ChurchGoal.Church.ChurchName,
                                                                                          NotificationDate = DateCalculation(gc.CommentDate),
                                                                                          CreatedDate = gc.CommentDate,
                                                                                          NotificationType = (int)NotificationType.CommentedonGroupUdate
                                                                                      })
                                                                                      .Concat(from f in followers
                                                                                              select new NotificationsViewModel()
                                                                                              {
                                                                                                  FollowUserId = f.FollowUserId,
                                                                                                  FromUser = f.FromUser,
                                                                                                  ToUser = f.ToUser,
                                                                                                  ProfilePicUrl = f.FromUser.ProfilePicUrl,
                                                                                                  FromUserId = f.FromUserId,
                                                                                                  ToUserId = f.ToUserId,
                                                                                                  CreatedDate = f.AddedDate,
                                                                                                  NotificationDate = DateCalculation(f.AddedDate),
                                                                                                  NotificationType = (int)NotificationType.FollowUser
                                                                                              }).OrderByDescending(n => n.CreatedDate);
        }


        internal IEnumerable<NotificationsViewModel> GetProfileNotifications(string userid, IGoalBusiness goalBusiness, ICommentBusiness commentBusiness, IUpdateBusiness updateBusiness, ISupportBusiness supportBusiness, IUserBusiness userBusiness, IChurchBusiness groupBusiness, IChurchUserBusiness groupUserSevice, IChurchGoalBusiness groupGoalBusiness, IChurchCommentBusiness groupcommentBusiness, IChurchUpdateBusiness groupupdateBusiness, ICommentUserBusiness commentUserBusiness)
        {
            var goals = goalBusiness.GetTop20Goals(userid);
            var comments = commentBusiness.GetTop20CommentsOfPublicGoals(userid);
            var updates = updateBusiness.GetTop20Updates(userid);
            var groupusers = groupUserSevice.GetTop20GroupsUsersForProfile(userid);
            var supports = supportBusiness.GetTop20Support(userid);


            return (from g in goals
                    select new NotificationsViewModel()
                    {
                        GoalId = g.GoalId,
                        UserId = g.UserId,
                        CreatedDate = g.CreatedDate,
                        UserName = g.User.UserName,
                        GoalName = g.GoalName,
                        Desc = g.Desc,
                        ProfilePicUrl = g.User.ProfilePicUrl,
                        Goal = g,
                        NotificationDate = DateCalculation(g.CreatedDate),
                        NotificationType = (int)NotificationType.CreatedGoal
                    })
                    .Concat(from c in comments
                            select new NotificationsViewModel()
                            {
                                CommentId = c.CommentId,
                                CreatedDate = c.CommentDate,

                                UserName = commentUserBusiness.GetUser(c.CommentId).UserName,
                                UserId = commentUserBusiness.GetUser(c.CommentId).Id,
                                ProfilePicUrl = commentUserBusiness.GetUser(c.CommentId).ProfilePicUrl,

                                CommentText = c.CommentText,
                                UpdateId = c.UpdateId,
                                GoalName = c.Update.Goal.GoalName,
                                GoalId = c.Update.GoalId,
                                NumberOfComments = commentBusiness.GetCommentsByUpdate(c.UpdateId).Count(),
                                Updatemsg = updateBusiness.GetUpdate(c.UpdateId).Updatemsg,
                                NotificationDate = DateCalculation(c.CommentDate),
                                NotificationType = (int)NotificationType.CommentedOnUpdate
                            })
                              .Concat(from u in updates
                                      select new NotificationsViewModel()
                                      {
                                          Update = u,
                                          UpdateId = u.UpdateId,
                                          GoalId = u.GoalId,
                                          ProfilePicUrl = u.Goal.User.ProfilePicUrl,
                                          GoalName = u.Goal.GoalName,
                                          Updatemsg = u.Updatemsg,
                                          UserId = u.Goal.UserId,
                                          UserName = u.Goal.User.UserName,
                                          NumberOfComments = commentBusiness.GetCommentsByUpdate(u.UpdateId).Count(),
                                          CreatedDate = u.UpdateDate,
                                          NotificationDate = DateCalculation(u.UpdateDate),
                                          NotificationType = (int)NotificationType.UpdatedGoal
                                      })
                                        .Concat(from s in supports
                                                select new NotificationsViewModel()
                                                {
                                                    Support = s,                                                    
                                                    SupportId = s.SupportId,
                                                    GoalName = s.Goal.GoalName,

                                                    ProfilePicUrl = userBusiness.GetUser(s.UserId).ProfilePicUrl,
                                                    UserName = userBusiness.GetUser(s.UserId).UserName,
                                                    UserId = s.UserId,                                             

                                                    CreatedDate = s.SupportedDate,
                                                    GoalId = s.GoalId,
                                                    NotificationType = (int)NotificationType.SupportGoal
                                                })
                                                .Concat(from gu in groupusers
                                                        select new NotificationsViewModel()
                                                        {
                                                            ChurchUser = gu,

                                                            ChurchUserId = gu.ChurchUserId,
                                                            
                                                            UserName = userBusiness.GetUser(gu.UserId).UserName,
                                                            ProfilePicUrl = userBusiness.GetUser(gu.UserId).ProfilePicUrl,
                                                            UserId = gu.UserId,

                                                            ChurchName = groupBusiness.GetGroup(gu.ChurchId).ChurchName,
                                                            ChurchId = gu.ChurchId,
                                                            CreatedDate = gu.AddedDate,
                                                            NotificationDate = DateCalculation(gu.AddedDate),
                                                            NotificationType = (int)NotificationType.JoinGroup
                                                        })
                                                        .OrderByDescending(n => n.CreatedDate);
        }

        private string DateCalculation(DateTime  createdDate)
        {
            string notificationdate;
            string creDate = createdDate.ToShortDateString();
            string dateNow = DateTime.Now.ToShortDateString();
             TimeSpan duration = DateTime.Now - createdDate;
            if (creDate == dateNow)
            {
               
                if (duration.Hours >= 1)
                {
                    if (duration.Hours == 1)
                    {
                        return notificationdate = duration.Hours.ToString() + " " + "Hour Ago";

                    }
                    else
                    {
                        return notificationdate = duration.Hours.ToString() + " " + "Hours Ago";
                    }
                }
                else if (duration.Minutes >= 1)
                {
                    if (duration.Minutes == 1)
                    {
                        return notificationdate = duration.Minutes.ToString() + " " + "Minute Ago";
                    }
                    else
                    {
                        return notificationdate = duration.Minutes.ToString() + " " + "Minutes Ago";
                    }
                }
                else
                {
                    return notificationdate = "Few Seconds Ago ";
                }
            }
            else
            {
                if (duration.Days == 0 && duration.Hours < 24)
                {
                    return notificationdate = duration.Hours.ToString() + " " + "Hours Ago";
                }
                else
                {
                    if (duration.Days.ToString() == "1")
                    {
                        return notificationdate = duration.Days.ToString() + " " + "Day Ago";
                    }
                    else
                    {
                        return notificationdate = duration.Days.ToString() + " " + "Days Ago";
                    }
                }
            }
            //else if (CreatedDate.Date ==  DateTime.Today.AddDays(-1).Date)
            //{
            //    return notificationdate = "Yesterday";
            //}
            //else
            //{
            //    return notificationdate = CreatedDate.ToShortDateString();
            //}
        }
    }
}