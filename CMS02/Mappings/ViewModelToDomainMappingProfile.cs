﻿using AutoMapper;
using CMS02.ViewModels;
using CMS02_Models.Models;

namespace CMS02.Mappings
{

    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<CommentFormModel, Comment>();
            Mapper.CreateMap<ChurchFormModel, Church>();
            Mapper.CreateMap<FocusFormModel, Focus>();
            Mapper.CreateMap<UpdateFormModel, Update>();
            Mapper.CreateMap<UserFormModel, ApplicationUser>();
            Mapper.CreateMap<UserProfileFormModel, UserProfile>();
            Mapper.CreateMap<ChurchGoalFormModel, ChurchGoal>();
            Mapper.CreateMap<ChurchUpdateFormModel, ChurchUpdate>();
            Mapper.CreateMap<ChurchCommentFormModel, ChurchComment>();
            Mapper.CreateMap<ChurchRequestFormModel, ChurchRequest>();
            Mapper.CreateMap<FollowRequestFormModel, FollowRequest>();
            Mapper.CreateMap<GoalFormModel, Goal>();
            Mapper.CreateMap<AttendEventFormModel, AttendEvent>();
            Mapper.CreateMap<PostUpdateFormModel, PostUpdate>();
            //Mapper.CreateMap<XViewModel, X()
            //    .ForMember(x => x.PropertyXYZ, opt => opt.MapFrom(source => source.Property1));     
        }
    }
}