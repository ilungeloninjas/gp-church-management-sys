﻿using AutoMapper;
using CMS02.ViewModels;
using CMS02_BusinessLogic.AutoMapperConverters;
using CMS02_Models.Models;
using PagedList;

namespace CMS02.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<Goal, GoalViewModel>();
            Mapper.CreateMap<Goal, GoalFormModel>();
            Mapper.CreateMap<Comment, CommentsViewModel>();
            Mapper.CreateMap<UserProfile, UserProfileFormModel>();
            Mapper.CreateMap<Church, ChurchGoalFormModel>();
            Mapper.CreateMap<Church, ChurchFormModel>();
            Mapper.CreateMap<ChurchGoal, ChurchGoalFormModel>();
            Mapper.CreateMap<ChurchInvitation, NotificationViewModel>();
            Mapper.CreateMap<SupportInvitation, NotificationViewModel>();
            Mapper.CreateMap<Church, ChurchViewModel>();
            Mapper.CreateMap<ChurchGoal, ChurchGoalViewModel>();
            Mapper.CreateMap<ChurchComment, ChurchCommentsViewModel>();
            Mapper.CreateMap<Focus, FocusViewModel>();
            Mapper.CreateMap<Focus, FocusFormModel>();
            Mapper.CreateMap<ChurchRequest, ChurchRequestViewModel>();
            Mapper.CreateMap<FollowRequest, NotificationViewModel>();
            Mapper.CreateMap<ApplicationUser, FollowersViewModel>();
            Mapper.CreateMap<ApplicationUser, FollowingViewModel>();
            Mapper.CreateMap<Update, UpdateFormModel>();
            Mapper.CreateMap<ChurchUpdate, ChurchUpdateFormModel>();
            Mapper.CreateMap<Update, UpdateViewModel>();
            Mapper.CreateMap<ChurchUpdate, ChurchUpdateViewModel>();
            Mapper.CreateMap<AttendEvent, AttendEventViewModel>();
            Mapper.CreateMap<PostUpdate, PostUpdateFormModel>();
            Mapper.CreateMap<PostUpdate, PostUpdateViewModel>();
            //Mapper.CreateMap<X, XViewModel>()
            //    .ForMember(x => x.Property1, opt => opt.MapFrom(source => source.PropertyXYZ));
            Mapper.CreateMap<Goal, GoalListViewModel>().ForMember(x => x.SupportsCount, opt => opt.MapFrom(source => source.Supports.Count))
                                                      .ForMember(x => x.UserName, opt => opt.MapFrom(source => source.User.UserName))
                                                      .ForMember(x => x.StartDate, opt => opt.MapFrom(source => source.StartDate.ToString("dd MMM yyyy")))
                                                      .ForMember(x => x.EndDate, opt => opt.MapFrom(source => source.EndDate.ToString("dd MMM yyyy")));
            Mapper.CreateMap<Church, ChurchsItemViewModel>().ForMember(x => x.CreatedDate, opt => opt.MapFrom(source => source.CreatedDate.ToString("dd MMM yyyy")));

            Mapper.CreateMap<IPagedList<Church>, IPagedList<ChurchsItemViewModel>>().ConvertUsing<PagedListConverter<Church, ChurchsItemViewModel>>();
           

        }
    }
}