﻿$(document).ready(function () {

})

function ClearResults() {
    $("#response").empty();
    $(".validation-summary-errors").empty();
    $(".validation-summary-errors").addClass("hide");
    $(".validation-summary-errors").removeClass("validation-summary-errors");
}

function ClearLoginResults() {
    $("#responseLogin").empty();
    $(".validation-summary-errors").empty();
    $(".validation-summary-errors").addClass("hide");
    $(".validation-summary-errors").removeClass("validation-summary-errors");
}
function RegSuccessful(username) {
    $("#initialReg").hide();
    $("#Register").hide();
    $("#signingIn").removeClass("hide");
    $("#signingIn").addClass("modal-body");
    window.open('/Account/Profile/?username=' + username + '&type=NewUser', '_self');
}

function SigningIn(a) {
    $("#Login").hide();
    $("#forgot").hide();
    if (a == 1){
        window.open('/Account/ChangePassword/?id='+a, '_self');
    }
    else {
        window.open('/Home/Index/', '_self');
    }
}

function ReturnToProfile(username) {
    window.open('/Account/Profile/?username=' + username, '_self');
}
