﻿using System.Web;

namespace CMS02.Helpers
{
    public static class GpSessionFacade
    {
        private const string joinGroupOrGoal = "JoinGroupOrGoal";


        public static string JoinGroupOrGoal
        {
            get
            {
                return (string)HttpContext.Current.Session[joinGroupOrGoal];
            }

            set
            {
                HttpContext.Current.Session[joinGroupOrGoal] = value;
            }
        }

        public static void Remove(string sessionVariable)
        {
            HttpContext.Current.Session.Remove(sessionVariable);
        }

        public static void Clear()
        {
            HttpContext.Current.Session.Clear();
        }
    }
}