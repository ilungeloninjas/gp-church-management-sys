﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using CMS02.Mailers;
using CMS02.Properties;
using CMS02.ViewModels;
using CMS02_BusinessLogic;
using CMS02_BusinessLogic.Extensions;
using CMS02_Models.Models;
using Microsoft.AspNet.Identity;

namespace CMS02.Controllers
{
    [Authorize]
    public class GoalController : Controller
    {
        private readonly IGoalBusiness _goalBusiness;
        private readonly IMetricBusiness _metricBusiness;
        private readonly IFocusBusiness _focusBusiness;
        private readonly ISupportBusiness _supportBusiness;
        private readonly IUpdateBusiness _updateBusiness;
        private readonly ICommentBusiness _commentBusiness;
        private readonly IUserBusiness _userBusiness;
        public readonly ISecurityTokenBusiness SecurityTokenBusiness;
        public readonly ISupportInvitationBusiness SupportInvitationBusiness;
        private readonly IGoalStatusBusiness _goalStatusBusiness;
        private readonly ICommentUserBusiness _commentUserBusiness;
        private readonly IUpdateSupportBusiness _updateSupportBusiness;

        private IUserMailer _userMailer = new UserMailer();
        public IUserMailer UserMailer
        {
            get { return _userMailer; }
            set { _userMailer = value; }
        }

        public GoalController(IGoalBusiness goalBusiness, IMetricBusiness metricBusiness, IFocusBusiness focusBusiness, ISupportBusiness supportBusiness, IUpdateBusiness updateBusiness, ICommentBusiness commentBusiness, IUserBusiness userBusiness, ISecurityTokenBusiness securityTokenBusiness, ISupportInvitationBusiness supportInvitationBusiness, IGoalStatusBusiness goalStatusBusiness, ICommentUserBusiness commentUserBusiness, IUpdateSupportBusiness updateSupportBusiness)
        {
            _goalBusiness = goalBusiness;
            SupportInvitationBusiness = supportInvitationBusiness;
            _metricBusiness = metricBusiness;
            _focusBusiness = focusBusiness;
            _supportBusiness = supportBusiness;
            _updateBusiness = updateBusiness;
            _commentBusiness = commentBusiness;
            _userBusiness = userBusiness;
            SecurityTokenBusiness = securityTokenBusiness;
            _goalStatusBusiness = goalStatusBusiness;
            _commentUserBusiness = commentUserBusiness;
            _updateSupportBusiness = updateSupportBusiness;
        }

        public ActionResult Index(int id)
        {
            var goal = _goalBusiness.GetGoal(id);
            if (goal == null)
            {
                return HttpNotFound();
            }
            var goalDetails = Mapper.Map<Goal, GoalViewModel>(goal);
            goalDetails.Supported = _supportBusiness.IsGoalSupported(id, User.Identity.GetUserId());
            var goalstatus = _goalStatusBusiness.GetGoalStatus();
            goalDetails.GoalStatuses = goalstatus.ToSelectListItems(goal.GoalStatusId);
            return View(goalDetails);
        }

        public ViewResult MyGoal()
        {
            string userid = User.Identity.GetUserId();
            var goals = _goalBusiness.GetMyGoals(userid);
            var goalDetails = Mapper.Map<IEnumerable<Goal>, IEnumerable<GoalViewModel>>(goals);
            return View(goalDetails);
        }

        public ViewResult FollowedGoal()
        {
            var followed = _supportBusiness.GetUserSupportedGoals(User.Identity.GetUserId(), _goalBusiness);
            return View(followed);
        }

        public PartialViewResult Create()
        {
            var createGoal = new GoalFormModel();
            var metrics = _metricBusiness.GetMetrics();
            var goalstatus = _goalStatusBusiness.GetGoalStatus();
            createGoal.Metrics = metrics.ToSelectListItems(-1);
            return PartialView(createGoal);
        }

        [HttpPost]
        public ActionResult Create(GoalFormModel createGoal)
        {
            Goal goal = Mapper.Map<GoalFormModel, Goal>(createGoal);
            var errors = _goalBusiness.CanAddGoal(goal, _updateBusiness).ToList();
            ModelState.AddModelErrors(errors);
            if (ModelState.IsValid)
            {
                _goalBusiness.CreateGoal(goal);
                return RedirectToAction("Index", new { id = goal.GoalId });
            }
            var metrics = _metricBusiness.GetMetrics();
            var goalstatuss = _goalStatusBusiness.GetGoalStatus();
            createGoal.Metrics = metrics.ToSelectListItems(-1);
            return View("Create", createGoal);
        }

        public ActionResult Edit(int id)
        {
            var goal = _goalBusiness.GetGoal(id);
            GoalFormModel editGoal = Mapper.Map<Goal, GoalFormModel>(goal);
            if (goal == null)
            {
                return HttpNotFound();
            }
            var metrics = _metricBusiness.GetMetrics();
            if (goal.Metric != null)
                editGoal.Metrics = metrics.ToSelectListItems(goal.Metric.MetricId);
            else
                editGoal.Metrics = metrics.ToSelectListItems(-1);
            return View(editGoal);
        }

        [HttpPost]
        public ActionResult Edit(GoalFormModel editGoal)
        {
            Goal goalToEdit = Mapper.Map<GoalFormModel, Goal>(editGoal);
            var errors = _goalBusiness.CanAddGoal(goalToEdit, _updateBusiness);
            ModelState.AddModelErrors(errors);
            if (ModelState.IsValid)
            {
                _goalBusiness.EditGoal(goalToEdit);
                return RedirectToAction("Index", new { id = editGoal.GoalId });
            }
            else
            {
                var metrics = _metricBusiness.GetMetrics();
                var goalstatus = _goalStatusBusiness.GetGoalStatus();
                var goal = _goalBusiness.GetGoal(editGoal.GoalId);
                if (goal.Metric != null)
                    editGoal.Metrics = metrics.ToSelectListItems(goal.Metric.MetricId);
                else
                    editGoal.Metrics = metrics.ToSelectListItems(-1);

                return View(editGoal);
            }
        }

        [HttpPost]
        public string GoalStatus(int id, int goalid)
        {
            var goal = _goalBusiness.GetGoal(goalid);
            goal.GoalStatusId = id;
            _goalBusiness.SaveGoal();
            string msg = goal.GoalStatus.GoalStatusType;
            return msg;
        }

        public ActionResult Delete(int id)
        {
            var goal = _goalBusiness.GetGoal(id);
            if (goal == null)
            {
                return HttpNotFound();
            }
            return View(goal);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var goal = _goalBusiness.GetGoal(id);
            if (goal == null)
            {
                return HttpNotFound();
            }

            _goalBusiness.DeleteGoal(id);
            return RedirectToAction("Index", "Home");
        }

        public PartialViewResult MyGoals()
        {
            string userid = User.Identity.GetUserId();
            var goals = _goalBusiness.GetMyGoals(userid);
            return PartialView("_MyGoalsView", goals);
        }

        public PartialViewResult GoalsFollowing()
        {
            var followed = _supportBusiness.GetUserSupportedGoals(User.Identity.GetUserId(), _goalBusiness);
            return PartialView("_FollowedGoals", followed);
        }

        public PartialViewResult DisplayUpdates(int id)
        {
            var Updates = Mapper.Map<IEnumerable<Update>, IEnumerable<UpdateViewModel>>(_updateBusiness.GetUpdatesByGoal(id));
            foreach (var item in Updates)
            {
                item.IsSupported = _updateSupportBusiness.IsUpdateSupported(item.UpdateId, User.Identity.GetUserId());
            }
            UpdateListViewModel updates = new UpdateListViewModel()
            {

                Updates = Updates,
                Metric = _goalBusiness.GetGoal(id).Metric,
                Target = _goalBusiness.GetGoal(id).Target,
                // IsSupported =  _updateSupportBusiness.IsUpdateSupported(,((SocialGoalUser)(User.Identity)).UserId)
            };
            return PartialView("_UpdateView", updates);
        }

        public ViewResult Supporters(int id)
        {
            var members = _supportBusiness.GetSupportersOfGoal(id, _userBusiness);
            GoalSupporterViewModel gsvm = new GoalSupporterViewModel() { GoalId = id, Goal = _goalBusiness.GetGoal(id), Users = members };
            return View(gsvm);
        }

        [HttpPost]
        public ActionResult SaveUpdate(UpdateFormModel newupdate)
        {
            // Update update = Mapper.Map<UpdateFormModel, Update>(newupdate);
            if (ModelState.IsValid)
            {
                Update update = Mapper.Map<UpdateFormModel, Update>(newupdate);
                update.Goal = _goalBusiness.GetGoal(newupdate.GoalId);
                var updateVal = _updateBusiness.GetHighestUpdateValue(newupdate.GoalId);
               
                if(updateVal!=null)
                {  
                    if (updateVal.Status <= newupdate.Status)
                    {
                        _updateBusiness.CreateUpdate(update);
                       
                    }
                    else
                    {
                        update.Status = -1;
                        ModelState.AddModelError(update.Updatemsg, "cannot enter");
                    }
                }
                else
                {
                    _updateBusiness.CreateUpdate(update);
                }


                     var Updates = Mapper.Map<IEnumerable<Update>, IEnumerable<UpdateViewModel>>(_updateBusiness.GetUpdatesByGoal(newupdate.GoalId));
                     foreach (var item in Updates)
                     {
                         item.IsSupported = _updateSupportBusiness.IsUpdateSupported(item.UpdateId, User.Identity.GetUserId());
                     }
                     UpdateListViewModel updates = new UpdateListViewModel()
                     {
                         Updates = Updates,
                         Metric = _goalBusiness.GetGoal(newupdate.GoalId).Metric,
                         Target = _goalBusiness.GetGoal(newupdate.GoalId).Target,
                         //IsSupported = _updateSupportBusiness.IsUpdateSupported(newupdate.UpdateId,((SocialGoalUser)(User.Identity)).UserId)
                     };
                     return PartialView("_UpdateView", updates);

            }
            return null;
        }

        public PartialViewResult InviteUser(int id)
        {
            return PartialView("_SupportInvite", _goalBusiness.GetGoal(id));
        }

        [HttpPost]
        public PartialViewResult InviteUser(int id, string userId)
        {
            SupportInvitation newInvitation = new SupportInvitation()
            {
                GoalId = id,
                FromUserId = User.Identity.GetUserId(),
                ToUserId = userId,
                SentDate = DateTime.Now
            };
            SupportInvitationBusiness.CreateSupportInvitation(newInvitation);
            var user = _userBusiness.GetUser(userId);
            return PartialView(user);
        }

        public PartialViewResult DisplayComments(int updateId)
        {
            var comments = _commentBusiness.GetCommentsByUpdate(updateId);
            IEnumerable<CommentsViewModel> commentsView = Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentsViewModel>>(comments);
            foreach (var item in commentsView)
            {
                var user = _commentUserBusiness.GetUser(item.CommentId);
                item.UserId = user.Id;
                item.User = user;
            }
            return PartialView("_CommentView", commentsView);
        }

        [HttpPost]
        public ActionResult SaveComment(CommentFormModel newcomment)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                var comment = Mapper.Map<CommentFormModel, Comment>(newcomment);
                _commentBusiness.CreateComment(comment, userId);
            }
            return RedirectToAction("DisplayComments", new { updateId = newcomment.UpdateId });
        }

        [HttpGet]
        public JsonResult DisplayCommentCount(int updId)
        {
            int commentcount = _commentBusiness.GetCommentsByUpdate(updId).Count();
            return Json(commentcount, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchUser(string username, int goalId)
        {
            return Json(from g in _supportBusiness.SearchUserToSupport(username, goalId, _userBusiness, SupportInvitationBusiness,User.Identity.GetUserId())
                        select new { label = g.UserName + ", " + g.Email, value = g.UserName, id = g.Id }, JsonRequestBehavior.AllowGet);
        }

        public int NoOfComments(int id)
        {
            return _commentBusiness.GetCommentcount(id);
        }

        public IEnumerable<Goal> SearchGoal(string name)
        {
            var goals = Mapper.Map<IEnumerable<Goal>, IEnumerable<GoalViewModel>>(_goalBusiness.SearchGoal(name)).ToList();
            goals.ForEach(g => g.Supported = g.UserId != User.Identity.GetUserId() ? (bool?)_supportBusiness.IsGoalSupported(g.GoalId, User.Identity.GetUserId()) : null);
            return (IEnumerable<Goal>)goals;
        }

        public void SupportGoal(int id)
        {
            _supportBusiness.CreateSupport(new Support() { UserId = User.Identity.GetUserId(), GoalId = id, SupportedDate = DateTime.Now });
        }

        public ActionResult SupportGoalNow(int id)
        {
            _supportBusiness.CreateUserSupport(new Support() { UserId = User.Identity.GetUserId(), GoalId = id, SupportedDate = DateTime.Now }, SupportInvitationBusiness);
            return RedirectToAction("Index", new { id = id });
        }

        public void UnSupportGoal(int id)
        {
            _supportBusiness.DeleteSupport(id, User.Identity.GetUserId());
        }

        public PartialViewResult SupportInvitation(int goalId)
        {
            var goal = _goalBusiness.GetGoal(goalId);
            return PartialView("_SupportInvite", goal);
        }

        [HttpPost]
        public string InviteEmail(InviteEmailFormModel inviteEmail)
        {
            var user = _userBusiness.GetUsersByEmail(inviteEmail.Email);
            if (user != null)
            {

                if (!_supportBusiness.CanInviteUser(user.Id, inviteEmail.GrouporGoalId))
                {
                    return Resources.PersonSupporting;
                }
            }
            if (ModelState.IsValid)
            {
                Guid goalIdToken = Guid.NewGuid();
                SecurityToken goalIdSecurity = new SecurityToken()
                {
                    Token = goalIdToken,
                    ActualId = inviteEmail.GrouporGoalId
                };
                SecurityTokenBusiness.CreateSecurityToken(goalIdSecurity);
                UserMailer.Support(inviteEmail.Email, goalIdToken).Send();
                return Resources.InvitationSent;

            }
            else
            {
                return Resources.WrongEmail;
            }
        }

        public PartialViewResult Reportpage(int id)
        {
            return PartialView();
        }

        public JsonResult GetGoalReport(int id)
        {
            var goal = _goalBusiness.GetGoal(id);
            return Json(new
            {
                Data = (from g in _updateBusiness.GetUpdatesWithStatus(id).OrderBy(u => u.UpdateDate)
                        select new { Date = g.UpdateDate.ToString(), Value = g.Status }),
                Target = new { EndDate = goal.EndDate.ToString(), Target = (goal.Target != null) ? goal.Target : 100 }
            }, JsonRequestBehavior.AllowGet);
        }

        public double GoalProgress(int id)
        {
            return _updateBusiness.Progress(id);
        }

        public ActionResult ListOfGoals()
        {
            var goals = _goalBusiness.GetGoals();
            return View(goals);
        }

        public ActionResult Goalslist(int sortby, int filter)
        {
            if (sortby == 0 && filter == 0)
            {
                var goals = _goalBusiness.GetGoals();
                return PartialView("_Goalslist", goals);
            }
            else if (sortby == 0 && filter == 1)
            {

                var goal = _goalBusiness.GetGoalsofFollowing(User.Identity.GetUserId());
                return PartialView("_Goalslist", goal);
            }
            else if (sortby == 0 && filter == 2)
            {
                var goal = _goalBusiness.GetGoalByDefault(User.Identity.GetUserId());
                return PartialView("_Goalslist", goal);
            }
            else if (sortby == 0 && filter == 3)
            {
                var goal = _supportBusiness.GetUserSupportedGoals(User.Identity.GetUserId(), _goalBusiness);
                return PartialView("_Goalslist", goal);
            }
            else if (sortby == 1 && filter == 0)
            {
                var goal = _goalBusiness.GetPublicGoalsbyPopularity();
                return PartialView("_Goalslist", goal);
            }
            else if (sortby == 1 && filter == 1)
            {
                var goals = _goalBusiness.GetGoalsofFollowingByPopularity(User.Identity.GetUserId());
                return PartialView("_Goalslist", goals);

            }
            else if (sortby == 1 && filter == 2)
            {
                var goals = _goalBusiness.GetGoalsbyPopularity(User.Identity.GetUserId());

                return PartialView("_Goalslist", goals);
            }
            else if (sortby == 1 && filter == 3)
            {
                var goals = _supportBusiness.GetUserSupportedGoalsByPopularity(User.Identity.GetUserId(), _goalBusiness);
                return PartialView("_Goalslist", goals);
            }
            return PartialView();
        }


        /// <summary>
        /// Action to load goal list
        /// </summary>
        /// <param name="sortBy"></param>
        /// <param name="filterBy"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        /// 


        public ActionResult GoalList(string sortBy = "Date", string filterBy = "All", int page = 0)
        {
            var goals = _goalBusiness.GetGoalsByPage(User.Identity.GetUserId(), page, 5, sortBy, filterBy).ToList();

            var goalsViewModel = Mapper.Map<IEnumerable<Goal>, IEnumerable<GoalListViewModel>>(goals).ToList();
            var goalsList = new GoalsPageViewModel(filterBy, sortBy);
            goalsList.GoalList = goalsViewModel;

            if (Request.IsAjaxRequest())
            {
                return Json(goalsViewModel, JsonRequestBehavior.AllowGet);
            }
            return View("ListOfGoals", goalsList);
        }

        public ActionResult EditUpdate(int id)
        {
            var update = _updateBusiness.GetUpdate(id);
            UpdateFormModel editUpdate = Mapper.Map<Update, UpdateFormModel>(update);
            if (update == null)
            {
                return HttpNotFound();
            }
            return PartialView("_EditUpdate", editUpdate);
        }

        [HttpPost]
        public ActionResult EditUpdate(UpdateFormModel newupdate)
        {
            Update update = Mapper.Map<UpdateFormModel, Update>(newupdate);
            if (ModelState.IsValid)
            {
                update.Goal = _goalBusiness.GetGoal(newupdate.GoalId);
                _updateBusiness.EditUpdate(update);
                var Updates = Mapper.Map<IEnumerable<Update>, IEnumerable<UpdateViewModel>>(_updateBusiness.GetUpdatesByGoal(newupdate.GoalId));
                foreach (var item in Updates)
                {
                    item.IsSupported = _updateSupportBusiness.IsUpdateSupported(item.UpdateId, User.Identity.GetUserId());
                }
                UpdateListViewModel updates = new UpdateListViewModel()
                {
                    Updates = Updates,
                    Metric = _goalBusiness.GetGoal(newupdate.GoalId).Metric,
                    Target = _goalBusiness.GetGoal(newupdate.GoalId).Target
                };
                return PartialView("_UpdateView", updates);
            }
            return null;
        }

        public ActionResult DeleteUpdate(int id)
        {
            var update = _updateBusiness.GetUpdate(id);

            if (update == null)
            {
                return HttpNotFound();
            }
            return PartialView("_DeleteUpdate", update);
        }

        [HttpPost, ActionName("DeleteUpdate")]
        public ActionResult DeleteConfirmedUpdate(int id)
        {
            var update = _updateBusiness.GetUpdate(id);
            if (update == null)
            {
                return HttpNotFound();
            }

            _updateBusiness.DeleteUpdate(id);
            return RedirectToAction("Index", new { id = update.GoalId });
        }

        public void SupportUpdate(int id)
        {
            _updateSupportBusiness.CreateSupport(new UpdateSupport() { UserId = User.Identity.GetUserId(), UpdateId = id, UpdateSupportedDate = DateTime.Now });
        }


        public int NoOfSupports(int id)
        {
            return _updateSupportBusiness.GetSupportcount(id);
        }


        public void UnSupportUpdate(int id)
        {
            _updateSupportBusiness.DeleteSupport(id, User.Identity.GetUserId());
        }

        [HttpGet]
        public JsonResult DisplayUpdateSupportCount(int id)
        {
            int supportcount = _updateSupportBusiness.GetSupportcount(id);
            return Json(supportcount, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult SupportersOfUpdate(int id)
        {
            var supporters = _updateSupportBusiness.GetSupportersOfUpdate(id, _userBusiness);
            UpdateSupportersViewModel usvm = new UpdateSupportersViewModel() { UpdateId = id, Update = _updateBusiness.GetUpdate(id), Users = supporters };
            return PartialView(usvm);
        }

        //public PartialViewResult Cancel(int id)
        //{

        //}
    }
}