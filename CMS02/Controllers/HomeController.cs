﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CMS02.Services;
using CMS02.ViewModels;
using CMS02_BusinessLogic;
using Microsoft.AspNet.Identity;

namespace CMS02.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        IMetricBusiness _metricBusiness;
        IFocusBusiness _focusBusiness;
        IGoalBusiness _goalBusiness;
        ICommentBusiness _commentBusiness;
        IUpdateBusiness _updateBusiness;
        ISupportBusiness _supportBusiness;
        IUserBusiness _userBusiness;
        IChurchBusiness _groupBusiness;
        IChurchUserBusiness _groupUserBusiness;
        IChurchGoalBusiness _groupGoalBusiness;
        IChurchUpdateBusiness _groupupdateBusiness;
        IChurchCommentBusiness _groupcommentBusiness;
        IFollowUserBusiness _followUserBusiness;
        ICommentUserBusiness _commentUserBusiness;
        IChurchCommentUserBusiness _groupCommentUserBusiness;
        IGroupUpdateUserBusiness _groupUpdateUserBusiness;
        private readonly CreateNotificationList _notificationListCreation = new CreateNotificationList();

        public HomeController(IMetricBusiness metricBusiness, IFocusBusiness focusBusiness, IGoalBusiness goalBusiness, ICommentBusiness commentBusiness, IUpdateBusiness updateBusiness, ISupportBusiness supportBusiness, IUserBusiness userBusiness, IChurchUserBusiness groupUserBusiness, IChurchBusiness groupBusiness, IChurchGoalBusiness groupGoalBusiness, IChurchUpdateBusiness groupupdateBusiness, IChurchCommentBusiness groupcommentBusiness, IFollowUserBusiness followUserBusiness, IGroupUpdateUserBusiness groupUpdateUserBusiness, IChurchCommentUserBusiness groupCommentUserBusiness, ICommentUserBusiness commentUserBusiness)
        {
            _metricBusiness = metricBusiness;
            _focusBusiness = focusBusiness;
            _goalBusiness = goalBusiness;
            _commentBusiness = commentBusiness;
            _updateBusiness = updateBusiness;
            _supportBusiness = supportBusiness;
            _userBusiness = userBusiness;
            _groupBusiness = groupBusiness;
            _groupUserBusiness = groupUserBusiness;
            _groupGoalBusiness = groupGoalBusiness;
            _groupupdateBusiness = groupupdateBusiness;
            _groupcommentBusiness = groupcommentBusiness;
            _followUserBusiness = followUserBusiness;
            _groupCommentUserBusiness = groupCommentUserBusiness;
            _groupUpdateUserBusiness = groupUpdateUserBusiness;
            _commentUserBusiness = commentUserBusiness;
        }

        /// <summary>
        /// returns all notifications on first load
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult Index(int page = 0)     
        {
            int noOfRecords = 10;
            HomeViewModel dashboard = new HomeViewModel()
            {
                Notification = GetNotifications(page, noOfRecords),
                Count = GetNotifications(page, noOfRecords).Count()
            };

            if (Request.IsAjaxRequest())
            {
                if (dashboard.Count != 0)
                    return PartialView("Notification", dashboard);
                else
                    return null;
            }
            return View(dashboard);
        }

        public ViewResult About()
        {
            //ViewBag.Message = "Your quintessential app description page.";

            return View();
        }

        public ViewResult Contact()
        {
            //ViewBag.Message = "Your quintessential contact page.";

            return View();
        }

        public PartialViewResult UserNotification(string id)
        {
            HomeViewModel dashboard = new HomeViewModel()
            {
                Notification = GetNotifications(id)
            };
            return PartialView("Notification", dashboard);
        }


        /// <summary>
        /// get notifications paged
        /// </summary>
        /// <param name="?"></param>
        /// <param name="?"></param>
        /// <param name="page"></param>
        /// <param name="noOfRecords"></param>
        /// <returns></returns>
        public IEnumerable<NotificationsViewModel> GetNotifications(int page, int noOfRecords)
        {
            var notifications = _notificationListCreation.GetNotifications(User.Identity.GetUserId(), _goalBusiness, _commentBusiness, _updateBusiness, _supportBusiness, _userBusiness, _groupBusiness, _groupUserBusiness, _groupGoalBusiness, _groupcommentBusiness, _groupupdateBusiness, _followUserBusiness, _groupCommentUserBusiness, _commentUserBusiness, _groupUpdateUserBusiness);

            var skipNotifications = noOfRecords * page;
            notifications = notifications.Skip(skipNotifications).Take(noOfRecords);

            return notifications;
        }

        public IEnumerable<NotificationsViewModel> GetNotifications(string userid)
        {
            var notifications = _notificationListCreation.GetProfileNotifications(userid, _goalBusiness, _commentBusiness, _updateBusiness, _supportBusiness, _userBusiness, _groupBusiness, _groupUserBusiness, _groupGoalBusiness, _groupcommentBusiness, _groupupdateBusiness, _commentUserBusiness);
            return notifications;
        }
    }
}