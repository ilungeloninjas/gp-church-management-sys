﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using CMS02.ViewModels;
using CMS02_BusinessLogic;
using CMS02_Models.Models;

namespace CMS02.Controllers
{
    public class SearchController : Controller
    {
        private readonly IGoalBusiness _goalBusiness;
        private readonly IUserBusiness _userBusiness;
        private readonly IChurchBusiness _groupBusiness;

        public SearchController(IGoalBusiness goalBusiness, IUserBusiness userBusiness, IChurchBusiness groupBusiness)
        {
            _goalBusiness = goalBusiness;
            _userBusiness = userBusiness;
            _groupBusiness = groupBusiness;
        }

        public ViewResult SearchAll(string searchText)
        {
            SearchViewModel searchViewModel = new SearchViewModel()
            {
                Goals = Mapper.Map<IEnumerable<Goal>, IEnumerable<GoalViewModel>>(_goalBusiness.SearchGoal(searchText)),
                Users = _userBusiness.SearchUser(searchText),
                Churchs = Mapper.Map<IEnumerable<Church>, IEnumerable<ChurchViewModel>>(_groupBusiness.SearchGroup(searchText)),
                SearchText = searchText
            };
            ViewBag.searchtext = searchText;
            return View("SearchResult", searchViewModel);
        }

    }
}