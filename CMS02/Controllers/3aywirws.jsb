﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using CMS02.Mailers;
using CMS02.Properties;
using CMS02.ViewModels;
using CMS02_BusinessLogic;
using CMS02_BusinessLogic.Extensions;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using Microsoft.AspNet.Identity;
using PagedList;
using CMS02.Models;

namespace CMS02.Controllers
{
    [Authorize]
    public class ChurchController : Controller
    {
        private readonly IChurchBusiness _groupBusiness;
        private readonly IChurchUserBusiness _groupUserBusiness;
        private readonly IMetricBusiness _metricBusiness;
        private readonly IFocusBusiness _focusBusiness;
        private readonly IChurchGoalBusiness _groupGoalBusiness;
        private readonly IUserBusiness _userBusiness;
        private readonly IChurchInvitationBusiness _groupInvitationBusiness;
        private readonly ISecurityTokenBusiness _securityTokenBusiness;
        private readonly IChurchUpdateBusiness _groupUpdateBusiness;
        private readonly IChurchCommentBusiness _groupCommentBusiness;
        private readonly IGoalStatusBusiness _goalStatusBusiness;
        public readonly IChurchRequestBusiness GroupRequestBusiness;
        private readonly IFollowUserBusiness _followUserBusiness;
        private readonly IChurchCommentUserBusiness _groupCommentUserBusiness;
        private readonly IChurchUpdateSupportBusiness _groupUpdateSupportBusiness;
        private readonly IGroupUpdateUserBusiness _groupUpdateUserBusiness;
        private readonly IAttendEventBusiness _attendEventBusiness;

        private IUserMailer _userMailer = new UserMailer();
        public IUserMailer UserMailer
        {
            get { return _userMailer; }
            set { _userMailer = value; }
        }

        public ChurchController(IAttendEventBusiness attendEventBusiness, IChurchBusiness groupBusiness, IChurchUserBusiness groupUserBusiness, IUserBusiness userBusiness, IMetricBusiness metricBusiness, IFocusBusiness focusBusiness, IChurchGoalBusiness groupgoalBusiness, IChurchInvitationBusiness groupInvitationBusiness, ISecurityTokenBusiness securityTokenBusiness, IChurchUpdateBusiness groupUpdateBusiness, IChurchCommentBusiness groupCommentBusiness, IGoalStatusBusiness goalStatusBusiness, IChurchRequestBusiness groupRequestBusiness, IFollowUserBusiness followUserBusiness, IChurchCommentUserBusiness groupCommentUserBusiness, IChurchUpdateSupportBusiness groupUpdateSupportBusiness, IGroupUpdateUserBusiness groupUpdateUserBusiness)
        {
            _groupBusiness = groupBusiness;
            _groupInvitationBusiness = groupInvitationBusiness;
            _userBusiness = userBusiness;
            _groupUserBusiness = groupUserBusiness;
            _metricBusiness = metricBusiness;
            _focusBusiness = focusBusiness;
            _groupGoalBusiness = groupgoalBusiness; ;
            _securityTokenBusiness = securityTokenBusiness;
            _groupUpdateBusiness = groupUpdateBusiness;
            _groupCommentBusiness = groupCommentBusiness;
            _goalStatusBusiness = goalStatusBusiness;
            GroupRequestBusiness = groupRequestBusiness;
            _followUserBusiness = followUserBusiness;
            _groupCommentUserBusiness = groupCommentUserBusiness;
            _groupUpdateSupportBusiness = groupUpdateSupportBusiness;
            _groupUpdateUserBusiness = groupUpdateUserBusiness;
            _attendEventBusiness = attendEventBusiness;
        }
        //
        // GET: /Group/

        public ViewResult Index(int id)
        {
            ChurchViewModel group = Mapper.Map<Church, ChurchViewModel>(_groupBusiness.GetGroup(id));
            group.Goals = Mapper.Map<IEnumerable<ChurchGoal>, IEnumerable<ChurchGoalViewModel>>(_groupGoalBusiness.GetGroupGoals(id));


            foreach (var item in group.Goals)
            {
                var user = _userBusiness.GetUser(item.ChurchUser.UserId);

                var upcomingEvents = group.Goals.Where(e => e.StartDate > DateTime.Now);
                var passedEvents = group.Goals.Where(e => e.EndDate <= DateTime.Now);

                item.UserId = user.Id;
                item.User = user;
            }

            var assignedgroupuser = _groupUserBusiness.GetGroupUser(User.Identity.GetUserId(), id);
            if (assignedgroupuser != null)
            {
                group.GoalsAssignedToOthers = Mapper.Map<IEnumerable<ChurchGoal>, IEnumerable<ChurchGoalViewModel>>(_groupGoalBusiness.GetAssignedGoalsToOthers(assignedgroupuser.ChurchUserId));
                group.GoalsAssignedToMe = Mapper.Map<IEnumerable<ChurchGoal>, IEnumerable<ChurchGoalViewModel>>(_groupGoalBusiness.GetAssignedGoalsToMe(assignedgroupuser.ChurchUserId));
            }
            group.Focus = _focusBusiness.GetFocussOfGroup(id);

            //group.GroupUserId = GroupUserBusiness.GetGroupUserByuserId(((GpUser)(User.Identity)).UserId).GroupUserId;
            group.Users = _groupUserBusiness.GetMembersOfGroup(id);
            //if (group.GroupUser.UserId == ((GpUser)(User.Identity)).UserId)
            if (_groupUserBusiness.GetAdminId(id) == User.Identity.GetUserId())
                group.Admin = true;
            var status = 0;
            foreach (var item in group.Users)
            {
                if (item.Id == (User.Identity.GetUserId()))
                    status = 1;
            }
            if (status == 1)
                group.IsAMember = true;
            if (GroupRequestBusiness.RequestSent((User.Identity.GetUserId()), id))
                group.RequestSent = true;
            if (_groupInvitationBusiness.IsUserInvited(id, (User.Identity.GetUserId())))
                group.InvitationSent = true;
            return View("Index", group);
        }

        public ViewResult EventIndex(int id)
        {
            ChurchViewModel group = Mapper.Map<Church, ChurchViewModel>(_groupBusiness.GetGroup(id));
            group.Goals = Mapper.Map<IEnumerable<ChurchGoal>, IEnumerable<ChurchGoalViewModel>>(_groupGoalBusiness.GetGroupGoals(id));

            var upcomingEvents = group.Goals.Where(e => e.StartDate > DateTime.Now);
            var passedEvents = group.Goals.Where(e => e.EndDate <= DateTime.Now);
 
            foreach (var item in group.Goals)
            {
                var user = _userBusiness.GetUser(item.ChurchUser.UserId);


                item.UserId = user.Id;
                item.User = user;
            }
            return View(new UpcomingPassedEventsViewModel()
            {
                UpcomingEvents = upcomingEvents,
                PassedEvents = passedEvents
            });
        }

        public ViewResult MyGroups()
        {
            var groupids = _groupUserBusiness.GetGroupAdminUsers(User.Identity.GetUserId());
            var allGroups = _groupBusiness.GetGroups(groupids);
            return View(allGroups);
        }

        public ViewResult Following()
        {
            var groupids = _groupUserBusiness.GetFollowedGroups(User.Identity.GetUserId());
            var allGroups = _groupBusiness.GetGroups(groupids);
            return View(allGroups);
        }

        public ViewResult JoinedUsers(int id)
        {
            var members = _groupUserBusiness.GetMembersOfGroup(id);
            ChurchMemberViewModel gmvm = new ChurchMemberViewModel() { ChurchId = id, Church = _groupBusiness.GetGroup(id), Users = members };
            return View(gmvm);
        }

        //
        // GET: /Group/Create
        public PartialViewResult CreateGroup()
        {
            var groupFormViewModel = new ChurchFormModel();
            return PartialView(groupFormViewModel);
        }

        public ViewResult ShowAllRequests(int id)
        {
            var groupRequests = GroupRequestBusiness.GetGroupRequests(id);
            var groupRequestViewModel = Mapper.Map<IEnumerable<ChurchRequest>, IEnumerable<ChurchRequestViewModel>>(groupRequests);
            ViewBag.CurrentGroupID = id;
            return View("_RequestsView", groupRequestViewModel);
        }

        public ViewResult Members(int id)
        {
            var members = _groupUserBusiness.GetMembersOfGroup(id);
            ChurchMemberViewModel gmvm = new ChurchMemberViewModel() { ChurchId = id, Church = _groupBusiness.GetGroup(id), Users = members };
            return View(gmvm);
        }

        public ActionResult DeleteMember(string userId, int groupId)
        {
            var membertodelete = _groupUserBusiness.GetGroupUser(userId, groupId);
            _groupUserBusiness.DeleteGroupUser(membertodelete.ChurchUserId);
            return RedirectToAction("Index", new { id = membertodelete.ChurchId });
        }

        //
        // POST: /Group/Create

        [HttpPost]
        public ActionResult CreateGroup(ChurchFormModel newGroup)
        {
            var userId = User.Identity.GetUserId();
            Church group = Mapper.Map<ChurchFormModel, Church>(newGroup);
            var errors = _groupBusiness.CanAddGroup(group).ToList();
            ModelState.AddModelErrors(errors);
            if (ModelState.IsValid)
            {
                //group.UserId = ((GpUser)(User.Identity)).UserId;
                var createdGroup = _groupBusiness.CreateGroup(group, userId);
                //var createdGroup = _groupBusiness.GetGroup(newGroup.GroupName);
                //var groupAdmin = new GroupUser { GroupId = createdGroup.GroupId, UserId = ((GpUser)(User.Identity)).UserId, Admin = true };
                //GroupUserBusiness.CreateGroupUser(groupAdmin, GroupInvitationBusiness);
                var user = new ApplicationUser();
                var userName = user.UserName;
                var msg = "";
                Sms s = new Sms();
                msg = "Hi " + userName + ", " + "your church registration was successful and the church name is " + group.ChurchName;
                s.Send_SMs(msg, user.PhoneNumber);
                return RedirectToAction("Index", new { id = createdGroup.ChurchId });
            }
            return View("CreateGroup", newGroup);
        }

        //
        // GET: /Group/Edit
        public ActionResult EditGroup(int id)
        {
            var group = _groupBusiness.GetGroup(id);
            ChurchFormModel editGroup = Mapper.Map<Church, ChurchFormModel>(group);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View("_EditGroup", editGroup);
        }

        public ViewResult CreateFocus(int id)
        {
            return View(new FocusFormModel() { ChurchId = id, Church = _groupBusiness.GetGroup(id) });
        }

        [HttpPost]
        public ActionResult CreateFocus(FocusFormModel focus)
        {
            var errors = _focusBusiness.CanAddFocus(Mapper.Map<FocusFormModel, Focus>(focus)).ToList();

            ModelState.AddModelErrors(errors);
            if (ModelState.IsValid)
            {
                Focus newFocus = Mapper.Map<FocusFormModel, Focus>(focus);
                _focusBusiness.CreateFocus(newFocus);
                //var createdfocus = _focusBusiness.GetFocus(focus.FocusName);
                return RedirectToAction("Focus", new { id = newFocus.FocusId });
            }
            return View("CreateFocus", focus);
        }

        public ActionResult Focus(int id)
        {
            FocusViewModel focus = Mapper.Map<Focus, FocusViewModel>(_focusBusiness.GetFocus(id));
            focus.ChurchGoal = Mapper.Map<IEnumerable<ChurchGoal>, IEnumerable<ChurchGoalViewModel>>(_groupGoalBusiness.GetGroupGoalsByFocus(id));
            foreach (var item in focus.ChurchGoal)
            {
                var user = _userBusiness.GetUser(item.ChurchUser.UserId);
                item.UserId = user.Id;
                item.User = user;
            }
            //Focus.GroupGoal = _groupGoalBusiness.GetGroupGoalsByFocus(id);
            focus.Users = _groupUserBusiness.GetMembersOfGroup(focus.ChurchId);
            var status = 0;
            foreach (var item in focus.Users)
            {
                if (item.Id == (User.Identity.GetUserId()))
                {
                    status = 1;
                }
            }
            if (status == 1)
            {
                focus.IsAMember = true;
            }
            return View("Focus", focus);
        }

        public ActionResult EditFocus(int id)
        {
            var focus = _focusBusiness.GetFocus(id);
            FocusFormModel editFocus = Mapper.Map<Focus, FocusFormModel>(focus);
            if (focus == null)
            {
                return HttpNotFound();
            }
            return View("EditFocus", editFocus);
        }

        public ActionResult DeleteFocus(int id)
        {
            var focus = _focusBusiness.GetFocus(id);
            if (focus == null)
            {
                return HttpNotFound();
            }
            return View(focus);
        }

        [HttpPost, ActionName("DeleteFocus")]
        public ActionResult DeleteConfirmedFocus(int id)
        {
            var focus = _focusBusiness.GetFocus(id);
            if (focus == null)
            {
                return HttpNotFound();
            }
            _focusBusiness.DeleteFocus(id);
            return RedirectToAction("Index", "Church", new { id = focus.ChurchId });
        }

        [HttpPost]
        public ActionResult EditFocus(FocusFormModel focusFormViewModel)
        {
            Focus focus = Mapper.Map<FocusFormModel, Focus>(focusFormViewModel);
            focus.Church = _groupBusiness.GetGroup(focus.ChurchId);
            var errors = _focusBusiness.CanAddFocus(focus).ToList();
            ModelState.AddModelErrors(errors);
            if (ModelState.IsValid)
            {
                _focusBusiness.UpdateFocus(focus);
                _focusBusiness.SaveFocus();
                return RedirectToAction("Index", new { id = focus.ChurchId });
            }
            return View("EditFocus", focusFormViewModel);
        }

        // POST: /Group/Edit/5
        [HttpPost]
        public ActionResult EditGroup(ChurchFormModel groupFormViewModel)
        {
            Church group = Mapper.Map<ChurchFormModel, Church>(groupFormViewModel);
            var errors = _groupBusiness.CanAddGroup(group).ToList();
            ModelState.AddModelErrors(errors);
            if (ModelState.IsValid)
            {
                _groupBusiness.UpdateGroup(group);
                return RedirectToAction("Index", new { id = group.ChurchId });
            }
            return View("_EditGroup", groupFormViewModel);
        }

        //
        // GET: /Group/Delete/5

        public ActionResult DeleteGroup(int id)
        {
            var group = _groupBusiness.GetGroup(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View("_DeleteGroup", group);
        }

        [HttpPost, ActionName("DeleteGroup")]
        public ActionResult DeleteConfirmedGroup(int id)
        {
            var group = _groupBusiness.GetGroup(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            _groupBusiness.DeleteGroup(id);
            _groupUserBusiness.DeleteGroupUserByGroupId(id);
            return RedirectToAction("Index", "Home");
        }

        //[ActionName("CreateGoal")]
        public ViewResult CreateGoal(int id)
        {
            var metrics = _metricBusiness.GetMetrics();
            var focuss = _focusBusiness.GetFocussOfGroup(id);
            var groupGoal = new ChurchGoalFormModel() { ChurchId = id };
            groupGoal.Metrics = metrics.ToSelectListItems(-1);
            groupGoal.Foci = focuss.ToSelectListItems(-1);
            return View(groupGoal);
        }
        public ActionResult CreateAttendEvent()
        {
            var churchevent = _groupGoalBusiness.GetGroupGoals();
            var attendEvents = new AttendEventFormModel();
            attendEvents.Events = churchevent.ToSelectListItems(-1);
            return View(attendEvents);
        }

        [HttpPost]
        public ActionResult CreateAttendEvent(AttendEventFormModel attendEvent)
        {            

            if (ModelState.IsValid)
            {
                AttendEvent attend = Mapper.Map<AttendEventFormModel, AttendEvent>(attendEvent);
                _attendEventBusiness.CreateAttend(attend);
                return RedirectToAction("Index", "Church");
            }

            var attendevent = _groupGoalBusiness.GetGroupGoals();
            attendEvent.Events = attendevent.ToSelectListItems(-1);
            return View(attendEvent);
        }

        [HttpPost]
        public ActionResult CreateGoal(ChurchGoalFormModel goal)
        {
            ChurchGoal groupgoal = Mapper.Map<ChurchGoalFormModel, ChurchGoal>(goal);
            var groupUser = _groupUserBusiness.GetGroupUser(User.Identity.GetUserId(), goal.ChurchId);
            groupgoal.ChurchUserId = groupUser.ChurchUserId;

            var errors = _groupGoalBusiness.CanAddGoal(groupgoal, _groupUpdateBusiness).ToList();
            ModelState.AddModelErrors(errors);
            if (ModelState.IsValid)
            {
                _groupGoalBusiness.CreateGroupGoal(groupgoal);
                return RedirectToAction("Index", new { id = goal.ChurchId });
            }
            var metrics = _metricBusiness.GetMetrics();
            var focuss = _focusBusiness.GetFocussOfGroup(goal.ChurchId);
            goal.Metrics = metrics.ToSelectListItems(-1);
            goal.Foci = focuss.ToSelectListItems(-1);
            return View(goal);
        }

        public ViewResult EditGoal(int id)
        {
            var goal = _groupGoalBusiness.GetGroupGoal(id);
            ChurchGoalFormModel editGoal = Mapper.Map<ChurchGoal, ChurchGoalFormModel>(goal);
            var metrics = _metricBusiness.GetMetrics();
            var focuss = _focusBusiness.GetFocussOfGroup(goal.ChurchUser.ChurchId);
            if (goal.Metric != null)
                editGoal.Metrics = metrics.ToSelectListItems(goal.Metric.MetricId);
            else
                editGoal.Metrics = metrics.ToSelectListItems(-1);
            if (goal.Focus != null)
                editGoal.Foci = focuss.ToSelectListItems(goal.Focus.FocusId);
            else
                editGoal.Foci = focuss.ToSelectListItems(-1);
            return View(editGoal);
        }

        [HttpPost]
        public ActionResult EditGoal(ChurchGoalFormModel editGoal)
        {
            ChurchGoal groupGoal = Mapper.Map<ChurchGoalFormModel, ChurchGoal>(editGoal);
            var errors = _groupGoalBusiness.CanAddGoal(groupGoal, _groupUpdateBusiness);
            ModelState.AddModelErrors(errors);
            if (ModelState.IsValid)
            {
                _groupGoalBusiness.EditGroupGoal(groupGoal);
                return RedirectToAction("GroupGoal", new { id = editGoal.ChurchGoalId }); ;

            }
            else
            {
                var metrics = _metricBusiness.GetMetrics();
                var focuss = _focusBusiness.GetFocuss();
                var groupGoalToEdit = _groupGoalBusiness.GetGroupGoal(editGoal.ChurchGoalId);
                editGoal.Church = _groupBusiness.GetGroup(editGoal.ChurchId);
                if (groupGoalToEdit.Metric != null)
                    editGoal.Metrics = metrics.ToSelectListItems(groupGoalToEdit.Metric.MetricId);
                else
                    editGoal.Metrics = metrics.ToSelectListItems(-1);
                if (groupGoalToEdit.Focus != null)
                    editGoal.Foci = focuss.ToSelectListItems(groupGoalToEdit.Focus.FocusId);
                else
                    editGoal.Foci = focuss.ToSelectListItems(-1);
                return View(editGoal);
            }
        }

        public ActionResult DeleteGoal(int id)
        {
            var goal = _groupGoalBusiness.GetGroupGoal(id);
            if (goal == null)
            {
                return HttpNotFound();
            }
            return View(goal);
        }

        [HttpPost, ActionName("DeleteGoal")]
        public ActionResult DeleteConfirmed(int id)
        {
            var goal = _groupGoalBusiness.GetGroupGoal(id);
            if (goal == null)
            {
                return HttpNotFound();
            }

            _groupGoalBusiness.DeleteGroupGoal(id);
            return RedirectToAction("Index", new { id = goal.ChurchId });
        }

        public ActionResult GroupGoal(int id)
        {
            var goal = _groupGoalBusiness.GetGroupGoal(id);
            if (goal == null)
            {
                return HttpNotFound();
            }
            var goalDetails = Mapper.Map<ChurchGoal, ChurchGoalViewModel>(goal);
            var user = _userBusiness.GetUser(goalDetails.ChurchUser.UserId);
            goalDetails.UserId = user.Id;
            goalDetails.User = user;
            var assignedGroupUser = _groupUserBusiness.GetGroupUser(goalDetails.AssignedChurchUserId);

            if (goalDetails.AssignedChurchUserId == 0)
            {
                goalDetails.AssignedUserId = null;
            }
            else
            {
                goalDetails.AssignedUserId = assignedGroupUser.UserId;
            }
            var goalstatus = _goalStatusBusiness.GetGoalStatus();
            goalDetails.GoalStatuses = goalstatus.ToSelectListItems(goal.GoalStatusId);

            goalDetails.Users = _groupUserBusiness.GetMembersOfGroup(goal.ChurchUser.ChurchId);
            var status = 0;
            foreach (var item in goalDetails.Users)
            {
                if (item.Id == User.Identity.GetUserId())
                {
                    status = 1;
                }
            }
            if (status == 1)
            {
                goalDetails.IsAMember = true;
            }
            return View(goalDetails);
        }

        // [HttpPost]
        public ActionResult SaveUpdate(ChurchUpdateFormModel newupdate)
        {

            if (ModelState.IsValid)
            {
                ChurchUpdate update = Mapper.Map<ChurchUpdateFormModel, ChurchUpdate>(newupdate);
                var userId = User.Identity.GetUserId();
                _groupUpdateBusiness.CreateUpdate(update, userId);
                var Updates = Mapper.Map<IEnumerable<ChurchUpdate>, IEnumerable<ChurchUpdateViewModel>>(_groupUpdateBusiness.GetUpdatesByGoal(newupdate.ChurchGoalId));
                foreach (var item in Updates)
                {
                    item.IsSupported = _groupUpdateSupportBusiness.IsUpdateSupported(item.ChurchUpdateId, User.Identity.GetUserId(), _groupUserBusiness);
                    item.UserId = _groupUpdateUserBusiness.GetGroupUpdateUser(item.ChurchUpdateId).Id;
                }
                ChurchUpdateListViewModel updates = new ChurchUpdateListViewModel()
                {
                    ChurchUpdates = Updates,
                    Metric = _groupGoalBusiness.GetGroupGoal(newupdate.ChurchGoalId).Metric,
                    Target = _groupGoalBusiness.GetGroupGoal(newupdate.ChurchGoalId).Target
                };
                return PartialView("_UpdateView", updates);

            }
            return null;
        }

        public PartialViewResult DisplayUpdates(int id)
        {
            var Updates = Mapper.Map<IEnumerable<ChurchUpdate>, IEnumerable<ChurchUpdateViewModel>>(_groupUpdateBusiness.GetUpdatesByGoal(id));

            foreach (var item in Updates)
            {


                item.IsSupported = _groupUpdateSupportBusiness.IsUpdateSupported(item.ChurchUpdateId, User.Identity.GetUserId(), _groupUserBusiness);
                item.UserId = _groupUpdateUserBusiness.GetGroupUpdateUser(item.ChurchUpdateId).Id;
            }
            ChurchUpdateListViewModel updates = new ChurchUpdateListViewModel()
            {
                ChurchUpdates = Updates,
                Metric = _groupGoalBusiness.GetGroupGoal(id).Metric,
                Target = _groupGoalBusiness.GetGroupGoal(id).Target
            };
            return PartialView("_UpdateView", updates);
        }
        public ActionResult SaveChurchUpdate(ChurchUpdateFormModel newupdate)
        {

            if (ModelState.IsValid)
            {
                ChurchUpdate update = Mapper.Map<ChurchUpdateFormModel, ChurchUpdate>(newupdate);
                var userId = User.Identity.GetUserId();
                _groupUpdateBusiness.CreateUpdate(update, userId);
                var Updates = Mapper.Map<IEnumerable<ChurchUpdate>, IEnumerable<ChurchUpdateViewModel>>(_groupUpdateBusiness.GetUpdatesByGoal(newupdate.ChurchGoalId));
                foreach (var item in Updates)
                {
                    item.IsSupported = _groupUpdateSupportBusiness.IsUpdateSupported(item.ChurchUpdateId, User.Identity.GetUserId(), _groupUserBusiness);
                    item.UserId = _groupUpdateUserBusiness.GetGroupUpdateUser(item.ChurchUpdateId).Id;
                }
                ChurchUpdateListViewModel updates = new ChurchUpdateListViewModel()
                {
                    ChurchUpdates = Updates
                };
                return PartialView("_UpdateChurchView", updates);

            }
            return null;
        }
        public PartialViewResult DisplayChurchUpdates(int id)
        {
            var Updates = Mapper.Map<IEnumerable<ChurchUpdate>, IEnumerable<ChurchUpdateViewModel>>(_groupUpdateBusiness.GetUpdatesByGoal(id));

            foreach (var item in Updates)
            {


                item.IsSupported = _groupUpdateSupportBusiness.IsUpdateSupported(item.ChurchUpdateId, User.Identity.GetUserId(), _groupUserBusiness);
                item.UserId = _groupUpdateUserBusiness.GetGroupUpdateUser(item.ChurchUpdateId).Id;
            }
            ChurchUpdateListViewModel updates = new ChurchUpdateListViewModel()
            {
                ChurchUpdates = Updates
            };
            return PartialView("_UpdateChurchView", updates);
        }
        public PartialViewResult DisplayComments(int id)
        {
            var comments = _groupCommentBusiness.GetCommentsByUpdate(id);
            IEnumerable<ChurchCommentsViewModel> commentsView = Mapper.Map<IEnumerable<ChurchComment>, IEnumerable<ChurchCommentsViewModel>>(comments);
            foreach (var item in commentsView)
            {
                var groupCommentUser = _groupCommentUserBusiness.GetCommentUser(item.ChurchCommentId);
                var user = _userBusiness.GetUser(groupCommentUser.UserId);
                item.UserId = user.Id;
                item.User = user;
            }
            return PartialView("_CommentView", commentsView);
        }

        [HttpPost]
        public ActionResult SaveComment(ChurchCommentFormModel newcomment)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                var comment = Mapper.Map<ChurchCommentFormModel, ChurchComment>(newcomment);
                _groupCommentBusiness.CreateComment(comment, userId);

            }
            return RedirectToAction("DisplayComments", new { id = newcomment.ChurchUpdateId });
        }

        [HttpGet]
        public JsonResult DisplayCommentCount(int id)
        {
            int commentcount = _groupCommentBusiness.GetCommentsByUpdate(id).Count();
            return Json(commentcount, JsonRequestBehavior.AllowGet);
        }


        public int NoOfComments(int id)
        {
            var no = _groupCommentBusiness.GetCommentcount(id);
            return (no);
        }
        public JsonResult SearchUserForGroup(string username, int groupId)
        {
            return Json(from g in _groupUserBusiness.SearchUserForGroup(username, groupId, _userBusiness, _groupInvitationBusiness)
                        select new { label = g.UserName + ", " + g.Email, value = g.UserName, id = g.Id }, JsonRequestBehavior.AllowGet);
        }

        public int NoOfUsers(int id)
        {
            return _groupUserBusiness.GetGroupUsersCount(id);
        }

        public PartialViewResult InviteUser(int id, string userId)
        {
            ChurchInvitation newInvitation = new ChurchInvitation()
            {
                ChurchId = id,
                FromUserId = User.Identity.GetUserId(),
                ToUserId = userId,
                SentDate = DateTime.Now
            };
            _groupInvitationBusiness.CreateGroupInvitation(newInvitation);
            return PartialView(_userBusiness.GetUser(userId));
        }

        [ActionName("InviteUsers")]
        public ViewResult InviteUsers(int id)
        {
            var group = _groupBusiness.GetGroup(id);
            ChurchViewModel invGroup = Mapper.Map<Church, ChurchViewModel>(group);
            return View("_InviteUsers", invGroup);
        }

        public ViewResult UsersList(int id)
        {
            var users = _groupUserBusiness.GetGroupUsersList(id).ToList();
            return View(users);
        }

        public ActionResult JoinGroup(int id)
        {
            var newGroupUser = new ChurchUser()
            {
                Admin = false,
                UserId = User.Identity.GetUserId(),
                ChurchId = id
            };
            _groupUserBusiness.CreateGroupUser(newGroupUser, _groupInvitationBusiness);
            if (GroupRequestBusiness.RequestSent(User.Identity.GetUserId(), id))
                GroupRequestBusiness.DeleteGroupRequest(User.Identity.GetUserId(), id);
            return RedirectToAction("Index", new { id = id });
        }

        public ActionResult GroupJoinRequest(int id)
        {
            var groupRequestFormModel = new ChurchRequestFormModel()
            {
                UserId = User.Identity.GetUserId(),
                ChurchId = id
            };
            var groupRequest = Mapper.Map<ChurchRequestFormModel, ChurchRequest>(groupRequestFormModel);
            GroupRequestBusiness.CreateGroupRequest(groupRequest);
            return RedirectToAction("Index", new { id = groupRequestFormModel.ChurchId });
        }

        public int GetNumberOfRequests(int id)
        {
            return GroupRequestBusiness.GetGroupRequestsForGroup(id).Count();
        }

        public ActionResult AcceptRequest(int churchId, string userId)
        {
            var newGroupUser = new ChurchUser()
            {
                Admin = false,
                UserId = userId,
                ChurchId = churchId
            };
            _groupUserBusiness.CreateGroupUserFromRequest(newGroupUser, GroupRequestBusiness);
            if (_groupInvitationBusiness.IsUserInvited(churchId, userId))
                _groupInvitationBusiness.AcceptInvitation(churchId, userId);
            return RedirectToAction("ShowAllRequests", new { id = churchId });
        }

        public ActionResult RejectRequest(int churchId, string userId)
        {
            GroupRequestBusiness.DeleteGroupRequest(userId, churchId);
            return RedirectToAction("ShowAllRequests", new { id = churchId });
        }

        public PartialViewResult GroupsView()
        {
            var groupIds = _groupUserBusiness.GetGroupAdminUsers(User.Identity.GetUserId());
            var groups = _groupBusiness.GetGroupsForUser(groupIds);
            var groupsList = Mapper.Map<IEnumerable<Church>, IEnumerable<ChurchsItemViewModel>>(groups);
            return PartialView("_GroupView", groupsList);
        }

        public ViewResult GroupViewOfUser()
        {
            return View();
        }

        public PartialViewResult FollowedGroups()
        {
            List<Church> groups = new List<Church> { };
            var groupids = _groupUserBusiness.GetFollowedGroups(User.Identity.GetUserId());
            foreach (var item in groupids)
            {
                var group = _groupBusiness.GetGroup(item);
                groups.Add(group);
            }
            return PartialView("FollowedGroups", groups);
        }

        [HttpPost]
        public string InviteEmail(InviteEmailFormModel inviteUser)
        {
            var user = _userBusiness.GetUsersByEmail(inviteUser.Email);
            if (user != null)
            {
                if (!_groupUserBusiness.CanInviteUser(user.Id, inviteUser.GrouporGoalId))
                    return Resources.PersonJoined;
            }
            if (ModelState.IsValid)
            {
                Guid groupIdToken = Guid.NewGuid();
                var groupIdSecurity = new SecurityToken()
                {
                    Token = groupIdToken,
                    ActualId = inviteUser.GrouporGoalId
                };
                _securityTokenBusiness.CreateSecurityToken(groupIdSecurity);
                UserMailer.Invite(inviteUser.Email, groupIdToken).Send();
                return Resources.InvitationSent;
            }
            else
                return Resources.WrongEmail;
        }

        public PartialViewResult Reportpage(int id)
        {
            return PartialView();
        }

        public JsonResult GetGoalReport(int id)
        {
            var goal = _groupGoalBusiness.GetGroupGoal(id);
            return Json(new
            {
                Data = (from g in _groupUpdateBusiness.GetUpdatesWithStatus(id).OrderBy(u => u.UpdateDate)
                        select new { Date = g.UpdateDate.ToString(), Value = g.Status }),
                Target = new { EndDate = goal.EndDate.ToString(), Target = (goal.Target != null) ? goal.Target : 100 }
            }, JsonRequestBehavior.AllowGet);
        }


        public string GoalStatus(int id, int goalid)
        {
            var goal = _groupGoalBusiness.GetGroupGoal(goalid);
            goal.GoalStatusId = id;
            _groupGoalBusiness.SaveGroupGoal();
            string msg = goal.GoalStatus.GoalStatusType;
            return msg;
        }

        public double GoalProgress(int id)
        {
            return _groupUpdateBusiness.Progress(id);
        }

        public ActionResult ListOfGroups()
        {
            var groups = _groupBusiness.GetGroups();
            return View(" ",groups);
        }

        public ActionResult Groupslist(int filter)
        {
            if (filter == 0)
            {
                var group = _groupBusiness.GetGroups();
                return PartialView("_Groupslist", group);
            }
            else if (filter == 1)
            {
                var userIds = _followUserBusiness.GetFollowingUsers(User.Identity.GetUserId());
                var groupIds = _groupUserBusiness.GetGroupUsers(userIds);
                var groups = _groupBusiness.GetGroups(groupIds);
                return PartialView("_Groupslist", groups);
            }
            else if (filter == 2)
            {
                var groupIds = _groupUserBusiness.GetGroupAdminUsers(User.Identity.GetUserId());
                var group = _groupBusiness.GetGroupsForUser(groupIds);
                return PartialView("_Groupslist", group);
            }
            else if (filter == 3)
            {
                var groupids = _groupUserBusiness.GetFollowedGroups(User.Identity.GetUserId());
                var allGroups = _groupBusiness.GetGroups(groupids);
                return PartialView("_Groupslist", allGroups);
            }
            return PartialView();
        }

        /// <summary>
        /// Action to load groups list 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult GroupList(ChurchFilter filter = ChurchFilter.All, int page = 1)
        {
            // Get a paged list of groups
            var groups = _groupBusiness.GetGroups(User.Identity.GetUserId(), filter, new Page(page,8));
            
            // map it to a paged list of models.
            var groupsViewModel = Mapper.Map<IPagedList<Church>, IPagedList<ChurchsItemViewModel>>(groups);

            foreach (var item in groupsViewModel)
            {
                var groupAdminId = _groupUserBusiness.GetAdminId(item.ChurchId);
                var groupUserAdmin = _userBusiness.GetUser(groupAdminId);
                item.UserId = groupUserAdmin.Id;
                item.UserName = groupUserAdmin.UserName;
            }
            var groupsList = new ChurchsPageViewModel { ChurchList = groupsViewModel, Filter = filter };

            // If its an ajax request, just return the table
            if (Request.IsAjaxRequest())
            {
                return PartialView("_GroupsTable", groupsList);
            }
            return View("ListOfGroups", groupsList);
        }

        public ActionResult SearchMemberForGoalAssigning(int id)
        {
            var currentUserId = User.Identity.GetUserId();
            var users = _groupUserBusiness.GetMembersOfGroup(id);
            var groupusers = _groupUserBusiness.GetGroupUsersListToAssign(id, currentUserId);
            var result = from u in users
                         join gu in groupusers on u.Id equals gu.UserId
                         where gu.ChurchId == id
                         select new MemberViewModel
                         {
                             ChurchId = id,
                             UserId = u.Id,
                             ChurchUserId = gu.ChurchUserId,
                             UserName = u.UserName,
                             User = u
                         };
            //MemberViewModel gmvm = new MemberViewModel() { GroupId = id, Group = _groupBusiness.GetGroup(id), Users = members , GroupUser = users};
            return PartialView("_MembersToSearch", result);
        }

        public ActionResult EditUpdate(int id)
        {
            var update = _groupUpdateBusiness.GetUpdate(id);
            ChurchUpdateFormModel editUpdate = Mapper.Map<ChurchUpdate, ChurchUpdateFormModel>(update);
            if (update == null)
            {
                return HttpNotFound();
            }
            return PartialView("_EditUpdate", editUpdate);
        }

        [HttpPost]
        public ActionResult EditUpdate(ChurchUpdateFormModel newupdate)
        {
            ChurchUpdate update = Mapper.Map<ChurchUpdateFormModel, ChurchUpdate>(newupdate);
            if (ModelState.IsValid)
            {

                _groupUpdateBusiness.EditUpdate(update);
                var Updates = Mapper.Map<IEnumerable<ChurchUpdate>, IEnumerable<ChurchUpdateViewModel>>(_groupUpdateBusiness.GetUpdatesByGoal(newupdate.ChurchGoalId));
                foreach (var item in Updates)
                {
                    item.IsSupported = _groupUpdateSupportBusiness.IsUpdateSupported(item.ChurchUpdateId, User.Identity.GetUserId(), _groupUserBusiness);
                    item.UserId = _groupUpdateUserBusiness.GetGroupUpdateUser(item.ChurchUpdateId).Id;
                }
                ChurchUpdateListViewModel updates = new ChurchUpdateListViewModel()
                {
                    ChurchUpdates = Updates,
                    Metric = _groupGoalBusiness.GetGroupGoal(newupdate.ChurchGoalId).Metric,
                    Target = _groupGoalBusiness.GetGroupGoal(newupdate.ChurchGoalId).Target
                };
                return PartialView("_UpdateView", updates);
            }
            return null;
        }

        public ActionResult DeleteUpdate(int id)
        {
            var update = _groupUpdateBusiness.GetUpdate(id);

            if (update == null)
            {
                return HttpNotFound();
            }
            return PartialView("_DeleteUpdate", update);
        }

        [HttpPost, ActionName("DeleteUpdate")]
        public ActionResult DeleteConfirmedUpdate(int id)
        {
            var update = _groupUpdateBusiness.GetUpdate(id);
            if (update == null)
            {
                return HttpNotFound();
            }

            _groupUpdateBusiness.DeleteUpdate(id);
            return RedirectToAction("GroupGoal", new { id = update.ChurchGoalId });
        }

        public void SupportUpdate(int id)
        {
            var groupuser = _groupUserBusiness.GetGroupUserByuserId(User.Identity.GetUserId());
            _groupUpdateSupportBusiness.CreateSupport(new ChurchUpdateSupport() { ChurchUserId = groupuser.ChurchUserId, ChurchUpdateId = id, UpdateSupportedDate = DateTime.Now });
        }


        public int NoOfSupports(int id)
        {
            return _groupUpdateSupportBusiness.GetSupportcount(id);
        }


        public void UnSupportUpdate(int id)
        {
            var groupuser = _groupUserBusiness.GetGroupUserByuserId(User.Identity.GetUserId());
            _groupUpdateSupportBusiness.DeleteSupport(id, groupuser.ChurchUserId);
        }

        [HttpGet]
        public JsonResult DisplayUpdateSupportCount(int id)
        {
            int supportcount = _groupUpdateSupportBusiness.GetSupportcount(id);
            return Json(supportcount, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult SupportersOfUpdate(int id)
        {
            var supporters = _groupUpdateSupportBusiness.GetSupportersOfUpdate(id, _userBusiness, _groupUserBusiness);
            ChurchUpdateSupportersViewModel usvm = new ChurchUpdateSupportersViewModel() { ChurchUpdateId = id, ChurchUpdate = _groupUpdateBusiness.GetUpdate(id), Users = supporters };
            return PartialView(usvm);
        }
    }
}
