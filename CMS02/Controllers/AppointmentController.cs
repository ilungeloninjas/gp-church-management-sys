﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS02_Data;
using CMS02_Models.Models;
using DHTMLX.Common;
using DHTMLX.Scheduler;
using DHTMLX.Scheduler.Controls;
using DHTMLX.Scheduler.Data;
using CMS02_Service.Repository;

namespace CMS02.Controllers
{
    public class AppointmentController : Controller
    {
        // GET: Appointment
        public virtual ActionResult Index()
        {

            var sched = new DHXScheduler(this) { InitialDate = new DateTime(2015, 7, 20) };
            sched.Extensions.Add(SchedulerExtensions.Extension.PDF);

            //load data initially
            sched.LoadData = true;

            //save client-side changes
            sched.EnableDataprocessor = true;


            return View(sched);
        }

        /// <summary>
        /// Block time areas
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// Colors for time areas
        /// </summary>
        /// <returns></returns>
        public ActionResult MarkedTimeSpans()
        {
            var sched = new DHXScheduler(this) { InitialDate = new DateTime(2015, 7, 19) };


            sched.TimeSpans.Add(new DHXMarkTime()
            {
                Day = DayOfWeek.Thursday,
                CssClass = "red_section",// apply this css class to the section
                HTML = "Forbidden", // inner html of the section
                Zones = new List<Zone>() { new Zone { Start = 2 * 60, End = 12 * 60 } },
                SpanType = DHXTimeSpan.Type.BlockEvents//if specified - user can't create event in this area
            });
            sched.TimeSpans.Add(new DHXMarkTime()
            {
                Day = DayOfWeek.Saturday,
                CssClass = "green_section"
            });
            sched.TimeSpans.Add(new DHXMarkTime()
            {
                StartDate = new DateTime(2015, 7, 25),
                EndDate = new DateTime(2015, 7, 29),
                CssClass = "green_section"
            });

            sched.LoadData = true;
            sched.EnableDataprocessor = true;
            return View(sched);
        }



        /// <summary>
        /// Highligting pointed area
        /// </summary>
        /// <returns></returns>
        public ActionResult Highlight()
        {
            var sched = new DHXScheduler(this) { InitialDate = new DateTime(2015, 7, 19) };

            sched.Highlighter.Enable("highlight_section");//use 'highlight_section' class for highlighted area

            sched.LoadData = true;
            sched.EnableDataprocessor = true;
            return View(sched);
        }

        /// <summary>
        /// Single click event creation
        /// </summary>
        /// <returns></returns>
        public ActionResult HighlightClickCreate()
        {
            var sched = new DHXScheduler(this) { InitialDate = new DateTime(2015, 7, 19), Config = { drag_create = false } };

            //use 'highlight_section' class for highlighted area   
            //size of the area = 120 minutes
            sched.Highlighter.Enable("highlight_section", 120);
            sched.Highlighter.FixedStep = false;
            //create event on single click on highlighted area
            sched.Highlighter.CreateOnClick = true;
            sched.LoadData = true;
            sched.EnableDataprocessor = true;
            return View(sched);
        }


        /// <summary>
        /// Custom event containers
        /// </summary>
        /// <returns></returns>
        public ActionResult CustomEventBox()
        {
            var sched = new DHXScheduler(this) { Skin = DHXScheduler.Skins.Terrace };

            //helper for event templates,
            //it also can be defined on the client side
            var evBox = new DHXEventTemplate
            {
                CssClass = sched.Templates.event_class = "my_event",
                Template = @"<div class='my_event_body'>
                    <% if((ev.end_date - ev.start_date) / 60000 > 60) { %>
                        <span class='event_date'>{start_date:date(%H:%i)} - {end_date:date(%H:%i)}</span><br/>
                    <% } else { %>
                        <span class='event_date'>{start_date:date(%H:%i)}</span>
                    <% } %>                  
                    <span>{text}</span>
                    <br>
                    <div style=""padding-top:5px;"">
                        Duration: <b><%= Math.ceil((ev.end_date - ev.start_date) / (60 * 60 * 1000)) %></b> hours
                    </div>
                  </div>"
            };

            // template will be rendered to the js function - function(ev, start, end){....}
            // where ev - is event object itself
            // template allows to inject js code within asp.net-like tags, and output properties of event with simplified sintax
            // {text} is equivalent to ' + ev.text + '
            sched.Templates.EventBox = evBox;
            sched.LoadData = true;
            sched.EnableDataprocessor = true;

            return View(sched);
        }

        public ActionResult IndexRazor()
        {
            return Index();
        }

        /// <summary>
        /// custom DataObject-to-JSON function
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="ev"></param>
        public void eventRenderer(System.Text.StringBuilder builder, object ev)
        {
            var item = ev as Appointment;
            builder.Append(
                string.Format("{{id:{0}, text:\"{1}\", start_date:\"{2:MM/dd/yyyy HH:mm}\", end_date:\"{3:MM/dd/yyyy HH:mm}\"}}",
                item.id,
                item.text.Replace("\"", "\\\""),//need to escape quotes and other characters, that may break json
                item.start_date,
                item.end_date));
        }
        /// <summary>
        /// Rendering data with custom function
        /// </summary>
        /// <returns></returns>
        public ContentResult CustomData()
        {
            var data = new SchedulerAjaxData();
            /*
             *  return data;
             *  is equal to 
             *  return Content(data.Render());
             *  SchedulerAjaxData.Render can also take Action<StringBuilder, object> as a parameter
             */
            return Content(data.Render(eventRenderer));
        }

        public ContentResult Data()
        {
            var context = new GodProsperityContext();
            var data = new SchedulerAjaxData(context.Appointments);
            return data;
        }

        public ContentResult Save(int? id, FormCollection actionValues)
        {

            var context = new GodProsperityContext();
            var app = new AppointmenRepository();
            var action = new DataAction(actionValues);
            try
            {
                var changedEvent = (Appointment)DHXEventsHelper.Bind(typeof(Appointment), actionValues);
                switch (action.Type)
                {
                    case DataActionTypes.Insert:
                        if (!app.CreateAppointment(changedEvent))
                        {
                            action.Type = DataActionTypes.Error;
                        }
                        break;
                    case DataActionTypes.Delete:
                        changedEvent =context.Appointments.SingleOrDefault(ev => ev.id == action.SourceId);
                        if (!app.RemoveAppointment((int)action.SourceId))
                        {
                            action.Type = DataActionTypes.Error;
                        }
                        break;

                    default:// "update"                          
                        var eventToUpdate = context.Appointments.SingleOrDefault(ev => ev.id == action.SourceId);
                        if (!app.UpdateAppointment(changedEvent))
                        {
                            action.Type = DataActionTypes.Error;
                        }
                        //update all properties, except for id
                        DHXEventsHelper.Update(eventToUpdate, changedEvent, new List<string>() { "id" });
                        break;
                }
                //data.SubmitChanges();
                action.TargetId = changedEvent.id;
            }
            catch
            {
                action.Type = DataActionTypes.Error;
            }
            return (new AjaxSaveResponse(action));
        }


    }
}