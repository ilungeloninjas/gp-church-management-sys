﻿using System;
using System.Web.Mvc;
using CMS02.Helpers;
using CMS02_BusinessLogic;
using CMS02_Models.Models;
using Microsoft.AspNet.Identity;

namespace CMS02.Controllers
{
    [Authorize]
    public class EmailRequestController : Controller
    {
        //
        // GET: /EmailRequest/
        private readonly ISecurityTokenBusiness _securityTokenBusiness;
        public readonly IChurchUserBusiness ChurchUserBusiness;
        public readonly ISupportBusiness SupportBusiness;
        public readonly IChurchInvitationBusiness ChurchInvitationBusiness;

        public EmailRequestController(ISecurityTokenBusiness securityTokenBusiness, IChurchUserBusiness groupUserBusiness, ISupportBusiness supportBusiness, IChurchInvitationBusiness groupInvitationBusiness)
        {
            _securityTokenBusiness = securityTokenBusiness;
            ChurchUserBusiness = groupUserBusiness;
            SupportBusiness = supportBusiness;
            ChurchInvitationBusiness = groupInvitationBusiness;
        }

        public ActionResult AddGroupUser()
        {
            Guid groupIdToken = (Guid)TempData["grToken"];
            var groupId = _securityTokenBusiness.GetActualId(groupIdToken);
            ChurchUser newGroupUser = new ChurchUser()
            {
                UserId = User.Identity.GetUserId(),
                ChurchId = groupId,
                Admin = false
            };
            ChurchUserBusiness.CreateGroupUser(newGroupUser, ChurchInvitationBusiness);
            _securityTokenBusiness.DeleteSecurityToken(groupIdToken);
            GpSessionFacade.Remove(GpSessionFacade.JoinGroupOrGoal);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult AddSupportToGoal()
        {
            Guid goalIdToken = (Guid)TempData["goToken"];
            var goalId = _securityTokenBusiness.GetActualId(goalIdToken);
            SupportBusiness.CreateSupport(new Support() { UserId = User.Identity.GetUserId(), GoalId = goalId, SupportedDate = DateTime.Now });
            _securityTokenBusiness.DeleteSecurityToken(goalIdToken);
            GpSessionFacade.Remove(GpSessionFacade.JoinGroupOrGoal);
            return RedirectToAction("Index", "Home");
        }
    }
}