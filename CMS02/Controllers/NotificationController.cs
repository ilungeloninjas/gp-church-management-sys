﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using CMS02.ViewModels;
using CMS02_BusinessLogic;
using CMS02_Models.Models;
using Microsoft.AspNet.Identity;

namespace CMS02.Controllers
{
    [Authorize]
    public class NotificationController : Controller
    {
        IGoalBusiness _goalBusiness;
        IUpdateBusiness _updateBusiness;
        ICommentBusiness _commentBusiness;
        IChurchInvitationBusiness _groupInvitationBusiness;
        ISupportInvitationBusiness _supportInvitationBusiness;
        IFollowRequestBusiness _followRequestBusiness;
        IUserBusiness _userBusiness;
        public NotificationController(IGoalBusiness goalBusiness, IUpdateBusiness updateBusiness, ICommentBusiness commentBusiness, IChurchInvitationBusiness groupInvitationBusiness, ISupportInvitationBusiness supportInvitationBusiness, IFollowRequestBusiness followRequestBusiness, IUserBusiness userBusiness)
        {
            _goalBusiness = goalBusiness;
            _supportInvitationBusiness = supportInvitationBusiness;
            _updateBusiness = updateBusiness;
            _groupInvitationBusiness = groupInvitationBusiness;
            _commentBusiness = commentBusiness;
            _followRequestBusiness = followRequestBusiness;
            _userBusiness = userBusiness;
        }


        /// <summary>
        /// Action that returns invitations
        /// </summary>
        /// <param name="page">current page no will be bere</param>
        /// <returns></returns>
        public ActionResult Index(int page = 0)
        {
            int noOfrecords = 10;
            var notifications = GetNotifications(page, noOfrecords);
            if (Request.IsAjaxRequest())
            {
                return PartialView("_NotificationList", notifications);
            }
            return View("Index", notifications);
        }



        public int GetNumberOfInvitations()
        {
            return _groupInvitationBusiness.GetGroupInvitationsForUser(User.Identity.GetUserId()).Count() + _supportInvitationBusiness.GetSupportInvitationsForUser(User.Identity.GetUserId()).Count() + _followRequestBusiness.GetFollowRequestsForUser(User.Identity.GetUserId()).Count();
        }

        /// <summary>
        /// Method returns paged notifications
        /// </summary>
        /// <param name="page"></param>
        /// <param name="noOfRecords"></param>
        /// <returns></returns>
        public IEnumerable<NotificationViewModel> GetNotifications(int page, int noOfRecords)
        {
            var groupInv = _groupInvitationBusiness.GetGroupInvitationsForUser(User.Identity.GetUserId());
            var supportInv = _supportInvitationBusiness.GetSupportInvitationsForUser(User.Identity.GetUserId());
            var followRequests = _followRequestBusiness.GetFollowRequests(User.Identity.GetUserId());
            IEnumerable<NotificationViewModel> invitations = Mapper.Map<IEnumerable<ChurchInvitation>, IEnumerable<NotificationViewModel>>(groupInv);
            invitations = invitations.Concat(Mapper.Map<IEnumerable<SupportInvitation>, IEnumerable<NotificationViewModel>>(supportInv));
            invitations = invitations.Concat(Mapper.Map<IEnumerable<FollowRequest>, IEnumerable<NotificationViewModel>>(followRequests));
            foreach (var item in invitations)
            {
                var fromUser = _userBusiness.GetUser(item.FromUserId);
                var toUser = _userBusiness.GetUser(item.ToUserId);
                item.FromUser = fromUser;
                item.ToUser = toUser;
            }

            //for paging

            var skipNotifications = noOfRecords * page;
            invitations = invitations.Skip(skipNotifications).Take(noOfRecords);
            return invitations;
        }
    }
}
