﻿using System;
using System.ComponentModel.DataAnnotations;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class FocusFormModel
    {
        public int FocusId { get; set; }
        [Required(ErrorMessage = "*")]
        [StringLength(50)]
        public string FocusName { get; set; }
        [Required(ErrorMessage = "*")]
        [StringLength(100)]
        public string Description { get; set; }

        public int ChurchId { get; set; }

        public virtual Church Church { get; set; }

        public DateTime CreatedDate { get; set; }

        public FocusFormModel()
        {
            CreatedDate = DateTime.Now;
        }
    }
}