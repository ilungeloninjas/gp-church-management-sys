﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS02_Models.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMS02.ViewModels
{
    public class AttendEventViewModel
    {
        public int AttendEventId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public int? ChurchGoalId { get; set; }
        public virtual ChurchGoal ChurchGoals { get; set; }
    }
}