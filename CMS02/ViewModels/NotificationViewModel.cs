﻿using System;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class NotificationViewModel
    {
        public int ChurchInvitationId { get; set; }

        public int SupportInvitationId { get; set; }

        public string FromUserId { get; set; }

        public int ChurchId { get; set; }

        public string ToUserId { get; set; }

        public DateTime SentDate { get; set; }

        public ApplicationUser FromUser { get; set; }

        public ApplicationUser ToUser { get; set; }

        public virtual Church Church { get; set; }

        public bool Accepted { get; set; }

        public int GoalId { get; set; }

        public virtual Goal Goal { get; set; }
    }
}