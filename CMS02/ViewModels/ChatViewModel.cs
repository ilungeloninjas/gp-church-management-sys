﻿using System;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChatViewModel
    {
        public int ChatId { get; set; }

        public string Message { get; set; }

        public DateTime DateSent { get; set; }

        public string FromUserId { get; set; }

        public string ToUserId { get; set; }

        public ApplicationUser User { get; set; }
    }
}