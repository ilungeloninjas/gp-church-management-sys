﻿namespace CMS02.ViewModels
{
    public class ChurchsItemViewModel
    {
        public int ChurchId { get; set; }

        public string ChurchName { get; set; }

        public string Description { get; set; }

        public string CreatedDate { get; set; }

        public string UserId { get; set; }
        public string UserName { get; set; }
    }
}