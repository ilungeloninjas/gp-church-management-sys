﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CMS02.ViewModels
{
    public class ChurchFormModel
    {
        public int ChurchId { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(50)]
        public string ChurchName { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(100)]
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UserId { get; set; }

        public ChurchFormModel()
        {
            CreatedDate = DateTime.Now;
        }
    }
}