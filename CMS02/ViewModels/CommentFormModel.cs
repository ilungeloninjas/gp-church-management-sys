﻿using System.ComponentModel.DataAnnotations;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class CommentFormModel
    {
        [Required(ErrorMessage = "Required")]
        public string CommentText { get; set; }

        public int UpdateId { get; set; }

        public int UserId { get; set; }

        public ApplicationUser User { get; set; }
    }
}