﻿using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class MemberViewModel
    {
        public int ChurchId { get; set; }

        public Church Church { get; set; }

        public string UserId { get; set; }

        public int ChurchUserId { get; set; }

        public string UserName { get; set; }

        public virtual ApplicationUser User { get; set; }

        //public IEnumerable<GroupUser> GroupUser { get; set; }

        //public IEnumerable<User> Users { get; set; }
    }
}