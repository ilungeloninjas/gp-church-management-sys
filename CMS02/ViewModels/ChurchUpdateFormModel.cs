﻿using System.ComponentModel.DataAnnotations;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChurchUpdateFormModel
    {
        public int ChurchUpdateId { get; set; }
        [Required(ErrorMessage = "*")]
        public string Updatemsg { get; set; }

        public double? Status { get; set; }

        public int ChurchGoalId { get; set; }

        public int ChurchId { get; set; }

        public string UserId { get; set; }

        public ApplicationUser User { get; set; }

        public ChurchGoal ChurchGoal { get; set; }
    }
}