﻿using System;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class NotificationsViewModel
    {
        //public NotificationsViewModel()
        //{
        //    if (CreatedDate == DateTime.Now)
        //    {
        //        NotificationDate = "Today";
        //    }
        //    else if (CreatedDate == System.DateTime.Today.AddDays(-1))
        //    {
        //        NotificationDate = "Yesterday";
        //    }
        //    else{
        //        NotificationDate = CreatedDate.ToShortDateString();
        //    }

       // }
        public int NotificationId { get; set; }
        public int NotificationType { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string ProfilePicUrl { get; set; }

        public int NumberOfComments { get; set; }
        
        public int? UpdateId { get; set; }
        public string Updatemsg { get; set; }
        public float? Status { get; set; }
        public DateTime UpdateDate { get; set; }
        public virtual Goal Goal { get; set; }
        
        public int? CommentId { get; set; }
        public string CommentText { get; set; }
        public DateTime CommentDate { get; set; }
        public virtual Update Update { get; set; }

        public int? GoalId { get; set; }
        public string GoalName { set; get; }
        public string Desc { get; set; }
        public float? Target { get; set; }
        public int MetricId { get; set; }
        public int? FocusId { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual Metric Metric { get; set; }
        public virtual Focus Focus { get; set; }

        public int? ChurchId { get; set; }
        public string ChurchName { get; set; }

        public int? ChurchUserId { get; set; }
        public virtual ChurchUser ChurchUser { get; set; }

        public int? SupportId { get; set; }
        public Support Support { get; set; }

        public int? ChurchGoalId { get; set; }
        public string ChurchGoalName { get; set; }
        public string GoalDesc { get; set; }
        public virtual ChurchGoal ChurchGoal { get; set; }
       
        //public bool SupportLink { get; set; }
        public bool IsAMember { get; set; }

        public int? ChurchUpdateId { get; set; }
        public string ChurchUpdatemsg { get; set; }
        public float? Churchstatus { get; set; }
        public DateTime ChurchUpdateDate { get; set; }


        public int? ChurchCommentId { get; set; }
        public string ChurchCommentText { get; set; }
        public DateTime ChurchCommentDate { get; set; }
        public virtual ChurchUpdate ChurchUpdate { get; set; }

        public int? FollowUserId { get; set; }
        public string FromUserId { get; set; }
        public string ToUserId { get; set; }
        public virtual ApplicationUser FromUser { get; set; }
        public virtual ApplicationUser ToUser { get; set; }

        public string NotificationDate { get; set; }
   
    }
}