﻿using System.Collections.Generic;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChurchUpdateSupportersViewModel
    {
        public int ChurchUpdateId { get; set; }

        public ChurchUpdate ChurchUpdate { get; set; }

        public IEnumerable<ApplicationUser> Users { get; set; }
    }
}