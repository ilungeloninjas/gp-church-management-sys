﻿using CMS02_Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS02.ViewModels
{
    public class User_ActivityFormModel
    {
        public int Id { get; set; }

        public static DateTime ActiveThreshold
        {
            get { return DateTime.Now.AddMinutes(-15); }
        }

        public DateTime LastActivity { get; set; }

        public bool IsOnline
        {
            get { return LastActivity > ActiveThreshold; }
        }

        public int UserId { get; set; }

        public virtual ICollection<ApplicationUser> AppUser { get; set; }
    }
}