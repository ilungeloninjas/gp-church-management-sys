﻿using System.ComponentModel.DataAnnotations;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChatFormModel
    {
        [Required(ErrorMessage = "Required")]
        public string Message { get; set; }

        public string FromUserId { get; set; }

        public string ToUserId { get; set; }

        public ApplicationUser User { get; set; }
    }
}