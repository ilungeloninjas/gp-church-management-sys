﻿using System.ComponentModel.DataAnnotations;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class UpdateFormModel
    {
        public int UpdateId { get; set; }
        [Required(ErrorMessage = "*")]
        public string Updatemsg { get; set; }
        
        
        public double? Status { get; set; }

        public int GoalId { get; set; }

        public virtual Goal Goal { get; set; }
    }
}