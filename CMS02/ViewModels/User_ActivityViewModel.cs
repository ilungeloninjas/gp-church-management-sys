﻿using CMS02_Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS02.Models
{
    public partial class User_ActivityViewModel
    {

        public static DateTime ActiveThreshold
        {
            get
            {
                return DateTime.Now.AddMinutes(-15);
            }
        }
        public DateTime Last_Activity { get; set; }
        public bool IsOnline
        {
            get
            {
                return this.Last_Activity > ActiveThreshold;
            }
        }

        public int UserId { get; set; }

        public virtual ICollection<ApplicationUser> AppUser { get; set; }
    }
}