﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChurchViewModel
    {

        public int ChurchId { get; set; }

        [Required]
        public string ChurchName { get; set; }

        [Required]
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        //public int UserId { get; set; }

        //public int GroupUserId { get; set; }

        public virtual IEnumerable<ChurchGoalViewModel> Goals { get; set; }

        public bool IsAMember { get; set; }

        public bool RequestSent { get; set; }

        public bool InvitationSent { get; set; }

        public int NoOfMembers { get; set; }

        public bool Admin { get; set; }

        public virtual IEnumerable<Focus> Focus { get; set; }

        public virtual IEnumerable<ChurchGoalViewModel> GoalsAssignedToOthers { get; set; }

        public virtual IEnumerable<ChurchGoalViewModel> GoalsAssignedToMe { get; set; }

        //public virtual GroupUser GroupUser { get; set; }

        public IEnumerable<ApplicationUser> Users { get; set; }

    }
}