﻿using System.ComponentModel.DataAnnotations;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChurchCommentFormModel
    {
        [Required(ErrorMessage = "*")]
        public string CommentText { get; set; }

        public int ChurchUpdateId { get; set; }

        public string UserId { get; set; }

        public ApplicationUser User { get; set; }

    }
}