﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChurchGoalViewModel
    {
        public int ChurchGoalId { get; set; }
       [Required(ErrorMessage = "*")]
        public string GoalName { set; get; }
        [Required(ErrorMessage = "*")]
        public string Description { get; set; }

        [Required(ErrorMessage = "*")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "*")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime EndDate { get; set; }

        public double? Target { get; set; }
        
        public int? MetricId { get; set; }

        public int? FocusId { get; set; }
        public int NumberofAttendance { get; set; }

        public string UserId { get; set; }

        public int GoalStatusId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int ChurchUserId { get; set; }

        public int AssignedChurchUserId { get; set; }

        public string AssignedUserId { get; set; }

        public string AssignedTo { get; set; }

        public TimeSpan? Duration { get; set; }

        public string Location { get; set; }

        public int ChurchId { get; set; }

        public virtual GoalStatus GoalStatus { get; set; }

        public IEnumerable<SelectListItem> GoalStatuses { get; set; }

        public ApplicationUser User { get; set; }

        public virtual ChurchUser ChurchUser { get; set; }

        public virtual Metric Metric { get; set; }

        public virtual Focus Focus { get; set; }

        public virtual Church Church { get; set; }

        public virtual IEnumerable<ChurchUpdate> Updates { get; set; }

        public IEnumerable<ApplicationUser> Users { get; set; }

        public virtual IEnumerable<AttendEventViewModel> AttendEvents { get; set; }

        public bool IsAMember { get; set; }

        public bool Admin { get; set; }

        //public GroupGoalViewModel()
        //{
        //    CreatedDate = DateTime.Now;
        //    StartDate = DateTime.Now;
        //    EndDate = DateTime.Now;
        //}
    }
}