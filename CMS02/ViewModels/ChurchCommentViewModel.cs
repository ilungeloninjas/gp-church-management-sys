﻿using System;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChurchCommentsViewModel
    {
        public int ChurchCommentId { get; set; }

        public string CommentText { get; set; }

        public int ChurchUpdateId { get; set; }

        public string UserId { get; set; }

        public DateTime CommentDate { get; set; }

        public ApplicationUser User { get; set; }
    }
}