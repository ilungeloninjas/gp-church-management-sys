﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace CMS02.ViewModels
{
    public class GoalsPageViewModel
    {
        public IEnumerable<GoalListViewModel> GoalList { get; set; }

        public IEnumerable<SelectListItem> FilterBy { get; set; }

        public IEnumerable<SelectListItem> SortBy { get; set; }

        public GoalsPageViewModel(string selectedFilter,string selectedSort)
        {
            FilterBy = new SelectList(new[]{
                       new SelectListItem{ Text="All", Value="All"},
                       new SelectListItem{ Text="My Events", Value="My Events"},
                       new SelectListItem{ Text="My Followed Events", Value="My Followed Events"},
                       new SelectListItem{ Text="My Followings Events", Value="My Followings Events"}
                       }, "Text", "Value", selectedFilter);
            SortBy = new SelectList(new[]{
                       new SelectListItem{ Text="Date", Value="Date"},
                       new SelectListItem{ Text="Popularity", Value="Popularity"}}, "Text", "Value", selectedSort);

        }
    }
    
}