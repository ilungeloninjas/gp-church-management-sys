﻿using System;
using System.Collections.Generic;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class UpdateViewModel
    {
        public int UpdateId { get; set; }

        public string Updatemsg { get; set; }

        public double? Status { get; set; }

        public int GoalId { get; set; }

        public DateTime UpdateDate { get; set; }

        public virtual Goal Goal { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public bool? IsSupported { get; set; }

        public virtual ICollection<UpdateSupport> UpdateSupports { get; set; }
    }
}