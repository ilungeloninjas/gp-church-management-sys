﻿using System.Collections.Generic;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class FocusViewModel
    {
        public int FocusId { get; set; }

        public string FocusName { get; set; }

        public string Description { get; set; }

        public int ChurchId { get; set; }

        public virtual Church Church { get; set; }

        public bool IsAMember { get; set; }

        public virtual IEnumerable<ChurchGoalViewModel> ChurchGoal { get; set; }

        public virtual IEnumerable<ApplicationUser> Users { get; set; }

    }
}