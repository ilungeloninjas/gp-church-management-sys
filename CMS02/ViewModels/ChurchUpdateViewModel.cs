﻿using System;
using System.Collections.Generic;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChurchUpdateViewModel
    {
        public int ChurchUpdateId { get; set; }

        public string Updatemsg { get; set; }

        public double? Status { get; set; }

        public int ChurchGoalId { get; set; }

        public DateTime UpdateDate { get; set; }

        public virtual ChurchGoal ChurchGoal { get; set; }

        public virtual ICollection<ChurchComment> ChurchComments { get; set; }

        public bool? IsSupported { get; set; }

        public int ChurchUserId { get; set; }

        public string UserId { get; set; }

        public virtual ICollection<ChurchUpdateSupport> ChurchUpdateSupports { get; set; }
    }
}