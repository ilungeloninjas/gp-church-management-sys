﻿using CMS02_BusinessLogic;
using PagedList;

namespace CMS02.ViewModels
{
    public class ChurchsPageViewModel
    {
        public IPagedList<ChurchsItemViewModel> ChurchList { get; set; }

        public ChurchFilter Filter { get; set; }
    }
}