﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChurchGoalFormModel
    {
        public int ChurchGoalId { get; set; }

       [Required(ErrorMessage = "*")]
       [StringLength(50)]
        public string GoalName { set; get; }

        [Required(ErrorMessage = "*")]
        [StringLength(100)]
        public string Description { get; set; }

        [Required(ErrorMessage = "*")]
        public DateTime? StartDate { get; set; }

        [Required(ErrorMessage = "*")]
        public DateTime? EndDate { get; set; }

        public TimeSpan? Duration { get; set; }

        public int NumberofAttendance { get; set; }
        public string Location { get; set; }

        public double? Target { get; set; }
        
        public int? MetricId { get; set; }

        public int? FocusId { get; set; }

        public int ChurchUserId { get; set; }

        public int? AssignedGroupUserId { get; set; }

        public string AssignedTo { get; set; }

        public DateTime CreatedDate { get; set; }

        public int ChurchId { get; set; }

        public IEnumerable<SelectListItem> Metrics { get; set; }

        public IEnumerable<SelectListItem> Foci { get; set; }

        //public virtual GroupUser GroupUser { get; set; }

        //public virtual User User { get; set; }

        public virtual Metric Metric { get; set; }

        public virtual Focus Focus { get; set; }

        public virtual Church Church { get; set; }

        public virtual IEnumerable<ChurchUpdate> Updates { get; set; }

        public ChurchGoalFormModel()
        {
            CreatedDate = DateTime.Now;
           // StartDate = DateTime.Now;
           // EndDate = DateTime.Now;
        }
    }
}