﻿using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class FollowRequestFormModel
    {
        public string FromUserId { get; set; }

        public string ToUserId { get; set; }

        public virtual ApplicationUser FromUser { get; set; }

        public virtual ApplicationUser ToUser { get; set; }
    }
}