﻿using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChurchRequestViewModel
    {
        public int ChurchId { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual Church Church { get; set; }
    }
}