﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class PostUpdateViewModel
    {
        public int PostUpdateId { get; set; }
        public string UpdateMessage { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        //public DateTime PostUpdateDate { get; set; }
    }
}