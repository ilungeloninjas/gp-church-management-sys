﻿using System.Collections.Generic;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChurchUpdateListViewModel
    {
        public IEnumerable<ChurchUpdateViewModel> ChurchUpdates { get; set; }

        public double? Target { get; set; }

        public Metric Metric { get; set; }
    }
}