﻿using System.Collections.Generic;
using CMS02_Models.Models;

namespace CMS02.ViewModels
{
    public class ChurchMemberViewModel
    {
        public int ChurchId { get; set; }

        public Church Church { get; set; }

        public int? ChurchUserId { get; set; }

        public IEnumerable<ApplicationUser> Users { get; set; }
    }
}