﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS02.ViewModels
{
    public class UpcomingPassedEventsViewModel
    {
        public IEnumerable<ChurchGoalViewModel> UpcomingEvents { get; set; }

        public IEnumerable<ChurchGoalViewModel> PassedEvents { get; set; }
    }
}