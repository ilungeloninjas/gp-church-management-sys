﻿using System;

namespace CMS02_Models.Models
{
    public class SecurityToken
    {
        public int SecurityTokenId { get; set; }

        public Guid Token { get; set; }

        public int ActualId { get; set; }
    }
}
