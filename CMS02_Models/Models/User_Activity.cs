﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS02_Models.Models
{
  public  class User_Activity
    {
        [Key]
        public int Id { get; set; }

        public static DateTime ActiveThreshold
        {
            get { return DateTime.Now.AddMinutes(-15); }
        }

        public DateTime LastActivity { get; set; }

        public bool IsOnline
        {
            get { return LastActivity > ActiveThreshold; }
        }

        public int UserId { get; set; }

        public virtual ICollection<ApplicationUser> AppUser{ get; set; }
    }
}
