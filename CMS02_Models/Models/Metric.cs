﻿using System.Collections.Generic;

namespace CMS02_Models.Models
{
    public class Metric
    {
        public int MetricId { get; set; }

        public string Type { get; set; }

        public virtual ICollection<Goal> Goals { get; set; }

        public virtual ICollection<ChurchGoal> ChurchGoals { get; set; }
    }
}
