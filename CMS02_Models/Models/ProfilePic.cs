﻿using System.Web;

namespace CMS02_Models.Models
{
    public class ProfilePic
    {
        public HttpPostedFileBase FileData { get; set; }

        public string SecurityToken { get; set; }

        public string Filename { get; set; }
    }
}
