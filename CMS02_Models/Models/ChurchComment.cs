﻿using System;

namespace CMS02_Models.Models
{
    public class ChurchComment
    {
        public int ChurchCommentId { get; set; }

        public string CommentText { get; set; }

        public int ChurchUpdateId { get; set; }

        public DateTime CommentDate { get; set; }

        public virtual ChurchUpdate ChurchUpdate { get; set; }

        public ChurchComment()
        {
            CommentDate = DateTime.Now;
        }
    }
}
