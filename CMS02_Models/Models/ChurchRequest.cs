﻿
namespace CMS02_Models.Models
{
    public class ChurchRequest
    {
        public int ChurchRequestId { get; set; }

        public int ChurchId { get; set; }

        public string UserId { get; set; }

        public virtual Church Church { get; set; }

        public virtual ApplicationUser User { get; set; }

        public bool Accepted { get; set; }
    }
}
