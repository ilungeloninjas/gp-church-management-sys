﻿using System;

namespace CMS02_Models.Models
{
    public class Chat
    {
        public int ChatId { get; set; }

        public string Message { get; set; }

        public DateTime DateSent { get; set; }

        public string FromUserId { get; set; }

        public string ToUserId { get; set; }

        public Chat()
        {
            DateSent = DateTime.Now;
        }
    }
}
