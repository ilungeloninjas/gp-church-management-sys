﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS02_Models.Models
{
    public class ChurchUpdate
    {
        [ScaffoldColumn(false)]
        public int ChurchUpdateId { get; set; }

        public string Updatemsg { get; set; }

        public double? Status { get; set; }

        public int ChurchGoalId { get; set; }

        public DateTime UpdateDate { get; set; }

        public virtual ChurchGoal ChurchGoal { get; set; }

        public virtual ICollection<ChurchComment> ChurchComments { get; set; }

        public ChurchUpdate()
        {
            UpdateDate = DateTime.Now;

        }

    }
}
