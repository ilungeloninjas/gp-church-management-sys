﻿using System;
using System.Collections.Generic;

namespace CMS02_Models.Models
{
    public class Church
    {
        public int ChurchId { get; set; }

        public string ChurchLocation { get; set; }

        public string email { get; set; }

        public string Address { get; set; }

        public string Telephone { get; set; }

        public string ChurchName { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public virtual ICollection<Focus> Foci { get; set; }

        public virtual ICollection<ChurchInvitation> ChurchInvitations { get; set; }

        public virtual ICollection<ChurchRequest> ChurchRequests { get; set; }

        public Church()
        {
            CreatedDate = DateTime.Now;
        }
    }
}
