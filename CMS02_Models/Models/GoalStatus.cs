﻿using System.Collections.Generic;

namespace CMS02_Models.Models
{
    public class GoalStatus
    {
        public GoalStatus()
        {
            Goals = new HashSet<Goal>();
            ChurchGoals = new HashSet<ChurchGoal>();
        }
        public int GoalStatusId { get; set; }

        public string GoalStatusType { get; set; }

        public virtual ICollection<Goal> Goals { get; set; }

        public virtual ICollection<ChurchGoal> ChurchGoals { get; set; }

    }
}
