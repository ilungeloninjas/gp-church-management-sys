﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace CMS02_Models.Models
{
    public class ApplicationUserRole : IdentityUserRole
    {
        public ApplicationUserRole()
            : base()
        { }

        public ApplicationRole Role { get; set; }
    }
}