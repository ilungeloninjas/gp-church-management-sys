﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CMS02_Models.Models
{
    public class ApplicationUser: IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public ICollection<ApplicationUserRole> UserRoles { get; set; }
        public ApplicationUser()
        {
            DateCreated = DateTime.Now;
        }
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public virtual string SecurityQuestion { get; set; }

        public virtual string SecurityAnswer { get; set; }

        public string ProfilePicUrl { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? LastLoginTime { get; set; }

        public bool Activated { get; set; }

        public int RoleId { get; set; }

        public virtual ICollection<Goal> Goals { get; set; }

        public virtual ICollection<FollowUser> FollowFromUser { get; set; }

        public virtual ICollection<FollowUser> FollowToUser { get; set; }

        public virtual ICollection<ChurchRequest> ChurchRequests { get; set; }        

        public string DisplayName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}
