﻿
namespace CMS02_Models.Models
{
    public class ChurchCommentUser
    {
        public int ChurchCommentUserId { get; set; }

        public int ChurchCommentId { get; set; }

        public string UserId { get; set; }

    }
}
