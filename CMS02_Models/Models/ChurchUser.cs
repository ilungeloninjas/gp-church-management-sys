﻿using System;
using System.Collections.Generic;

namespace CMS02_Models.Models
{
    public class ChurchUser
    {
        public ChurchUser()
        {
            AddedDate = DateTime.Now;
        }
        public int ChurchUserId { get; set; }

        public int ChurchId { get; set; }

        public string UserId { get; set; }

        public bool Admin { get; set; }

        public DateTime AddedDate { get; set; }

        public virtual ICollection<ChurchGoal> ChurchGoals { get; set; }
    }
}
