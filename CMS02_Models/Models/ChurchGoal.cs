﻿using System;
using System.Collections.Generic;

namespace CMS02_Models.Models
{
    public class ChurchGoal
    {
        public int ChurchGoalId { get; set; }

        public string GoalName { set; get; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public TimeSpan? Duration { get; set; }

        public string Location { get; set; }

        public double? Target { get; set; }

        public int? MetricId { get; set; }

        public int? FocusId { get; set; }

        public int NumberofAttendance { get; set; }

        public DateTime CreatedDate { get; set; }

        public int GoalStatusId { get; set; }

        public int ChurchUserId { get; set; }

        public int? AssignedChurchUserId { get; set; }

        public string AssignedTo { get; set; }

        public virtual ChurchUser ChurchUser { get; set; }

        public int ChurchId { get; set; }

        public virtual Church Church { get; set; }


        public virtual Metric Metric { get; set; }

        public virtual Focus Focus { get; set; }

        public virtual GoalStatus GoalStatus { get; set; }

        public virtual ICollection<ChurchUpdate> Updates { get; set; }

        public int AttendEventId { get; set; }
        public virtual ICollection<AttendEvent> AttendEvents { get; set; }

        public ChurchGoal()
        {
            CreatedDate = DateTime.Now;
            GoalStatusId = 1;
        }
    }
}
