﻿using System.Collections.Generic;

namespace CMS02_Models.Models
{
    public class AppointmentStatus
    {
        public AppointmentStatus() 
        {
            Appointments = new HashSet<Appointment>();
        }
        public int ApointmentId { get; set; }
        public string StatusType { get; set; }

        public virtual ICollection<Appointment> Appointments { get; set; } 
    }
}
