﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS02_Models.Models
{
    public class PostUpdate
    {
        [Key]
        public int PostUpdateId { get; set; }
        public string UpdateMessage { get; set; }

        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public bool Admin { get; set; }
        //public DateTime PostUpdateDate { get; set; }

        //public PostUpdate()
        //{
        //    PostUpdateDate = DateTime.Now;
        //}
    }
}
