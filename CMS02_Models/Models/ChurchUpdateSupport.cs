﻿using System;

namespace CMS02_Models.Models
{
    public class ChurchUpdateSupport
    {
        public int ChurchUpdateSupportId { get; set; }

        public int ChurchUpdateId { get; set; }

        public int ChurchUserId { get; set; }

        public virtual ChurchUpdate ChurchUpdate { get; set; }

        public DateTime UpdateSupportedDate { get; set; }

    }
}
