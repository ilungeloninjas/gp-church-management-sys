﻿using System;

namespace CMS02_Models.Models
{
    public class ChurchInvitation
    {
        public int ChurchInvitationId { get; set; }

        public string FromUserId { get; set; }

        public int ChurchId { get; set; }

        public string ToUserId { get; set; }

        public DateTime SentDate { get; set; }

        public virtual Church Church { get; set; }

        public bool Accepted { get; set; }
    }
}
