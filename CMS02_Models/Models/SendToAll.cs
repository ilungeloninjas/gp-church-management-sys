﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS02_Models.Models
{
    public class SendToAll
    {
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
