﻿using System;
using System.Collections.Generic;

namespace CMS02_Models.Models
{
    public class Focus
    {
        public int FocusId { get; set; }

        public string FocusName { get; set; }

        public string Description { get; set; }

        public int ChurchId { get; set; }

        public virtual Church Church { get; set; }

        public DateTime CreatedDate { get; set; }

        public virtual ICollection<ChurchGoal> ChurchGoals { get; set; }

        public Focus()
        {
            CreatedDate = DateTime.Now;
        }
    }
}
