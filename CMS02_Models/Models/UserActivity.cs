﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS02_Models.Models
{
  public  class UserActivity
    {
        public int UserId { get; set; }

        public string Last_Activity { get; set; }
        public virtual ICollection<ApplicationUser> AppUser{ get; set; }
    }
}
