﻿using System.Collections.Generic;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface IGoalStatusBusiness
    {
        IEnumerable<GoalStatus> GetGoalStatus();
        GoalStatus GetGoalStatus(int id);
        void CreateGoalStatus(GoalStatus goalStatus);
        void DeleteGoalStatus(int id);
        void SaveGoalStatus();
    }

    public class GoalStatusBusiness : IGoalStatusBusiness
    {
        private readonly IGoalStatusRepository _goalStatusRepository;
        private readonly IUnitOfWork _unitOfWork;

        public GoalStatusBusiness(IGoalStatusRepository goalStatusRepository, IUnitOfWork unitOfWork)
        {
            _goalStatusRepository = goalStatusRepository;
            _unitOfWork = unitOfWork;
        }

        #region IGoalStatusBusiness Members

        public IEnumerable<GoalStatus> GetGoalStatus()
        {
            var goalStatus = _goalStatusRepository.GetAll();
            return goalStatus;
        }

        public GoalStatus GetGoalStatus(int id)
        {
            var goalStatus = _goalStatusRepository.GetById(id);
            return goalStatus;
        }

        public void CreateGoalStatus(GoalStatus goalStatus)
        {
            _goalStatusRepository.Add(goalStatus);
            SaveGoalStatus();
        }

        public void DeleteGoalStatus(int id)
        {
            var goalStatus = _goalStatusRepository.GetById(id);
            _goalStatusRepository.Delete(goalStatus);
            SaveGoalStatus();
        }

        public void SaveGoalStatus()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}
