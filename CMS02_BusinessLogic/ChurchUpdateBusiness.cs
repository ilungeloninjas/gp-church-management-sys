﻿using System.Collections.Generic;
using System.Linq;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface IChurchUpdateBusiness
    {
        IEnumerable<ChurchUpdate> GetUpdates();
        IEnumerable<ChurchUpdate> GetUpdatesByGoal(int goalid);
       // IEnumerable<GroupUpdate> GetUpdatesOfPublicGoals();
      //IEnumerable<GroupUpdate> GetUpdatesForaUser(int userid);
        IEnumerable<ChurchUpdate> GetTop20Updates(string userid, IChurchUserBusiness groupUserBusiness);
        IEnumerable<ChurchUpdate> GetUpdatesWithStatus(int goalid);
        ChurchUpdate GetLastUpdate(string userid);
        ChurchUpdate GetUpdate(int id);
        double Progress(int id);
        void CreateUpdate(ChurchUpdate update, string userId);
        void EditUpdate(ChurchUpdate update);
        void DeleteUpdate(int id);
        void SaveUpdate();
        //IEnumerable<Update> GetSupportGoalsUpdates(int UserId);
    }
    public class ChurchUpdateBusiness : IChurchUpdateBusiness
    {
        private readonly IChurchUpdateRepository _groupUpdateRepository;
        private readonly IChurchUpdateUserRepository _groupUpdateUserRepository;
        private readonly IChurchGoalRepository _groupGoalRepository;
        private readonly IUnitOfWork _unitOfWork;
        public ChurchUpdateBusiness(IChurchUpdateRepository updateRepository, IChurchUpdateUserRepository groupUpdateUserRepository, IChurchGoalRepository groupGoalRepository, IUnitOfWork unitOfWork)
        {
            _groupUpdateRepository = updateRepository;
            _groupUpdateUserRepository = groupUpdateUserRepository;
            _groupGoalRepository = groupGoalRepository;
            _unitOfWork = unitOfWork;
        }
        #region IChurchUpdateBusiness Members

        public IEnumerable<ChurchUpdate> GetUpdates()
        {
            var update = _groupUpdateRepository.GetAll();
            return update;
        }
        public ChurchUpdate GetLastUpdate(string userid)
        {
            var updates = _groupUpdateRepository.GetMany(g => g.ChurchGoal.ChurchUser.UserId == userid).Last();
            return updates;
        }
        //public IEnumerable<GroupUpdate> GetUpdatesForaUser(int userid)
        //{
        //    var updates = groupUpdateRepository.GetMany(u => (u.GroupGoal.UserId == userid && u.GroupGoal.GoalType == false)).OrderByDescending(u => u.UpdateDate).ToList(); ;
        //    return updates;
        //}

        public IEnumerable<ChurchUpdate> GetTop20Updates(string userid, IChurchUserBusiness groupUserBusiness)
        {
            var updates = from u in _groupUpdateRepository.GetAll() where (from g in groupUserBusiness.GetGroupUsers() where g.UserId == userid select g.ChurchId).ToList().Contains(u.ChurchGoal.ChurchUser.ChurchId) select u;
            return updates;
        }

        public IEnumerable<ChurchUpdate> GetUpdatesByGoal(int goalid)
        {
            var updates = _groupUpdateRepository.GetMany(u => u.ChurchGoalId == goalid).OrderByDescending(u => u.ChurchUpdateId).ToList();
            return updates;
        }
        public IEnumerable<ChurchUpdate> GetUpdatesWithStatus(int goalid)
        {
            var updates = _groupUpdateRepository.GetMany(u => (u.ChurchGoalId == goalid) && (u.Status != null)).OrderByDescending(u => u.ChurchUpdateId).ToList();
            return updates;
        }

        public ChurchUpdate GetUpdate(int id)
        {
            var update = _groupUpdateRepository.GetById(id);
            return update;
        }

        public void CreateUpdate(ChurchUpdate update, string userId)
        {
            _groupUpdateRepository.Add(update);
            SaveUpdate();
            var groupUpdateUser = new ChurchUpdateUser { UserId = userId, ChurchUpdateId = update.ChurchUpdateId };
            _groupUpdateUserRepository.Add(groupUpdateUser);
            SaveUpdate();
        }

        public double Progress(int id)
        {
            var status = _groupUpdateRepository.GetById(id).Status;
            var target = _groupGoalRepository.GetById(_groupUpdateRepository.GetById(id).ChurchGoalId).Target;
            var progress = (status / target) * 100;
            return (double)progress;

        }
        public void EditUpdate(ChurchUpdate update)
        {
            _groupUpdateRepository.Update(update);
            SaveUpdate();
        }

        public void DeleteUpdate(int id)
        {
            var update = _groupUpdateRepository.GetById(id);
            _groupUpdateRepository.Delete(update);
            _groupUpdateUserRepository.Delete(gu => gu.ChurchUpdateId == id);
            SaveUpdate();
        }

        public void SaveUpdate()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}


