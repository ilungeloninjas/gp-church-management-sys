﻿using System.Collections.Generic;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface IMetricBusiness
    {
        IEnumerable<Metric> GetMetrics();
        Metric GetMetric(int id);
        void CreateMetric(Metric metric);
        void DeleteMetric(int id);
        void SaveMetric();
    }
  
    public class MetricBusiness : IMetricBusiness
    {
        private readonly IMetricRepository _metricRepository;
        private readonly IUnitOfWork _unitOfWork;
      
        public MetricBusiness(IMetricRepository metricRepository, IUnitOfWork unitOfWork)
        {
            _metricRepository = metricRepository;
            _unitOfWork = unitOfWork;
        }
     
        #region IMetricBusiness Members

        public IEnumerable<Metric> GetMetrics()
        {
            var metric = _metricRepository.GetAll();
            return metric;
        }

        public Metric GetMetric(int id)
        {
            var metric = _metricRepository.GetById(id);
            return metric;
        }

        public void CreateMetric(Metric metric)
        {
            _metricRepository.Add(metric);
            SaveMetric();
        }

        public void DeleteMetric(int id)
        {
            var metric = _metricRepository.GetById(id);
            _metricRepository.Delete(metric);
            SaveMetric();
        }

        public void SaveMetric()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}
