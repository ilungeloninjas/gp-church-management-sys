﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using CMS02_Data;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace CMS02_BusinessLogic
{
    public interface IAttendEventBusiness
    {
        int CountAttendees(int id);
        IEnumerable<AttendEvent> GetAttendanceOfEvent(int attendanceId);
        IEnumerable<AttendEvent> GetEventAttendees();
        AttendEvent GetAttendee(string userid);
        void CreateAttend(AttendEvent attendEvent);
        void EditEventAttendees(AttendEvent attendEventToEdit);
        void SaveAttendEvent();
        FileStreamResult PrintPdf(int id);
    }

    public class AttendEventBusiness : IAttendEventBusiness
    {
        private readonly IAttendEventRepository _attendEventRepository;
        private readonly IChurchGoalRepository _churchGoalRepository;
        private readonly IUnitOfWork _unitOfWork;

        public AttendEventBusiness(IAttendEventRepository attendEventRepository, IChurchGoalRepository churchGoalRepository, IUnitOfWork unitOfWork)
        {
            _attendEventRepository = attendEventRepository;
            _churchGoalRepository = churchGoalRepository;
            _unitOfWork = unitOfWork;
        }

        public int CountAttendees(int id)
        {
            return _attendEventRepository.GetAll().Count(a => a.ChurchGoalId == id);
        }

        public IEnumerable<AttendEvent> GetAttendanceOfEvent(int attendanceId)
        {
            var users = from u in _attendEventRepository.GetAll()
                        join gu in
                            (from g in _churchGoalRepository.GetMany(gr => gr.ChurchGoalId == attendanceId) select g) on u.ChurchGoalId equals gu.ChurchGoalId
                        select u;
            return users;
        }

        public IEnumerable<AttendEvent> GetEventAttendees()
        {
            var attendees = _attendEventRepository.GetAll();
            return attendees;
        }

        public AttendEvent GetAttendee(string userid)
        {
            var attendee = _attendEventRepository.Get(g => g.UserId == userid);
            return attendee;
        }
        public void CreateAttend(AttendEvent attendEvent)
        {
            _attendEventRepository.Add(attendEvent);
            SaveAttendEvent();
        }
        public void EditEventAttendees(AttendEvent attendEventToEdit)
        {
            _attendEventRepository.Update(attendEventToEdit);
            SaveAttendEvent();
        }
        public FileStreamResult PrintPdf(int id)
        {


            // Set up the document and the MS to write it to and create the PDF writer instance
            var ms = new MemoryStream();
            var document = new Document(PageSize.A4, 0, 0, 0, 0);
            PdfWriter.GetInstance(document, ms);
            //var obj = new simphiwe();
            var obj = new GodProsperityContext();

            // Open the PDF document
            document.Open();

            // Set up fonts used in the document
            Font fontHeading3 = FontFactory.GetFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD, BaseColor.RED);
            FontFactory.GetFont(fontname: FontFactory.TIMES_ROMAN, size: 9, color: BaseColor.BLUE);

            // Create the heading paragraph with the headig font
            new Paragraph("Gods Prosperity", fontHeading3);
            new Paragraph("**********************Event Details***********************");

            // Add image to pdf 
            //String path = HttpContext.Current.Server.MapPath("~/img/blog-large.jpg");
            //Image gif = Image.GetInstance(path);
            //document.Add(gif);
            // Create the heading paragraph with the headig font
            var assets = from x in obj.AttendEvents
                         where x.AttendEventId == (id)
                         select x;

            foreach (var q in assets)
            {
                //paragraph;
                // Add a horizontal line below the headig text and add it to the paragraph
                iTextSharp.text.pdf.draw.VerticalPositionMark seperator = new iTextSharp.text.pdf.draw.LineSeparator();
                seperator.Offset = -6f;

                var table1 = new PdfPTable(1);
                var table = new PdfPTable(1);
                var table3 = new PdfPTable(1);
                var table7 = new PdfPTable(1);

                table.WidthPercentage = 80;
                table3.SetWidths(new float[] { 100 });
                table3.WidthPercentage = 80;
                table7.SetWidths(new float[] { 100 });
                table7.WidthPercentage = 80;
                var cell = new PdfPCell(new Phrase(""));
                cell.Colspan = 3;
                table1.AddCell(cell);
                table7.AddCell(
                    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" +
                    "Gods Prosperity" + "\n\n" +
                    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" +
                    "**********************Attendee Details***********************");



                table.AddCell("FirstName: " + q.FirstName);
                table.AddCell("LastName   : " + q.LastName);
                table.AddCell("Phone Number: " + q.Phone);

                table.AddCell("Email Address : " + q.Email);




                table.AddCell("Date Issued : " + DateTime.Today.Day + "/" + DateTime.Today.Month + "/" +
                              DateTime.Today.Year);
                table.AddCell(cell);
                document.Add(table3);
                document.Add(table7);
                document.Add(table1);
                document.Add(table);
                document.Close();
            }

            byte[] file = ms.ToArray();
            var output = new MemoryStream();
            output.Write(file, 0, file.Length);
            output.Position = 0;
            var f = new FileStreamResult(output, "application/pdf");
            return f; //File(output, "application/pdf"); //new FileStreamResult(output, "application/pdf");
        }
        public void SaveAttendEvent()
        {
            _unitOfWork.Commit();
        }
    }
}


