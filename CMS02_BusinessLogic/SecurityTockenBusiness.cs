﻿using System;
using System.Collections.Generic;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface ISecurityTokenBusiness
    {
        IEnumerable<SecurityToken> GetSecurityTokens();
        Guid GetSecurityToken(Guid id);
        int GetActualId(Guid id);
        void CreateSecurityToken(SecurityToken securityToken);
        void DeleteSecurityToken(Guid id);
        void SaveSecurityToken();
    }

    public class SecurityTokenBusiness : ISecurityTokenBusiness
    {
        private readonly ISecurityTokenRepository _securityTokenRepository;
        private readonly IUnitOfWork _unitOfWork;

        public SecurityTokenBusiness(ISecurityTokenRepository securityTokenRepository, IUnitOfWork unitOfWork)
        {
            _securityTokenRepository = securityTokenRepository;
            _unitOfWork = unitOfWork;
        }

        #region ISecurityTokenBusiness Members

        public IEnumerable<SecurityToken> GetSecurityTokens()
        {
            var securityToken = _securityTokenRepository.GetAll();
            return securityToken;
        }

        public Guid GetSecurityToken(Guid id)
        {
            var securityToken = _securityTokenRepository.Get(s => s.Token == id).Token;
            if (securityToken != null)
            {
                return securityToken;
            }
            else
            {
                Guid newguid = Guid.NewGuid();
                return newguid;
            }
        }
        public int GetActualId(Guid id)
        {
            var actualId = _securityTokenRepository.Get(s => s.Token == id).ActualId;
            return actualId;
        }
        public void CreateSecurityToken(SecurityToken securityToken)
        {
            _securityTokenRepository.Add(securityToken);
            SaveSecurityToken();
        }

        public void DeleteSecurityToken(Guid id)
        {
            var securityToken = _securityTokenRepository.Get(s => s.Token == id);
            _securityTokenRepository.Delete(securityToken);
            SaveSecurityToken();
        }

        public void SaveSecurityToken()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}
