﻿using System.ComponentModel;

namespace CMS02_BusinessLogic
{
    public enum ChurchFilter
    {
        [Description("All")]
        All,

        [Description("My Churches")]
        MyGroups,

        [Description("My Followings Churches")]
        MyFollowingsGroups,

        [Description("My Attended Churches")]
        MyFollowedGroups
    }
}
