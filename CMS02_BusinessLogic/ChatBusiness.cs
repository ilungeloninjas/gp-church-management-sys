﻿using System.Collections.Generic;
using System.Linq;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface IChatBusiness
    {
        IEnumerable<Chat> GetChatsByUser(string userid, string sender);
        void CreateChat(Chat chat);
        void DeleteChat(int id);
        void DeleteHistory(string userid, string sender);
        void SaveChat();
    }

    public class ChatBusiness : IChatBusiness
    {
        private readonly IChatRepository _chatRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ChatBusiness(IChatRepository chatRepository, IUnitOfWork unitOfWork)
        {
            _chatRepository = chatRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Chat> GetChatsByUser(string userid, string sender)
        {
            var chats = from c in _chatRepository.GetMany(c => c.FromUserId == userid && c.ToUserId == sender || c.FromUserId == sender && c.ToUserId == userid).OrderByDescending(c => c.DateSent)
                           select c;

            return chats;
        }

        public void CreateChat(Chat chat)
        {
            _chatRepository.Add(chat);
            SaveChat();
        }

        public void DeleteChat(int id)
        {
            var chat = _chatRepository.GetById(id);
            _chatRepository.Delete(chat);
            SaveChat();
        }

        public void DeleteHistory(string userid, string sender)
        {
            var chats = _chatRepository.GetAll();
            foreach (var c in chats)
            {
                if (c.FromUserId == userid && c.ToUserId == sender || c.FromUserId == sender && c.ToUserId == userid)
                {
                    _chatRepository.Delete(c);
                }
            }
            SaveChat();
        }

        public void SaveChat()
        {
            _unitOfWork.Commit();
        }
    }
}