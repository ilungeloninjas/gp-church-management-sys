﻿using System.Collections.Generic;
using System.Linq;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{

    public interface IChurchUserBusiness
    {
        IEnumerable<ChurchUser> GetGroupUsers();
        IEnumerable<int> GetGroupUsers(string userid);
        IEnumerable<int> GetGroupUsers(IEnumerable<string> userid);
        IEnumerable<ChurchUser> GetTop20GroupsUsersForProfile(string userid);
        IEnumerable<ChurchUser> GetTop20GroupsUsers(string userid);
        bool CanInviteUser(string userId, int groupId);
        ChurchUser GetGroupUser(int id);
        ChurchUser GetGroupUserByuserId(string id);
        ChurchUser GetGroupUser(string userId, int groupId);
        void CreateGroupUserFromRequest(ChurchUser groupUser, IChurchRequestBusiness groupRequestBusiness);
        IEnumerable<int> GetFollowedGroups(string userid);
        //IEnumerable<Group> GetGroups(string email);
        IEnumerable<ChurchUser> GetGroupUsersByGroup(int groupId);
        IEnumerable<int> GetGroupAdminUsers(string userid);
        IEnumerable<ChurchUser> GetGroupUsersList(int groupId);
        IEnumerable<ChurchUser> GetGroupUsersListToAssign(int groupId, string currentUserId);
        IEnumerable<ApplicationUser> GetMembersOfGroup(int groupId);
        IEnumerable<string> GetUserIdByGroupUserId(IEnumerable<int> groupuserid);
        int GetGroupUsersCount(int groupId);
        string GetAdminId(int groupId);

        void CreateGroupUser(ChurchUser groupUser, IChurchInvitationBusiness groupInvitationBusiness);
        void DeleteGroupUserByGroupId(int groupid);
        void DeleteGroupUser(int id);
        void SaveGroupUser();
        IEnumerable<ApplicationUser> SearchUserForGroup(string searchString, int groupId, IUserBusiness userBusiness, IChurchInvitationBusiness groupInvitationBusiness);
    }

    public class ChurchUserBusiness : IChurchUserBusiness
    {
        private readonly IChurchUserRepository _groupUserRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ChurchUserBusiness(IChurchUserRepository groupUserRepository, IUserRepository userRepository, IUnitOfWork unitOfWork)
        {
            _groupUserRepository = groupUserRepository;
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        #region IChurchUserBusiness Members

        public IEnumerable<ChurchUser> GetGroupUsers()
        {
            var groupUser = _groupUserRepository.GetAll();
            return groupUser;
        }

        public ChurchUser GetGroupUser(string userId, int groupId)
        {
            return _groupUserRepository.Get(gu => gu.UserId == userId && gu.ChurchId == groupId);

        }

        public IEnumerable<int> GetGroupUsers(string userid)
        {
            List<int> groupids = new List<int> { };
            var groupUsers = _groupUserRepository.GetMany(g => g.UserId == userid).OrderByDescending(g => g.ChurchId).ToList();
            foreach (var item in groupUsers)
            {
                var ids = item.ChurchId;
                groupids.Add(ids);
            }

            return groupids;
        }

        public IEnumerable<int> GetGroupAdminUsers(string userid)
        {
            List<int> groupIds = new List<int> { };
            var groupUsers = _groupUserRepository.GetMany(g => (g.UserId == userid) && (g.Admin == true)).OrderByDescending(g => g.ChurchUserId).ToList();
            foreach (ChurchUser item in groupUsers)
            {
                var groupId = item.ChurchId;
                groupIds.Add(groupId);
            }
            return groupIds;
        }
        public IEnumerable<ChurchUser> GetTop20GroupsUsers(string userid)
        {
            var groupids = GetGroupUsers(userid);
            var userids = GetGroupMembers(groupids);

            //var users = groupUserRepository.GetAll().OrderByDescending(g => g.GroupUserId).Take(20).ToList();
            return userids;
        }

        public IEnumerable<ChurchUser> GetTop20GroupsUsersForProfile(string userid)
        {

            var users = _groupUserRepository.GetMany(g => g.UserId == userid).OrderByDescending(g => g.ChurchUserId).Take(20).ToList();

            return users;
        }

        public int GetGroupUsersCount(int groupId)
        {
            return _groupUserRepository.GetMany(g => g.ChurchId == groupId).Count();
        }

        public IEnumerable<ChurchUser> GetGroupUsersList(int groupId)
        {
            var groupUsers = _groupUserRepository.GetMany(g => g.ChurchId == groupId).OrderByDescending(g => g.ChurchUserId).ToList();
            return groupUsers;
        }

        public IEnumerable<ChurchUser> GetGroupUsersListToAssign(int groupId, string currentUserId)
        {
            var groupUsers = _groupUserRepository.GetMany(g => g.ChurchId == groupId && g.UserId != currentUserId).OrderByDescending(g => g.ChurchUserId).ToList();
            return groupUsers;
        }

        public IEnumerable<ApplicationUser> SearchUserForGroup(string searchString, int groupId, IUserBusiness userBusiness, IChurchInvitationBusiness groupInvitationBusiness)
        {
            var users = from u in userBusiness.GetUsers(searchString)
                        where !(from g in GetGroupUsersByGroup(groupId) select g.UserId).Contains(u.Id) &&
                        !(groupInvitationBusiness.IsUserInvited(groupId, u.Id))
                        select u;
            return users;
        }

        public IEnumerable<ChurchUser> GetGroupUsersByGroup(int groupId)
        {
            var groupUsers = _groupUserRepository.GetMany(g => g.ChurchId == groupId).OrderBy(g => g.UserId).ToList();
            return groupUsers;
        }


        public ChurchUser GetGroupUser(int id)
        {
            var groupUser = _groupUserRepository.GetById(id);
            return groupUser;
        }

        public ChurchUser GetGroupUserByuserId(string id)
        {
            var user = _groupUserRepository.Get(g => g.UserId == id);
            return user;
        }
        public IEnumerable<int> GetFollowedGroups(string userid)
        {
            List<int> groupIds = new List<int> { };
            var groupUsers = _groupUserRepository.GetMany(g => (g.UserId == userid) && (g.Admin == false)).OrderByDescending(g => g.ChurchUserId).ToList();
            foreach (ChurchUser item in groupUsers)
            {
                var groupId = item.ChurchId;
                groupIds.Add(groupId);
            }
            return groupIds;
        }

        public IEnumerable<ApplicationUser> GetMembersOfGroup(int groupId)
        {
            //return _userBusiness.GetUsers().Join(groupUserRepository.GetMany(g => g.GroupId == groupId),
            //     u => u.UserId,
            //     gu => gu.UserId,
            //     (u, gu) => u);
            var users = from u in _userRepository.GetAll()
                        join gu in
                            (from g in _groupUserRepository.GetMany(gr => gr.ChurchId == groupId) select g) on u.Id equals gu.UserId
                        select u;
            return users;
        }

        public void CreateGroupUser(ChurchUser groupUser, IChurchInvitationBusiness groupInvitationBusiness)
        {
            var oldUser = _groupUserRepository.GetMany(g => g.UserId == groupUser.UserId && g.ChurchId == groupUser.ChurchId);
            if (oldUser.Count() == 0)
            {
                _groupUserRepository.Add(groupUser);
                SaveGroupUser();
            }
            groupInvitationBusiness.AcceptInvitation(groupUser.ChurchId, groupUser.UserId);
        }

        public void CreateGroupUserFromRequest(ChurchUser groupUser, IChurchRequestBusiness groupRequestBusiness)
        {
            var oldUser = _groupUserRepository.GetMany(g => g.UserId == groupUser.UserId && g.ChurchId == groupUser.ChurchId);
            if (oldUser.Count() == 0)
            {
                _groupUserRepository.Add(groupUser);
                SaveGroupUser();
            }
            groupRequestBusiness.ApproveRequest(groupUser.ChurchId, groupUser.UserId);
        }

        public IEnumerable<int> GetGroupUsers(IEnumerable<string> userid)
        {
            List<int> groupsIds = new List<int> { };
            foreach (var item in userid)
            {
                var groupUsers = _groupUserRepository.GetMany(g => g.UserId == item);
                foreach (ChurchUser gruser in groupUsers)
                {
                    groupsIds.Add(gruser.ChurchId);
                }
                
            }
            return groupsIds;

        }

        public IEnumerable<ChurchUser> GetGroupMembers(IEnumerable<int> groupid)
        {

            List<ChurchUser> users = new List<ChurchUser> { };
            foreach (int item in groupid)
            {
                var groupUsers = _groupUserRepository.Get(g => g.ChurchId == item);
                users.Add(groupUsers);

            }
            return users;

        }

        public string GetAdminId(int groupId)
        {
            return _groupUserRepository.Get(g => g.ChurchId == groupId && g.Admin == true).UserId;
        }


        public bool CanInviteUser(string userId, int groupId)
        {
            var groupUser = _groupUserRepository.Get(g => g.ChurchId == groupId && g.UserId == userId);
            if (groupUser != null)
                return false;
            else
                return true;
        }


        public IEnumerable<string>GetUserIdByGroupUserId(IEnumerable<int> groupuserid)
        {
            List<string> users = new List<string> { };
            foreach (int item in groupuserid)
            {
                var groupUsers = _groupUserRepository.Get(g => g.ChurchUserId == item).UserId;
                users.Add(groupUsers);

            }
            return users;
        }
        public void DeleteGroupUser(int id)
        {
            var groupUser = _groupUserRepository.GetById(id);
            _groupUserRepository.Delete(groupUser);
            SaveGroupUser();
        }

        public void DeleteGroupUserByGroupId(int groupid)
        {
            var groupuser = GetGroupUsersByGroup(groupid);
            foreach (var item in groupuser)
            {
                DeleteGroupUser(item.ChurchUserId);
            }
        }
        public void SaveGroupUser()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}
