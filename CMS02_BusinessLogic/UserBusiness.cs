using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using CMS02_BusinessLogic.Properties;
using CMS02_Data;
using CMS02_Data.Infrastructure;
using CMS02_Models.Common;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{

    public interface IUserBusiness
    {
        ApplicationUser GetUser(string userId);
        IEnumerable<ApplicationUser> GetUsers();
        IEnumerable<ApplicationUser> GetUsers(string username);
        ApplicationUser GetUserProfile(string userid);
        ApplicationUser GetUsersByEmail(string email);
        IEnumerable<ApplicationUser> GetInvitedUsers(string username, int groupId, IChurchInvitationBusiness groupInvitationBusiness);
        IEnumerable<ApplicationUser> GetUserByUserId(IEnumerable<string> userid);
        IEnumerable<ApplicationUser> SearchUser(string searchString);
        void SendToAllMany(SendToAll model);
        IEnumerable<ValidationResult> CanAddUser(string email);      
        void UpdateUser(ApplicationUser user);
       void SaveUser();
       void EditUser(string id, string firstname, string lastname, string email);


       void SaveImageUrl(string userId, string imageUrl);
    }

    public class UserBusiness : IUserBusiness
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserBusiness(IUserRepository userRepository, IUnitOfWork unitOfWork,IUserProfileRepository userProfileRepository)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
            _userProfileRepository = userProfileRepository;
        }
        public ApplicationUser GetUserProfile(string userid)
        {
            var userprofile = _userRepository.Get(u=>u.Id==userid);
            return userprofile;
        }

        public void SendToAllMany(SendToAll model)
        {
            var allUsers = _userRepository.GetAll();

            foreach (var emails in allUsers)
            {
                //string[] allMails = emails.ApplicationUser.UserName.Split(new[] {";"}, StringSplitOptions.RemoveEmptyEntries);
                var all = emails.Email;


                var fromAddress = new MailAddress("rhudulu1@gmail.com", "Ongezwa");
                var toAddress = new MailAddress(all, "");
                const string fromPassword = "1993ongezwa";
                string msg = " ";
                string subject = model.Subject;
                string body = model.Body;


                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Timeout = 30000,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    };

                    using (var message = new MailMessage(fromAddress, toAddress){Subject = subject, Body = body})
                    {
                        smtp.Send(message);
                    }
                }
            }

        #region IUserBusiness Members
        public ApplicationUser GetUser(string userId)
        {
            return _userRepository.Get(u => u.Id == userId);
        }

        public IEnumerable<ApplicationUser> GetUsers()
        {
            var users = _userRepository.GetAll();
            return users;
        }
        public ApplicationUser GetUsersByEmail(string email)
        {
            var users = _userRepository.Get(u => u.Email.Contains(email));
            return users;

        }
        public IEnumerable<ApplicationUser> GetUsers(string username)
        {
            var users = _userRepository.GetMany(u => (u.FirstName + " " + u.LastName).Contains(username) || u.Email.Contains(username)).OrderBy(u => u.FirstName).ToList();

            return users;
        }
        public IEnumerable<ApplicationUser> GetInvitedUsers(string username, int groupId, IChurchInvitationBusiness groupInvitationBusiness)
        {
            return GetUsers(username).Join(groupInvitationBusiness.GetGroupInvitationsForGroup(groupId).Where(inv => inv.Accepted == false), u => u.Id, i => i.ToUserId, (u, i) => u);
        }

        public IEnumerable<ValidationResult> CanAddUser(string email)
        {

            var user = _userRepository.Get(u => u.Email == email);
            if (user != null)
            {
                yield return new ValidationResult("UserName", Resources.EmailExixts);


            }
        }

        



        public void SaveImageUrl(string userId, string imageUrl)
        {
            var user = GetUser(userId);
            user.ProfilePicUrl = imageUrl;
            UpdateUser(user);
        }
        public void EditUser(string id,string firstname, string lastname,string email)
        {
            var user = GetUser(id);
            user.FirstName = firstname;
            user.LastName = lastname;
            user.Email = email;
            UpdateUser(user);
        }
        public void UpdateUser(ApplicationUser user)
        {
            _userRepository.Update(user);
            SaveUser();
        }

        public IEnumerable<ApplicationUser> SearchUser(string searchString)
        {
            var users = _userRepository.GetMany(u=>u.UserName.Contains(searchString)|| u.FirstName.Contains(searchString) || u.LastName.Contains(searchString) || u.Email.Contains(searchString)).OrderBy(u=>u.DisplayName);
            return users;
        }

       public void SaveUser()
        {
            _unitOfWork.Commit();
        }
      
        public IEnumerable<ApplicationUser> GetUserByUserId(IEnumerable<string> userid)
        {
            List<ApplicationUser> users = new List<ApplicationUser> { };
            foreach (string item in userid)
            {
                var Users = _userRepository.GetById(item);
                users.Add(Users);

            }
            return users;
        }

        #endregion
    }
}
