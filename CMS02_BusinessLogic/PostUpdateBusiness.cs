﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface IPostUpdateBusiness
    {
        IEnumerable<PostUpdate> GetPostedUpdate();
        void CreateAttend(PostUpdate postUpdate, string userId);
        void SaveUpdatePost();
    }
    public class PostUpdateBusiness : IPostUpdateBusiness
    {
        private readonly IPostUpdateRepository _postUpdateRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IChurchUpdateUserRepository _groupUpdateUserRepository;
        public PostUpdateBusiness(IPostUpdateRepository postUpdateRepository, IUnitOfWork unitOfWork, IChurchUpdateUserRepository groupUpdateUserRepository)
        {
            _postUpdateRepository = postUpdateRepository;
            _unitOfWork = unitOfWork;
            groupUpdateUserRepository = _groupUpdateUserRepository;
        }
        public void CreateAttend(PostUpdate postUpdate, string userId)
        {
            var groupUpdateUser = new PostUpdate { UserId = userId};
            _postUpdateRepository.Add(groupUpdateUser);
            SaveUpdatePost();
        }

        public IEnumerable<PostUpdate> GetPostedUpdate()
        {
            var update = _postUpdateRepository.GetAll();
            return update;
        }
        public void SaveUpdatePost()
        {
            _unitOfWork.Commit();
        }
    }
}
