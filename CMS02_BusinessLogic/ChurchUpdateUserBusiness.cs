﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{

    public interface IGroupUpdateUserBusiness
    {

        
        void CreateGroupUpdateUser(string userId, int groupUpdateId);
        void DeleteGroupUpdateUser(string userId, int groupUpdateId);

        ApplicationUser GetGroupUpdateUser(int groupUpdateId);

        void DeleteGroupUpdateUser(int id);
        void SaveGroupUpdateUser();
       
    }

    public class ChurchUpdateUserBusiness : IGroupUpdateUserBusiness
    {
        private readonly IChurchUpdateUserRepository _groupUpdateUserRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ChurchUpdateUserBusiness(IChurchUpdateUserRepository groupUpdateUserRepository, IUnitOfWork unitOfWork, IUserRepository userRepository)
        {
            _groupUpdateUserRepository = groupUpdateUserRepository;
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;

        }

        #region IChurchUpdateUserBusiness Members

        public void DeleteGroupUpdateUser(string userId, int groupUpdateId)
        {
            var groupUpdateUser = _groupUpdateUserRepository.Get(cu => cu.UserId == userId && cu.ChurchUpdateId == groupUpdateId);
            _groupUpdateUserRepository.Delete(groupUpdateUser);
            SaveGroupUpdateUser();
        }

        public void DeleteGroupUpdateUser(int id)
        {
            var groupUpdateUser = _groupUpdateUserRepository.GetById(id);
            _groupUpdateUserRepository.Delete(groupUpdateUser);
            SaveGroupUpdateUser();
        }


        public void SaveGroupUpdateUser()
        {
            _unitOfWork.Commit();
        }

       
        public ApplicationUser GetGroupUpdateUser(int groupUpdateId)
        {
            var groupUpdateUserId = _groupUpdateUserRepository.Get(g => g.ChurchUpdateId == groupUpdateId).UserId;
            var user = _userRepository.Get(u => u.Id == groupUpdateUserId);
            return user;

        }


        public void CreateGroupUpdateUser(string userId, int groupUpdateId)
        {
            var groupUpdateUser = new ChurchUpdateUser { UserId = userId, ChurchUpdateId = groupUpdateId };
            _groupUpdateUserRepository.Add(groupUpdateUser);
            SaveGroupUpdateUser();
        }
        #endregion
    }
}
