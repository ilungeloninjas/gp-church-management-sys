﻿using System.Collections.Generic;
using System.Linq;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{

    public interface IChurchCommentBusiness
    {
        IEnumerable<ChurchComment> GetComments();
        IEnumerable<ChurchComment> GetCommentsByUpdate(int updateid);
       // IEnumerable<GroupComment> GetCommentsOfPublicGoals();
       // IEnumerable<GroupComment> GetCommentsByUser(int userid);
        IEnumerable<ChurchComment> GetTop20CommentsOfPublicGoals(string userid, IChurchUserBusiness groupUserBusiness);
        ChurchComment GetLastComment(string userid);
        ChurchComment GetComment(int id);
        ChurchComment GEtUpdateByCommentId(int id);
        int GetCommentcount(int id);

        void CreateComment(ChurchComment comment, string useId);
        void DeleteComment(int id);
        void SaveComment();
    }

    public class ChurchCommentBusiness : IChurchCommentBusiness
    {
        private readonly IChurchCommentRepository _groupCommentRepository;
        private readonly IChurchCommentUserRepository _groupCommentUserRepository;
        private readonly IChurchUpdateRepository _groupUdateRepository;
        private readonly IUnitOfWork _unitOfWork;


        public ChurchCommentBusiness(IChurchCommentRepository groupCommentRepository, IChurchCommentUserRepository groupCommentUserRepository, IChurchUpdateRepository groupUdateRepository, IUnitOfWork unitOfWork)
        {
            _groupCommentRepository = groupCommentRepository;
            _groupCommentUserRepository = groupCommentUserRepository;
            _groupUdateRepository = groupUdateRepository;
            _unitOfWork = unitOfWork;
        }

        #region IChurchCommentBusiness Members

        public IEnumerable<ChurchComment> GetComments()
        {
            var comment = _groupCommentRepository.GetAll();
            return comment;
        }
        //public IEnumerable<GroupComment> GetCommentsOfPublicGoals()
        //{
        //    var comment = groupCommentRepository.GetMany(c => c.Goal.GoalType == false).ToList();
        //    return comment;
        //}
        //public IEnumerable<GroupComment> GetCommentsByUser(int userid)
        //{
        //    var comment = groupCommentRepository.GetMany(c => (c.UserId == userid && c.Update.Goal.GoalType == false)).OrderByDescending(c => c.CommentDate).ToList();
        //    return comment;
        //}
        public IEnumerable<ChurchComment> GetTop20CommentsOfPublicGoals(string userid, IChurchUserBusiness groupUserBusiness)
        {
            var comment = from u in _groupCommentRepository.GetAll() where (from g in groupUserBusiness.GetGroupUsers() where g.UserId == userid select g.ChurchId).ToList().Contains(u.ChurchUpdate.ChurchGoal.ChurchUser.ChurchId) select u;
          
            return comment;
        }
        public ChurchComment GetLastComment(string userid)
        {
            //var comments = groupCommentRepository.GetMany(c => c.UserId == userid).Last();
            var comments = new ChurchComment();
            return comments;
        }

        public IEnumerable<ChurchComment> GetCommentsByUpdate(int updateid)
        {
            var comments = _groupCommentRepository.GetMany(c => c.ChurchUpdateId == updateid).ToList();
            return comments;
        }


        public ChurchComment GetComment(int id)
        {
            var comment = _groupCommentRepository.GetById(id);
            return comment;
        }

        public ChurchComment GEtUpdateByCommentId(int id)
        {
            var comment = _groupCommentRepository.Get(u => u.ChurchCommentId == id);
            return comment;
        }

        public void CreateComment(ChurchComment comment, string userId)
        {
            _groupCommentRepository.Add(comment);
            SaveComment();
            var groupCommentUser = new ChurchCommentUser { UserId = userId, ChurchCommentId = comment.ChurchCommentId };
            _groupCommentUserRepository.Add(groupCommentUser);
            SaveComment();
        }

        public int GetCommentcount(int id)
        {
            return _groupCommentRepository.GetMany(c => c.ChurchUpdateId == id).Count();
        }

        public void DeleteComment(int id)
        {
            var comment = _groupCommentRepository.GetById(id);
            _groupCommentRepository.Delete(comment);
            _groupCommentUserRepository.Delete(gc => gc.ChurchCommentId == id);
            SaveComment();
        }

        public void SaveComment()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}
