﻿using System.Web.Mvc;
using CMS02_BusinessLogic.Helpers;

namespace CMS02_BusinessLogic.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static UserHtmlHelper User(this HtmlHelper html)
        {
            return new UserHtmlHelper(html, new UrlHelper(html.ViewContext.RequestContext));
        }
    }
}