﻿using System.Web.Mvc;
using HtmlHelper = System.Web.Mvc.HtmlHelper;

namespace CMS02_BusinessLogic.Helpers
{
    public class BaseHtmlHelper
    {
        private readonly HtmlHelper _html;
        private readonly UrlHelper _url;

        public BaseHtmlHelper(HtmlHelper html, UrlHelper url)
        {
            _html = html;
            _url = url;
        }
        protected HtmlHelper Html { get { return _html; } }
        protected UrlHelper Url { get { return _url; } }
    }
} 