﻿using System;
using System.Security.Principal;
using System.Web.Security;

namespace CMS02_BusinessLogic.Models
{ 
    [Serializable]
    public class GpUser : IIdentity
    {
        public GpUser(){}
        public GpUser(string name, string displayName, string userId)
        {
            Name = name;
            DisplayName = displayName;
            UserId = userId;
        }
        public GpUser(string name, string displayName, string userId,string roleName)
        {
            Name = name;
            DisplayName = displayName;
            UserId = userId;
            RoleName = roleName;
        }
        public GpUser(string name, UserInfo userInfo)
            : this(name, userInfo.DisplayName, userInfo.UserId,userInfo.RoleName)
        {
            if (userInfo == null) throw new ArgumentNullException("userInfo");
            UserId = userInfo.UserId;
        }

        public GpUser(FormsAuthenticationTicket ticket)
            : this(ticket.Name, UserInfo.FromString(ticket.UserData))
        {
            if (ticket == null) throw new ArgumentNullException("ticket");
        }

        public string Name { get; private set; }

        public string AuthenticationType
        {
            get { return "GoalSetterForms"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }

        public string DisplayName { get; private set; }
        public string RoleName { get; private set; }
        public string UserId { get; private set; }
    }
}
