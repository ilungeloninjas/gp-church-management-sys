﻿using System;
using System.Collections.Generic;
using System.Linq;
using CMS02_BusinessLogic.Properties;
using CMS02_Data.Infrastructure;
using CMS02_Models.Common;
using CMS02_Models.Models;
using CMS02_Service.Repository;
using PagedList;

namespace CMS02_BusinessLogic
{
    public interface IChurchBusiness
    {
        IEnumerable<Church> GetGroups();
        IEnumerable<Church> GetGroups(IEnumerable<int> id);
        IEnumerable<Church> GetGroupsForUser(IEnumerable<int> groupIds);
        IEnumerable<Church> GetTop20Groups(IEnumerable<int> id);
        IEnumerable<Church> SearchGroup(string group);
        Church GetGroup(string groupname);
        Church GetGroup(int id);
        Church CreateGroup(Church group, string userId);
        void UpdateGroup(Church group);
        void DeleteGroup(int id);
        void SaveGroup();
        IEnumerable<ValidationResult> CanAddGroup(Church group);
        IPagedList<Church> GetGroups(string userId, ChurchFilter filter, Page page);
    }

    public class ChurchBusiness : IChurchBusiness
    {
        private readonly IChurchRepository _groupRepository;
        private readonly IFollowUserRepository _followUserrepository;
        private readonly IChurchUserRepository _groupUserrepository;
        private readonly IUnitOfWork _unitOfWork;

        public ChurchBusiness(IChurchRepository groupRepository, IFollowUserRepository followUserrepository, IChurchUserRepository groupUserrepository, IUnitOfWork unitOfWork)
        {
            _groupRepository = groupRepository;
            _groupUserrepository = groupUserrepository;
            _followUserrepository = followUserrepository;
            _unitOfWork = unitOfWork;
        }

        #region IGroupBusiness Members

        public IEnumerable<Church> GetGroups()
        {
            var groups = _groupRepository.GetAll().OrderByDescending(g => g.CreatedDate);
            return groups;
        }

        public IEnumerable<Church> GetGroups(IEnumerable<int> id)
        {
            List<Church> groups = new List<Church> { };
            foreach (int item in id)
            {
                var group = GetGroup(item);
                if(!groups.Contains(group))
                {
                groups.Add(group);
                }
            }
            return groups;

        }


        public IEnumerable<Church> GetGroupsForUser(IEnumerable<int> groupIds)
        {
            List<Church> groups = new List<Church> { };
            foreach(var item in groupIds)
            {
                var group=_groupRepository.GetById(item);
                groups.Add(group);
            }
            return groups;
        }

        public Church GetGroup(string groupname)
        {
            var group = _groupRepository.Get(g => g.ChurchName == groupname);
            return group;
        }


        public Church GetGroup(int id)
        {
            var group = _groupRepository.GetById(id);
            return group;
        }

        public IEnumerable<Church> GetTop20Groups(IEnumerable<int> id)
        {
            List<Church> groups = new List<Church> { };
            foreach (int item in id)
            {
                var group = GetGroup(item);
                groups.Add(group);
            }
            //var goals = groupRepository.GetAll().OrderByDescending(g => g.CreatedDate).Take(20).ToList();
            return groups.OrderByDescending(g => g.CreatedDate).Take(20).ToList();
        }

        public IEnumerable<Church> SearchGroup(string group)
        {
            return _groupRepository.GetMany(g => g.ChurchName.ToLower().Contains(group.ToLower())).OrderBy(g => g.ChurchName);
        }

        public Church CreateGroup(Church group, string userId)
        {
            _groupRepository.Add(group);
            SaveGroup();

            var groupUser = new ChurchUser { ChurchId = group.ChurchId, UserId = userId, Admin = true };
            try
            {
                _groupUserrepository.Add(groupUser);
                SaveGroup();
            }
            catch
            {
                _groupRepository.Delete(group);
                SaveGroup();
            }            
            return group;
        }

        public void UpdateGroup(Church group)
        {
            _groupRepository.Update(group);
            SaveGroup();
        }

        public void DeleteGroup(int id)
        {
            var group = _groupRepository.GetById(id);
            _groupRepository.Delete(group);
            _groupUserrepository.Delete(gu => gu.ChurchId == id);
            SaveGroup();
        }

        public IEnumerable<ValidationResult> CanAddGroup(Church newChurch)
        {
            Church group;
            if (newChurch.ChurchId == 0)
                group = _groupRepository.Get(g => g.ChurchName == newChurch.ChurchName);
            else
                group = _groupRepository.Get(g => g.ChurchName == newChurch.ChurchName && g.ChurchId != newChurch.ChurchId);
            if (group != null)
            {
                yield return new ValidationResult("ChurchName", Resources.ChurchExists);
            }
        }

        public IPagedList<Church> GetGroups(string userId, ChurchFilter filter, Page page)
        {
            switch (filter)
            {
                case ChurchFilter.All:
                {
                    return _groupRepository.GetPage(page, x => true, order => order.ChurchName);
                }
                case ChurchFilter.MyGroups:
                {
                    var groupsIds = _groupUserrepository.GetMany(gru => gru.UserId == userId && gru.Admin).Select(gru => gru.ChurchId);
                    return _groupRepository.GetPage(page, where => groupsIds.Contains(where.ChurchId), order => order.CreatedDate);
                }
                case ChurchFilter.MyFollowingsGroups:
                {
                    var userIds = _followUserrepository.GetMany(g => g.FromUserId == userId).Select(x => x.ToUserId);
                    var groupIds = from item in userIds from gruser in _groupUserrepository.GetMany(g => g.UserId == item) select gruser.ChurchId;
                    return _groupRepository.GetPage(page, where => groupIds.Contains(where.ChurchId), order => order.CreatedDate);
                }
                case ChurchFilter.MyFollowedGroups:
                {
                    var groupIds = _groupUserrepository.GetMany(g => (g.UserId == userId) && (g.Admin == false)).Select(item => item.ChurchId);
                    return _groupRepository.GetPage(page, where => groupIds.Contains(where.ChurchId), order => order.CreatedDate);
                }
                default:
                {
                    throw new ApplicationException("Filter not understood");
                }
            }
        }

        public void SaveGroup()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}
