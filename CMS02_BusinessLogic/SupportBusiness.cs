﻿using System.Collections.Generic;
using System.Linq;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface ISupportBusiness
    {
        IEnumerable<Support> GetSupports();
        IEnumerable<Support> GetSupports(IEnumerable<int> id);
        Support GetSupport(int id);
        IEnumerable<Support> GetSupporForGoal(int goalId);
        IEnumerable<Goal> GetUserSupportedGoals(string userid, IGoalBusiness goalBusiness);
        IEnumerable<Goal> GetUserSupportedGoalsByPopularity(string userid, IGoalBusiness goalBusiness);
        bool CanInviteUser(string userId, int goalId);
        IEnumerable<Support> GetTop20SupportsOfFollowings(string userId);
        IEnumerable<Support> GetTop20Support(string userid);
        void CreateSupport(Support support);
        void DeleteSupport(int id);
        bool IsGoalSupported(int goalid, string userid);
        void DeleteSupport(int goalid, string userid);
        void SaveSupport();
        IEnumerable<ApplicationUser> SearchUserToSupport(string searchString, int goalId, IUserBusiness userBusiness, ISupportInvitationBusiness supportInvitationBusiness, string userid);

        void CreateUserSupport(Support support, ISupportInvitationBusiness supportInvitationBusiness);

        IEnumerable<ApplicationUser> GetSupportersOfGoal(int id, IUserBusiness userBusiness);
    }

    public class SupportBusiness : ISupportBusiness
    {
        private readonly ISupportRepository _supportRepository;
        private readonly IFollowUserRepository _followUserRepository;
        private readonly IUnitOfWork _unitOfWork;

        public SupportBusiness(ISupportRepository supportRepository, IFollowUserRepository followUserRepository, IUnitOfWork unitOfWork)
        {
            _supportRepository = supportRepository;
            _followUserRepository = followUserRepository;
            _unitOfWork = unitOfWork;
        }

        #region ISupportBusiness Members

        public IEnumerable<Support> GetSupports()
        {
            var supports = _supportRepository.GetAll();
            return supports;
        }

        public IEnumerable<Support> GetSupporForGoal(int goalId)
        {
            return _supportRepository.GetMany(s => s.GoalId == goalId).OrderByDescending(s => s.SupportedDate);
        }

        public IEnumerable<Support> GetSupports(IEnumerable<int> id)
        {
            List<Support> supports = new List<Support> { };
            Support support;
            foreach (int item in id)
            {
                support = GetSupport(item);
                yield return support;
                //Supports.Add(Support);
            }
            // return Supports;

        }

        public IEnumerable<Support> GetTop20SupportsOfFollowings(string userId)
        {

            //var supports = SupportRepository.GetMany(s => s.Goal.GoalType == false).OrderByDescending(s => s.SupportedDate).Take(20).ToList();
            var supports = (from s in _supportRepository.GetMany(s => s.Goal.GoalType == false) where (from f in _followUserRepository.GetMany(fol => fol.FromUserId == userId) select f.ToUserId).ToList().Contains(s.UserId) select s).OrderByDescending(s => s.SupportedDate).Take(20);
            return supports;
        }
        public IEnumerable<Support> GetTop20Support(string userid)
        {

            var supports = _supportRepository.GetMany(s => (s.Goal.GoalType == false) && (s.UserId == userid)).OrderByDescending(s => s.SupportedDate).Take(20).ToList();
            return supports;
        }
        public void CreateUserSupport(Support support, ISupportInvitationBusiness supportInvitationBusiness)
        {
            var oldUser = _supportRepository.GetMany(g => g.UserId == support.UserId && g.SupportId == support.SupportId);
            if (oldUser.Count() == 0)
            {
                _supportRepository.Add(support);
                SaveSupport();
            }
            supportInvitationBusiness.AcceptInvitation(support.GoalId, support.UserId);
        }

        public Support GetSupport(int id)
        {
            var support = _supportRepository.GetById(id);
            return support;
        }

        public void CreateSupport(Support support)
        {
            _supportRepository.Add(support);
            SaveSupport();
        }

        public void DeleteSupport(int id)
        {
            var support = _supportRepository.GetById(id);
            _supportRepository.Delete(support);
            SaveSupport();
        }

        public IEnumerable<Goal> GetUserSupportedGoals(string userid, IGoalBusiness goalBusiness)
        {
            return GetSupports().Where(s => s.UserId == userid).Join(goalBusiness.GetGoals(), s => s.GoalId, g => g.GoalId, (s, g) => g).OrderByDescending(g => g.CreatedDate).ToList();
        }


        public IEnumerable<Goal> GetUserSupportedGoalsByPopularity(string userid, IGoalBusiness goalBusiness)
        {
            return GetSupports().Where(s => s.UserId == userid).Join(goalBusiness.GetGoals(), s => s.GoalId, g => g.GoalId, (s, g) => g).OrderByDescending(g => g.Supports.Count()).ToList();

        }
        public void DeleteSupport(int goalid, string userid)
        {
            int id = (from s in GetSupports() where s.GoalId == goalid && s.UserId == userid select s.SupportId).FirstOrDefault();
            if (id != 0) DeleteSupport(id);
        }

        public bool IsGoalSupported(int goalid, string userid)
        {
            return _supportRepository.Get(g => g.GoalId == goalid && g.UserId == userid) != null;
        }

        public IEnumerable<ApplicationUser> GetSupportersOfGoal(int id, IUserBusiness userBusiness)
        {
            return userBusiness.GetUsers().Join(_supportRepository.GetMany(g => g.GoalId == id),
                 u => u.Id,
                 s => s.UserId,
                 (u, s) => u);
        }

        public IEnumerable<ApplicationUser> SearchUserToSupport(string searchString, int goalId, IUserBusiness userBusiness, ISupportInvitationBusiness supportInvitationBusiness, string userId)
        {
            var users = from u in userBusiness.GetUsers(searchString)
                        where u.Id != userId && !(from g in GetSupporForGoal(goalId) select g.UserId).Contains(u.Id) &&
                        !(supportInvitationBusiness.IsUserInvited(goalId, u.Id))
                        select u;
            return users;
        }

        public bool CanInviteUser(string userId, int goalId)
        {
            var supportingUser = _supportRepository.Get(s => s.UserId == userId && s.GoalId == goalId);
            if (supportingUser != null)
                return false;
            else
                return true;
        }


        public void SaveSupport()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}
