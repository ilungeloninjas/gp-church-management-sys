﻿using System.Collections.Generic;
using System.Linq;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface IUpdateSupportBusiness
    {
        IEnumerable<UpdateSupport> GetSupports();
        //IEnumerable<Support> GetSupports(IEnumerable<int> id);
        UpdateSupport GetSupport(int id);
        IEnumerable<UpdateSupport> GetSupportForUpdate(int updateId);
        //IEnumerable<Goal> GetUserSupportedGoals(int userid, IGoalBusiness goalService);
        //IEnumerable<Goal> GetUserSupportedGoalsBYPopularity(int userid, IGoalBusiness goalService);
        //IEnumerable<ValidationResult> CanInviteUser(int userId, int goalId);
        //IEnumerable<Support> GetTop20SupportsOfFollowings(int userId);
       // IEnumerable<Support> GetTop20Support(int userid);
        int GetSupportcount(int id);
        void CreateSupport(UpdateSupport support);
        void DeleteSupport(int id);
        bool IsUpdateSupported(int updateid, string userid);
        void DeleteSupport(int updateid, string userid);
        void SaveSupport();
        //IEnumerable<User> SearchUserToSupport(string searchString, int goalId, IUserBusiness _userBusiness, ISupportInvitationBusiness supportInvitationService, int userid);

        void CreateUserSupport(UpdateSupport support);//, ISupportInvitationBusiness supportInvitationService);

        IEnumerable<ApplicationUser> GetSupportersOfUpdate(int id, IUserBusiness userBusiness);
    }

    public class UpdateSupportBusiness : IUpdateSupportBusiness
    {
        private readonly IUpdateSupportRepository _updateSupportRepository;
      //  private readonly IFollowUserRepository followUserRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateSupportBusiness(IUpdateSupportRepository updateSupportRepository, IUnitOfWork unitOfWork)
        {
            _updateSupportRepository = updateSupportRepository;
            //this.followUserRepository = followUserRepository;
            _unitOfWork = unitOfWork;
        }

        #region IUpdateSupportBusiness Members

        public IEnumerable<UpdateSupport> GetSupports()
        {
            var updateSupports = _updateSupportRepository.GetAll();
            return updateSupports;
        }

        public IEnumerable<UpdateSupport> GetSupportForUpdate(int updateId)
        {
            return _updateSupportRepository.GetMany(s => s.UpdateId == updateId).OrderByDescending(s => s.UpdateSupportedDate);
        }

        //public IEnumerable<Support> GetSupports(IEnumerable<int> id)
        //{
        //    List<Support> Supports = new List<Support> { };
        //    Support Support;
        //    foreach (int item in id)
        //    {
        //        Support = GetSupport(item);
        //        yield return Support;
        //        //Supports.Add(Support);
        //    }
        //    // return Supports;

        //}

        //public IEnumerable<Support> GetTop20SupportsOfFollowings(int userId)
        //{

        //    //var supports = SupportRepository.GetMany(s => s.Goal.GoalType == false).OrderByDescending(s => s.SupportedDate).Take(20).ToList();
        //    var supports = (from s in SupportRepository.GetMany(s => s.Goal.GoalType == false) where (from f in followUserRepository.GetMany(fol => fol.FromUserId == userId) select f.ToUserId).ToList().Contains(s.UserId) select s).OrderByDescending(s => s.SupportedDate).Take(20);
        //    return supports;
        //}
        //public IEnumerable<Support> GetTop20Support(int userid)
        //{

        //    var supports = SupportRepository.GetMany(s => (s.Goal.GoalType == false) && (s.UserId == userid)).OrderByDescending(s => s.SupportedDate).Take(20).ToList();
        //    return supports;
        //}
        public void CreateUserSupport(UpdateSupport support)//, ISupportInvitationBusiness supportInvitationService)
        {
            var oldUser = _updateSupportRepository.GetMany(g => g.UserId == support.UserId && g.UpdateSupportId == support.UpdateSupportId);
            if (oldUser.Count() == 0)
            {
                _updateSupportRepository.Add(support);
                SaveSupport();
            }
           // supportInvitationService.AcceptInvitation(support.GoalId, support.UserId);
        }

        public UpdateSupport GetSupport(int id)
        {
            var support = _updateSupportRepository.GetById(id);
            return support;
        }

        public void CreateSupport(UpdateSupport support)
        {
            _updateSupportRepository.Add(support);
            SaveSupport();
        }

        public void DeleteSupport(int id)
        {
            var support = _updateSupportRepository.GetById(id);
            _updateSupportRepository.Delete(support);
            SaveSupport();
        }

        //public IEnumerable<Goal> GetUserSupportedGoals(int userid, IGoalBusiness goalService)
        //{
        //    return GetSupports().Where(s => s.UserId == userid).Join(goalService.GetGoals(), s => s.GoalId, g => g.GoalId, (s, g) => g).OrderByDescending(g => g.CreatedDate).ToList();
        //}


        //public IEnumerable<Goal> GetUserSupportedGoalsBYPopularity(int userid, IGoalBusiness goalService)
        //{
        //    return GetSupports().Where(s => s.UserId == userid).Join(goalService.GetGoals(), s => s.GoalId, g => g.GoalId, (s, g) => g).OrderByDescending(g => g.Supports.Count()).ToList();

        //}
        public void DeleteSupport(int updateid, string userid)
        {
            var support = _updateSupportRepository.Get(f => (f.UpdateId == updateid && f.UserId == userid));
            _updateSupportRepository.Delete(support);
            SaveSupport();
            //int id = (from s in GetSupports() where s.UpdateId == updateid && s.UserId == userid select s.UpdateSupportId).FirstOrDefault();
            //if (id != 0) DeleteSupport(id);
        }

        public bool IsUpdateSupported(int updateid, string userid)
        {
            return _updateSupportRepository.Get(g => g.UpdateId == updateid && g.UserId == userid) != null;
        }

        public IEnumerable<ApplicationUser> GetSupportersOfUpdate(int id, IUserBusiness userBusiness)
        {
            return userBusiness.GetUsers().Join(_updateSupportRepository.GetMany(g => g.UpdateId == id),
                 u => u.Id,
                 s => s.UserId,
                 (u, s) => u);
        }

        //public IEnumerable<User> SearchUserToSupport(string searchString, int goalId, IUserBusiness _userBusiness, ISupportInvitationBusiness supportInvitationService, int userId)
        //{
        //    var users = from u in _userBusiness.GetUsers(searchString)
        //                where u.UserId != userId && !(from g in GetSupporForGoal(goalId) select g.UserId).Contains(u.UserId) &&
        //                !(supportInvitationService.IsUserInvited(goalId, u.UserId))
        //                select u;
        //    return users;
        //}

        //public IEnumerable<ValidationResult> CanInviteUser(int userId, int goalId)
        //{
        //    var supportingUser = SupportRepository.Get(s => s.UserId == userId && s.GoalId == goalId);
        //    if (supportingUser != null)
        //    {
        //        yield return new ValidationResult("", Resources.PersonSupporting);
        //    }
        //}
        public int GetSupportcount(int id)
        {
            return _updateSupportRepository.GetMany(c => c.UpdateId == id).Count();
        }

        public void SaveSupport()
        {
            _unitOfWork.Commit();
        }


        #endregion
    }
}
