﻿using System.Collections.Generic;
using CMS02_BusinessLogic.Properties;
using CMS02_Data.Infrastructure;
using CMS02_Models.Common;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface IFocusBusiness
    {
        IEnumerable<Focus> GetFocuss();
        IEnumerable<Focus> GetFocussOfGroup(int id);
        Focus GetFocus(int id);
        Focus GetFocus(string focusname);
        void CreateFocus(Focus focus);
        void DeleteFocus(int id);
        void SaveFocus();
        Focus GetGroup(int focusid);
        void UpdateFocus(Focus focus);
        IEnumerable<ValidationResult> CanAddFocus(Focus newFocus);
    }
   
    public class FocusBusiness : IFocusBusiness
    {
        private readonly IFocusRepository _focusRepository;
        private readonly IUnitOfWork _unitOfWork;
        public FocusBusiness(IFocusRepository focusRepository, IUnitOfWork unitOfWork)
        {
            _focusRepository = focusRepository;
            _unitOfWork = unitOfWork;
        }
        #region IFocusBusiness Members

        public IEnumerable<Focus> GetFocuss()
        {
            var focus = _focusRepository.GetAll();
            return focus;
        }

        public Focus GetFocus(int id)
        {
            var focus = _focusRepository.GetById(id);
            return focus;
        }

        public Focus GetFocus(string focusname)
        {
            var focus = _focusRepository.Get(f => f.FocusName == focusname);

            return focus;
        }

        public Focus GetGroup(int focusid)
        {
            var group = _focusRepository.Get(f => f.FocusId == focusid);
            return group;
        }
        public IEnumerable<Focus> GetFocussOfGroup(int id)
        {
            var focus = _focusRepository.GetMany(f =>f.ChurchId == id);
            return focus;
        }
        public void CreateFocus(Focus focus)
        {
            _focusRepository.Add(focus);
            SaveFocus();
        }

        public IEnumerable<ValidationResult> CanAddFocus(Focus newFocus)
        {
            Focus focus;
            if (newFocus.FocusId == 0)
            {
                focus = _focusRepository.Get(f => f.FocusName == newFocus.FocusName && f.ChurchId == newFocus.ChurchId);
            }
            else
            {
                focus = _focusRepository.Get(f => f.FocusName == newFocus.FocusName && f.ChurchId == newFocus.ChurchId && f.FocusId != newFocus.FocusId);
            }
            if (focus != null)
            {
                yield return new ValidationResult("FocusName", Resources.FocusExists);
            }
        }

        public void DeleteFocus(int id)
        {
            var focus = _focusRepository.GetById(id);
            _focusRepository.Delete(focus);
            SaveFocus();
        }
        public void UpdateFocus(Focus focus)
        {
            _focusRepository.Update(focus);
            //SaveFocus();
        }

        public void SaveFocus()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}
