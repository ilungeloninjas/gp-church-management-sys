﻿using System.Collections.Generic;
using System.Linq;
using CMS02_BusinessLogic.Properties;
using CMS02_Data.Infrastructure;
using CMS02_Models.Common;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface IChurchGoalBusiness
    {
        IEnumerable<ChurchGoal> GetGroupGoals();
        IEnumerable<ChurchGoal> GetGroupGoalsByFocus(int focusid);
        ChurchGoal GetGroupGoal(int id);
        void CreateGroupGoal(ChurchGoal groupGoal);
        IEnumerable<ChurchGoal> GetGroupGoals(int groupId);
        void DeleteGroupGoal(int id);
        void SaveGroupGoal();
        IEnumerable<ValidationResult> CanAddGoal(ChurchGoal goal, IChurchUpdateBusiness groupUpdateBusiness);
        void EditGroupGoal(ChurchGoal groupGoalToEdit);
        int GroupGoals(int groupId);
        IEnumerable<ChurchGoal> GetTop20GroupsGoals(string userid, IChurchUserBusiness groupUserBusiness);
        IEnumerable<ChurchGoal> GetAssignedGoalsToOthers(int userid);
        IEnumerable<ChurchGoal> GetAssignedGoalsToMe(int userid);
    }

    public class ChurchGoalBusiness : IChurchGoalBusiness
    {
        private readonly IChurchGoalRepository _groupGoalRepository;
        private readonly IUnitOfWork _unitOfWork;
        public ChurchGoalBusiness(IChurchGoalRepository groupGoalRepository, IUnitOfWork unitOfWork)
        {
            _groupGoalRepository = groupGoalRepository;
            _unitOfWork = unitOfWork;
        }
        #region IChurchGoalBusiness Members

        public IEnumerable<ChurchGoal> GetGroupGoals()
        {
            var groupGoal = _groupGoalRepository.GetAll();
            return groupGoal;
        }
        public IEnumerable<ChurchGoal> GetGroupGoalsByFocus(int focusid)
        {
            var goals = _groupGoalRepository.GetMany(g => g.FocusId == focusid);
            return goals;
        }

        public IEnumerable<ChurchGoal> GetGroupGoals(int groupId)
        {
            return _groupGoalRepository.GetMany(g => g.ChurchUser.ChurchId == groupId);
        }


        public ChurchGoal GetGroupGoal(int id)
        {
            var groupGoal = _groupGoalRepository.GetById(id);
            return groupGoal;
        }

        public IEnumerable<ChurchGoal> GetTop20GroupsGoals(string userid, IChurchUserBusiness groupUserBusiness)
        {
            var goals = from g in _groupGoalRepository.GetAll() where (from gu in groupUserBusiness.GetGroupUsers() where gu.UserId == userid select gu.ChurchId).ToList().Contains(g.ChurchUser.ChurchId) select g;
            return goals;
        }
        public void CreateGroupGoal(ChurchGoal groupGoal)
        {
            _groupGoalRepository.Add(groupGoal);
            SaveGroupGoal();
        }

        public void DeleteGroupGoal(int id)
        {
            var groupGoal = _groupGoalRepository.GetById(id);
            _groupGoalRepository.Delete(groupGoal);
            SaveGroupGoal();
        }

        public IEnumerable<ValidationResult> CanAddGoal(ChurchGoal newGoal, IChurchUpdateBusiness groupUpdateBusiness)
        {
            ChurchGoal goal;
            if (newGoal.ChurchGoalId == 0)
                goal = _groupGoalRepository.Get(g => (g.ChurchId == newGoal.ChurchId) && (g.GoalName == newGoal.GoalName));
            else
                goal = _groupGoalRepository.Get(g => (g.ChurchId == newGoal.ChurchId) && (g.GoalName == newGoal.GoalName) && g.ChurchGoalId != newGoal.ChurchGoalId);
            if (goal != null)
            {
                yield return new ValidationResult("GoalName", Resources.GoalExists);
            }
            if (newGoal.StartDate.Subtract(newGoal.EndDate).TotalSeconds > 0)
            {
                yield return new ValidationResult("EndDate", Resources.EndDate);
            }
            int flag = 0;
            int status = 0;
            if (newGoal.ChurchGoalId != 0)
            {
                var updates = groupUpdateBusiness.GetUpdatesByGoal(newGoal.ChurchGoalId).OrderByDescending(g => g.UpdateDate).ToList();

                if (updates.Count() > 0)
                {
                    if (updates[0].UpdateDate.Subtract(newGoal.EndDate).TotalSeconds > 0)
                    {
                        flag = 1;
                    }
                    if (newGoal.StartDate.Subtract(updates[0].UpdateDate).TotalSeconds > 0)
                    {
                        status = 1;
                    }
                    if (flag == 1)
                    {
                        
                        yield return new ValidationResult("EndDate", Resources.EndDateNotValid + " " + updates[0].UpdateDate.ToString("dd-MMM-yyyy"));
                    }
                    else if (status == 1)
                    {                        
                        yield return new ValidationResult("StartDate", Resources.StartDate + " " + updates[0].UpdateDate.ToString("dd-MMM-yyyy"));
                    }

                }
            }
        }

        public void EditGroupGoal(ChurchGoal groupGoalToEdit)
        {
            _groupGoalRepository.Update(groupGoalToEdit);
            SaveGroupGoal();
        }


        public int GroupGoals(int groupId)
        {
            return _groupGoalRepository.GetMany(g => g.ChurchUser.ChurchId == groupId).Count();
        }


        public IEnumerable<ChurchGoal> GetAssignedGoalsToOthers(int userid)
        {

            var goals = _groupGoalRepository.GetMany(f => ((f.ChurchUserId == userid) && (f.AssignedChurchUserId != null))).ToList();
            return goals;
        }

        public IEnumerable<ChurchGoal> GetAssignedGoalsToMe(int userid)
        {

            var goals = _groupGoalRepository.GetMany(f => f.AssignedChurchUserId == userid).ToList();
            return goals;
        }


        public void SaveGroupGoal()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}
