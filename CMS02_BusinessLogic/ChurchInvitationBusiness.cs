﻿using System.Collections.Generic;
using System.Linq;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface IChurchInvitationBusiness
    {
        IEnumerable<ChurchInvitation> GetGroupInvitations();
        ChurchInvitation GetGroupInvitation(int id);
        void CreateGroupInvitation(ChurchInvitation groupInvitation);
        void DeleteGroupInvitation(int id);
        void AcceptInvitation(int id, string userid);
        void SaveGroupInvitation();
        IEnumerable<ChurchInvitation> GetGroupInvitationsForUser(string userid);
        IEnumerable<ChurchInvitation> GetGroupInvitationsForGroup(int groupId);
        bool IsUserInvited(int groupId, string userId);
    }

    public class ChurchInvitationBusiness : IChurchInvitationBusiness
    {
        private readonly IChurchInvitationRepository _groupInvitationRepository;
        private readonly IUnitOfWork _unitOfWork;
        public ChurchInvitationBusiness(IChurchInvitationRepository groupInvitationRepository, IUnitOfWork unitOfWork)
        {
            _groupInvitationRepository = groupInvitationRepository;
            _unitOfWork = unitOfWork;
        }
        #region IChurchInvitationBusiness Members

        public IEnumerable<ChurchInvitation> GetGroupInvitations()
        {
            var groupInvitation = _groupInvitationRepository.GetAll();
            return groupInvitation;
        }

        public ChurchInvitation GetGroupInvitation(int id)
        {
            var groupInvitation = _groupInvitationRepository.GetById(id);
            return groupInvitation;
        }

        public void CreateGroupInvitation(ChurchInvitation groupInvitation)
        {
            var oldgroup = GetGroupInvitations().Where(g => g.ToUserId == groupInvitation.ToUserId && g.ChurchId == groupInvitation.ChurchId);
            if (oldgroup.Count() == 0)
            {
                _groupInvitationRepository.Add(groupInvitation);
                SaveGroupInvitation();
            }
        }

        public void DeleteGroupInvitation(int id)
        {
            var groupInvitation = _groupInvitationRepository.GetById(id);
            _groupInvitationRepository.Delete(groupInvitation);
            SaveGroupInvitation();
        }



        public void AcceptInvitation(int id, string userid)
        {
            var groupInvitation = _groupInvitationRepository.Get(g => (g.ChurchId == id && g.ToUserId == userid));
            if (groupInvitation != null)
            {
                _groupInvitationRepository.Delete(groupInvitation);
                //groupInvitation.Accepted = true;
                //GroupInvitationRepository.Update(groupInvitation);
                SaveGroupInvitation();
            }
        }
        public IEnumerable<ChurchInvitation> GetGroupInvitationsForGroup(int groupId)
        {
            return from g in GetGroupInvitations() where g.ChurchId == groupId select g;
        }

        public IEnumerable<ChurchInvitation> GetGroupInvitationsForUser(string userid)
        {
            return from g in GetGroupInvitations() where g.ToUserId == userid && g.Accepted == false select g;
        }

        public bool IsUserInvited(int groupId, string userId)
        {
            return _groupInvitationRepository.Get(g => g.ToUserId == userId && g.ChurchId == groupId) != null;
        }

        public void SaveGroupInvitation()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}
