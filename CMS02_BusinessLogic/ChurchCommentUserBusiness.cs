﻿using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{

    public interface IChurchCommentUserBusiness
    {


        void CreateGroupCommentUser(string userId, int groupCommentId);
        void DeleteGroupCommentUser(string userId, int groupCommentId);
        ChurchCommentUser GetCommentUser(int commentId);

        ApplicationUser GetGroupCommentUser(int groupCommentId);
        void DeleteGroupCommentUser(int id);
        void SaveGroupCommentUser();
       
    }

    public class ChurchCommentUserBusiness : IChurchCommentUserBusiness
    {
        private readonly IChurchCommentUserRepository _groupCommentUserRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ChurchCommentUserBusiness(IChurchCommentUserRepository groupCommentUserRepository, IUnitOfWork unitOfWork, IUserRepository userRepository)
        {
            _groupCommentUserRepository = groupCommentUserRepository;
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }


        #region IChurchCommentUserBusiness Members

        public void DeleteGroupCommentUser(string userId, int groupCommentId)
        {
            var groupCommentUser = _groupCommentUserRepository.Get(cu => cu.UserId == userId && cu.ChurchCommentId == groupCommentId);
            _groupCommentUserRepository.Delete(groupCommentUser);
            SaveGroupCommentUser();
        }

        public ChurchCommentUser GetCommentUser(int commentId)
        {
            return _groupCommentUserRepository.Get(gcu => gcu.ChurchCommentId == commentId);
        }

        public void DeleteGroupCommentUser(int id)
        {
            var groupCommentUser = _groupCommentUserRepository.GetById(id);
            _groupCommentUserRepository.Delete(groupCommentUser);
            SaveGroupCommentUser();
        }

       
        public void SaveGroupCommentUser()
        {
            _unitOfWork.Commit();
        }




        public void CreateGroupCommentUser(string userId, int groupCommentId)
        {
            var groupCommentUser = new ChurchCommentUser { UserId = userId, ChurchCommentId = groupCommentId };
            _groupCommentUserRepository.Add(groupCommentUser);
            SaveGroupCommentUser();
        }


        public ApplicationUser GetGroupCommentUser(int groupCommentId)
        {
            var groupCommentUserId = _groupCommentUserRepository.Get(g => g.ChurchCommentId == groupCommentId).UserId;
            return _userRepository.GetById(groupCommentUserId);
        }


        #endregion
    }
}
