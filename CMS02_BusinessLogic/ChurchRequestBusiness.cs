﻿using System.Collections.Generic;
using System.Linq;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface IChurchRequestBusiness
    {
        IEnumerable<ChurchRequest> GetGroupRequests();
        ChurchRequest GetGroupRequest(int id);
        IEnumerable<ChurchRequest> GetGroupRequests(int groupId);
        void CreateGroupRequest(ChurchRequest groupRequest);
        IEnumerable<ChurchRequest> GetGroupRequestsForGroup(int groupId);
        bool RequestSent(string userId, int groupId);
        void DeleteGroupRequest(string userId, int groupId);
        void ApproveRequest(int id, string userid);
        void DeleteGroupRequest(int id);
        void SaveGroupRequest();
    }

    public class ChurchRequestBusiness : IChurchRequestBusiness
    {
        private readonly IChurchRequestRepository _groupRequestRepository;
        private readonly IUnitOfWork _unitOfWork;
        public ChurchRequestBusiness(IChurchRequestRepository groupRequestRepository, IUnitOfWork unitOfWork)
        {
            _groupRequestRepository = groupRequestRepository;
            _unitOfWork = unitOfWork;
        }
        #region IChurchRequestBusiness Members

        public IEnumerable<ChurchRequest> GetGroupRequests()
        {
            var groupRequest = _groupRequestRepository.GetAll();
            return groupRequest;
        }

        public ChurchRequest GetGroupRequest(int id)
        {
            var groupRequest = _groupRequestRepository.GetById(id);
            return groupRequest;
        }

        public void CreateGroupRequest(ChurchRequest groupRequest)
        {
            var oldgroup = GetGroupRequests().Where(g => g.UserId == groupRequest.UserId && g.ChurchId == groupRequest.ChurchId);
            if (oldgroup.Count() == 0)
            {
                _groupRequestRepository.Add(groupRequest);
                SaveGroupRequest();
            }
        }

        public void DeleteGroupRequest(int id)
        {
            var groupRequest = _groupRequestRepository.GetById(id);
            _groupRequestRepository.Delete(groupRequest);
            SaveGroupRequest();
        }

        public bool RequestSent(string userId, int groupId)
        {
            var groupRequests = _groupRequestRepository.GetMany(g => g.UserId == userId && g.ChurchId == groupId && g.Accepted == false);
            if (groupRequests.Count() == 1)
            {
                return true;
            }
            else return false;

        }
        public IEnumerable<ChurchRequest> GetGroupRequests(int groupId)
        {
            var groupRequests = _groupRequestRepository.GetMany(g => g.ChurchId == groupId && g.Accepted == false);
            return groupRequests;
        }

        public void ApproveRequest(int id, string userid)
        {
            var groupRequest = _groupRequestRepository.Get(g => (g.ChurchId == id && g.UserId == userid));
            if (groupRequest != null)
            {
                _groupRequestRepository.Delete(groupRequest);
                //groupInvitation.Accepted = true;
                //GroupInvitationRepository.Update(groupInvitation);
                SaveGroupRequest();
            }
        }

        public void DeleteGroupRequest(string userId, int groupId)
        {
            var groupRequest = _groupRequestRepository.Get(g => g.UserId == userId && g.ChurchId == groupId);
            DeleteGroupRequest(groupRequest.ChurchRequestId);

        }
        //public void AcceptInvitation(int id, int userid)
        //{
        //    var groupInvitation = GroupInvitationRepository.Get(g => (g.GroupId == id && g.ToUserId == userid));
        //    if (groupInvitation != null)
        //    {
        //        GroupInvitationRepository.Delete(groupInvitation);
        //        //groupInvitation.Accepted = true;
        //        //GroupInvitationRepository.Update(groupInvitation);
        //        SaveGroupInvitation();
        //    }
        //}
        //public IEnumerable<GroupInvitation> GetGroupInvitationsForGroup(int groupId)
        //{
        //    return from g in GetGroupInvitations() where g.GroupId == groupId select g;
        //}

        public IEnumerable<ChurchRequest> GetGroupRequestsForGroup(int groupId)
        {
            return from g in GetGroupRequests() where g.ChurchId == groupId && g.Accepted == false select g;
        }

        //public bool IsUserInvited(int groupId, int userId)
        //{
        //    return GroupInvitationRepository.Get(g => g.ToUserId == userId && g.GroupId == groupId) != null;
        //}

        public void SaveGroupRequest()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}
