﻿using System.Collections.Generic;
using System.Linq;
using CMS02_Data.Infrastructure;
using CMS02_Models.Models;
using CMS02_Service.Repository;

namespace CMS02_BusinessLogic
{
    public interface IChurchUpdateSupportBusiness
    {
        IEnumerable<ChurchUpdateSupport> GetSupports();
        ChurchUpdateSupport GetSupport(int id);
        IEnumerable<ChurchUpdateSupport> GetSupportForUpdate(int updateId);
        int GetSupportcount(int id);
        void CreateSupport(ChurchUpdateSupport support);
        void DeleteSupport(int id);
        bool IsUpdateSupported(int updateid, string userid, IChurchUserBusiness groupUserBusiness);
        void DeleteSupport(int updateid, int userid);
        void SaveSupport();

        void CreateUserSupport(ChurchUpdateSupport support);

        IEnumerable<ApplicationUser> GetSupportersOfUpdate(int id, IUserBusiness userBusiness, IChurchUserBusiness groupUserBusiness);
    }

    public class ChurchUpdateSupportBusiness : IChurchUpdateSupportBusiness
    {
        private readonly IChurchUpdateSupportRepository _groupUpdateSupportRepository;
        //  private readonly IFollowUserRepository followUserRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ChurchUpdateSupportBusiness(IChurchUpdateSupportRepository groupUpdateSupportRepository, IUnitOfWork unitOfWork)
        {
            _groupUpdateSupportRepository = groupUpdateSupportRepository;
            //this.followUserRepository = followUserRepository;
            _unitOfWork = unitOfWork;
        }

        #region IChurchUpdateSupportBusiness Members

        public IEnumerable<ChurchUpdateSupport> GetSupports()
        {
            var updateSupports = _groupUpdateSupportRepository.GetAll();
            return updateSupports;
        }

        public IEnumerable<ChurchUpdateSupport> GetSupportForUpdate(int updateId)
        {
            return _groupUpdateSupportRepository.GetMany(s => s.ChurchUpdateId == updateId).OrderByDescending(s => s.UpdateSupportedDate);
        }

        //public IEnumerable<Support> GetSupports(IEnumerable<int> id)
        //{
        //    List<Support> Supports = new List<Support> { };
        //    Support Support;
        //    foreach (int item in id)
        //    {
        //        Support = GetSupport(item);
        //        yield return Support;
        //        //Supports.Add(Support);
        //    }
        //    // return Supports;

        //}

        //public IEnumerable<Support> GetTop20SupportsOfFollowings(int userId)
        //{

        //    //var supports = SupportRepository.GetMany(s => s.Goal.GoalType == false).OrderByDescending(s => s.SupportedDate).Take(20).ToList();
        //    var supports = (from s in SupportRepository.GetMany(s => s.Goal.GoalType == false) where (from f in followUserRepository.GetMany(fol => fol.FromUserId == userId) select f.ToUserId).ToList().Contains(s.UserId) select s).OrderByDescending(s => s.SupportedDate).Take(20);
        //    return supports;
        //}
        //public IEnumerable<Support> GetTop20Support(int userid)
        //{

        //    var supports = SupportRepository.GetMany(s => (s.Goal.GoalType == false) && (s.UserId == userid)).OrderByDescending(s => s.SupportedDate).Take(20).ToList();
        //    return supports;
        //}
        public void CreateUserSupport(ChurchUpdateSupport support)//, ISupportInvitationBusiness supportInvitationService)
        {
            var oldUser = _groupUpdateSupportRepository.GetMany(g => g.ChurchUserId == support.ChurchUserId && g.ChurchUpdateSupportId == support.ChurchUpdateSupportId);
            if (oldUser.Count() == 0)
            {
                _groupUpdateSupportRepository.Add(support);
                SaveSupport();
            }
            // supportInvitationService.AcceptInvitation(support.GoalId, support.UserId);
        }

        public ChurchUpdateSupport GetSupport(int id)
        {
            var support = _groupUpdateSupportRepository.GetById(id);
            return support;
        }

        public void CreateSupport(ChurchUpdateSupport support)
        {
            _groupUpdateSupportRepository.Add(support);
            SaveSupport();
        }

        public void DeleteSupport(int id)
        {
            var support = _groupUpdateSupportRepository.GetById(id);
            _groupUpdateSupportRepository.Delete(support);
            SaveSupport();
        }

        //public IEnumerable<Goal> GetUserSupportedGoals(int userid, IGoalBusiness goalService)
        //{
        //    return GetSupports().Where(s => s.UserId == userid).Join(goalService.GetGoals(), s => s.GoalId, g => g.GoalId, (s, g) => g).OrderByDescending(g => g.CreatedDate).ToList();
        //}


        //public IEnumerable<Goal> GetUserSupportedGoalsBYPopularity(int userid, IGoalBusiness goalService)
        //{
        //    return GetSupports().Where(s => s.UserId == userid).Join(goalService.GetGoals(), s => s.GoalId, g => g.GoalId, (s, g) => g).OrderByDescending(g => g.Supports.Count()).ToList();

        //}
        public void DeleteSupport(int updateid, int userid)
        {
            var support = _groupUpdateSupportRepository.Get(f => (f.ChurchUpdateId == updateid && f.ChurchUserId == userid));
            _groupUpdateSupportRepository.Delete(support);
            SaveSupport();
            //int id = (from s in GetSupports() where s.UpdateId == updateid && s.UserId == userid select s.UpdateSupportId).FirstOrDefault();
            //if (id != 0) DeleteSupport(id);
        }

        public bool IsUpdateSupported(int updateid, string userid, IChurchUserBusiness groupUserBusiness)
        {
            var groupuserid = groupUserBusiness.GetGroupUserByuserId(userid).ChurchUserId;
            return _groupUpdateSupportRepository.Get(g => g.ChurchUpdateId == updateid && g.ChurchUserId == groupuserid) != null;
        }

        public IEnumerable<ApplicationUser> GetSupportersOfUpdate(int id, IUserBusiness userBusiness, IChurchUserBusiness groupUserBusiness)
        {

            List<int> users = new List<int> { };
            var supports = _groupUpdateSupportRepository.GetMany(f => f.ChurchUpdateId == id);
            foreach (var item in supports)
            {
                var user = item.ChurchUserId;
                users.Add(user);
            }
            var userids= groupUserBusiness.GetUserIdByGroupUserId(users);
            var userlist = userBusiness.GetUserByUserId(userids);
            return userlist;
               
        }

        //public IEnumerable<User> SearchUserToSupport(string searchString, int goalId, IUserBusiness _userBusiness, ISupportInvitationBusiness supportInvitationService, int userId)
        //{
        //    var users = from u in _userBusiness.GetUsers(searchString)
        //                where u.UserId != userId && !(from g in GetSupporForGoal(goalId) select g.UserId).Contains(u.UserId) &&
        //                !(supportInvitationService.IsUserInvited(goalId, u.UserId))
        //                select u;
        //    return users;
        //}

        //public IEnumerable<ValidationResult> CanInviteUser(int userId, int goalId)
        //{
        //    var supportingUser = SupportRepository.Get(s => s.UserId == userId && s.GoalId == goalId);
        //    if (supportingUser != null)
        //    {
        //        yield return new ValidationResult("", Resources.PersonSupporting);
        //    }
        //}
        public int GetSupportcount(int id)
        {
            return _groupUpdateSupportRepository.GetMany(c => c.ChurchUpdateId == id).Count();
        }

        public void SaveSupport()
        {
            _unitOfWork.Commit();
        }


        #endregion
    }
}
