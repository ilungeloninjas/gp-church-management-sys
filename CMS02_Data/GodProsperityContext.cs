﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using CMS02_Data.Configuration;
using CMS02_Data.Models;
using CMS02_Models.Models;
using Microsoft.AspNet.Identity.EntityFramework;

#pragma warning disable 618

namespace CMS02_Data
{
    public class GodProsperityContext : IdentityDbContext<ApplicationUser>
    {
        public new DbSet<ApplicationRole> Roles { get; set; }
        public GodProsperityContext()
            : base("GodContext")
        {
        }

        public DbSet<PostUpdate> PostUpdates { get; set; }
        public DbSet<AttendEvent> AttendEvents { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Focus> Focuses { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Update> Updates { get; set; }
        public DbSet<User_Activity> UserActivity { get; set; }
        public DbSet<Metric> Metrics { get; set; }
        public DbSet<Church> Churchs { get; set; }
        public DbSet<ChurchUser> ChurchUsers { get; set; }
        public DbSet<SecurityToken> SecurityTokens { get; set; }
        public DbSet<Support> Support { get; set; }
        public DbSet<ChurchInvitation> ChurchInvitation { get; set; }
        public DbSet<SupportInvitation> SupportInvitation { get; set; }
        public DbSet<ChurchGoal> ChurchGoal { get; set; }
        public DbSet<UserProfile> UserProfile { get; set; }
        public DbSet<GoalStatus> GoalStatus { get; set; }
        public DbSet<ChurchUpdate> ChurchUpdate { get; set; }
        public DbSet<ChurchComment> ChurchComment { get; set; }
        public DbSet<ChurchRequest> ChurchRequests { get; set; }
        public DbSet<FollowRequest> FollowRequest { get; set; }
        public DbSet<FollowUser> FollowUser { get; set; }
        public DbSet<ChurchUpdateUser> ChurchUpdateUsers { get; set; }
        public DbSet<ChurchCommentUser> ChurchCommentUsers { get; set; }
        public DbSet<CommentUser> CommentUsers { get; set; }
        public DbSet<UpdateSupport> UpdateSupports { get; set; }
        public DbSet<ChurchUpdateSupport> ChurchUpdateSupports { get; set; }

        public DbSet<Location> Locations { get; set; } 


        public virtual void Commit()
        {
            SaveChanges();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<IncludeMetadataConvention>();

            modelBuilder.Configurations.Add(new UserActivityConfiguration());
            modelBuilder.Configurations.Add(new CommentConfiguration());
            modelBuilder.Configurations.Add(new CommentUserConfiguration());
            modelBuilder.Configurations.Add(new FocusConfiguration());
            modelBuilder.Configurations.Add(new FollowRequestConfiguration());
            modelBuilder.Configurations.Add(new FollowUserConfiguration());
            modelBuilder.Configurations.Add(new GoalConfiguration());
            modelBuilder.Configurations.Add(new GoalStatusConfiguration());
            modelBuilder.Configurations.Add(new ChurchCommentConfiguration());
            modelBuilder.Configurations.Add(new ChurchCommentUserConfguration());
            modelBuilder.Configurations.Add(new ChurchConfiguration());
            modelBuilder.Configurations.Add(new ChurchGoalConfiguration());
            modelBuilder.Configurations.Add(new ChurchInvitationConfiguration());
            modelBuilder.Configurations.Add(new ChurchRequestConfiguration());
            modelBuilder.Configurations.Add(new ChurchUpdateSupportConfiguration());
            modelBuilder.Configurations.Add(new ChurchUpdateUserConfiguration());
            modelBuilder.Configurations.Add(new ChurchUserConfiguration());
            modelBuilder.Configurations.Add(new MetricConfiguration());
            modelBuilder.Configurations.Add(new RegistrationTokenConfiguration());
            modelBuilder.Configurations.Add(new SupportConfiguration());
            modelBuilder.Configurations.Add(new SupportInvitationConfiguration());
            modelBuilder.Configurations.Add(new UpdateConfiguration());
            modelBuilder.Configurations.Add(new UpdateSupportConfiguration());
            modelBuilder.Configurations.Add(new UserProfileConfiguration());
            modelBuilder.Configurations.Add(new AttendEventConfiguration());
            modelBuilder.Configurations.Add(new UpdatePostConfiguration());

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

        }
        public static GodProsperityContext Create()
        {
            return new GodProsperityContext();
        }
        
    }

}