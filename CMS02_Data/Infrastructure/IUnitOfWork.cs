﻿
namespace CMS02_Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}