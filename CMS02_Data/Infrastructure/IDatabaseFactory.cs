﻿using System;

namespace CMS02_Data.Infrastructure
{
    public interface IDatabaseFactory : IDisposable
    {
        GodProsperityContext Get();
    }
}
