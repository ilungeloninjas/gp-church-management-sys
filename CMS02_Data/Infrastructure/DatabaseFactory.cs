﻿namespace CMS02_Data.Infrastructure
{
public class DatabaseFactory : Disposable, IDatabaseFactory
{
    private GodProsperityContext _dataContext;
    public GodProsperityContext Get()
    {
        return _dataContext ?? (_dataContext = new GodProsperityContext());
    }
    protected override void DisposeCore()
    {
        if (_dataContext != null)
            _dataContext.Dispose();
    }
}
}
