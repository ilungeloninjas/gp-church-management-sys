﻿using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class ChurchUpdateUserConfiguration : EntityTypeConfiguration<ChurchUpdateUser>
    {
        public ChurchUpdateUserConfiguration()
        {
            Property(g => g.ChurchUpdateUserId).IsRequired();
            Property(g => g.UserId).IsRequired();
        }
    }
}
