﻿using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class MetricConfiguration:EntityTypeConfiguration<Metric>
    {
        public MetricConfiguration()
        {
            Property(m => m.Type).IsMaxLength();
        }
    }
}
