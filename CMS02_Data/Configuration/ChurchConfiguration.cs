﻿using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class ChurchConfiguration : EntityTypeConfiguration<Church>
    {
        public ChurchConfiguration()
        {
            Property(g => g.ChurchName).HasMaxLength(50);
            Property(g => g.ChurchLocation).HasMaxLength(100);
            Property(g => g.Address).HasMaxLength(50);
            Property(g => g.email).HasMaxLength(50);
            Property(g => g.Telephone).HasMaxLength(10);
            Property(g => g.CreatedDate).IsRequired();
            Property(g => g.Description).IsMaxLength();
        }
    }
}
