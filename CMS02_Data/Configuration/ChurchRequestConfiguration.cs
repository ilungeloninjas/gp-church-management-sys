﻿using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class ChurchRequestConfiguration : EntityTypeConfiguration<ChurchRequest>
    {
        public ChurchRequestConfiguration()
        {
            Property(g => g.ChurchId).IsRequired();
            Property(g => g.UserId).HasMaxLength(128);
            Property(g => g.Accepted).IsRequired();
        }
    }
}
