﻿using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class ChurchGoalConfiguration : EntityTypeConfiguration<ChurchGoal>
    {
        public ChurchGoalConfiguration()
        {
            Property(g => g.ChurchId).IsRequired();
            Property(g => g.GoalName).HasMaxLength(50);
            Property(g => g.Description).HasMaxLength(100);
            Property(g => g.StartDate).IsRequired();
            Property(g => g.EndDate).IsRequired();
            Property(g => g.GoalStatusId).IsRequired();
            Property(g => g.ChurchUserId).IsRequired();
        }
    }
}
