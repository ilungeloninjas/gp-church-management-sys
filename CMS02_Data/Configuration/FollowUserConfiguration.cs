﻿using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class FollowUserConfiguration: EntityTypeConfiguration<FollowUser>
    {
        public FollowUserConfiguration()
        {
            Property(f => f.Accepted).IsRequired();
            Property(f => f.AddedDate).IsRequired();
        }
    }
}
