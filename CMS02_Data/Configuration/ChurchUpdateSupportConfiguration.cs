﻿using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class ChurchUpdateSupportConfiguration : EntityTypeConfiguration<ChurchUpdateSupport>
    {
        public ChurchUpdateSupportConfiguration()
        {
            Property(g => g.ChurchUpdateId).IsRequired();
            Property(g => g.ChurchUserId).IsRequired();
            Property(g => g.UpdateSupportedDate).IsRequired();
        }
    }
}
