﻿using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class ChurchUserConfiguration : EntityTypeConfiguration<ChurchUser>
    {
        public ChurchUserConfiguration()
        {
            Property(g => g.ChurchId).IsRequired();
            Property(g => g.UserId).IsRequired().IsMaxLength();
            Property(g => g.Admin).IsRequired();
            Property(g => g.AddedDate).IsRequired();
        
        }
    }
}
