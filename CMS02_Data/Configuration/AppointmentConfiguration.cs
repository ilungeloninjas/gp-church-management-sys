﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class AppointmentConfiguration : EntityTypeConfiguration<Appointment>
    {
        public AppointmentConfiguration()
        {
            Property(a => a.id).IsRequired();
            Property(a => a.text).IsRequired();
            Property(a => a.start_date).IsRequired();
            Property(a => a.end_date).IsRequired();
            Property(a => a.MemberId).IsRequired();
            
        }
    }
}
