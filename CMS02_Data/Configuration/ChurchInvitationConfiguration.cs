﻿using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class ChurchInvitationConfiguration : EntityTypeConfiguration<ChurchInvitation>
    {
        public ChurchInvitationConfiguration()
        {
            Property(g => g.FromUserId).IsMaxLength();
            Property(g => g.ToUserId).IsMaxLength();
            Property(g => g.ChurchId).IsRequired();
            Property(g => g.SentDate).IsRequired();
            Property(g => g.Accepted).IsRequired();
        }
    }
}
