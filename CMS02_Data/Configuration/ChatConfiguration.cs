﻿using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class ChatConfiguration : EntityTypeConfiguration<Chat>
    {
        public ChatConfiguration()
       {
           Property(c => c.Message).HasMaxLength(250);
       }
    }
}