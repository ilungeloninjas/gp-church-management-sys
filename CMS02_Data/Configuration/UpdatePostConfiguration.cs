﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class UpdatePostConfiguration : EntityTypeConfiguration<PostUpdate>
    {
        public UpdatePostConfiguration()
        {
            Property(g => g.UpdateMessage).IsMaxLength();
          
        }
    }
}
