﻿using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class ChurchCommentUserConfguration : EntityTypeConfiguration<ChurchCommentUser>
    {
        public ChurchCommentUserConfguration()
        {
            Property(g => g.ChurchCommentId).IsRequired();
            Property(g => g.UserId).IsRequired();
        }
    }
}
