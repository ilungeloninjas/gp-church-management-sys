﻿using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class ChurchCommentConfiguration: EntityTypeConfiguration<ChurchComment>
    {
        public ChurchCommentConfiguration()
        {
            Property(g => g.ChurchUpdateId).IsRequired();
            Property(g => g.CommentText).IsMaxLength();
            Property(g => g.CommentDate).IsRequired();
        }
    }
}
