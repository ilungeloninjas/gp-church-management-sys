﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
    public class AttendEventConfiguration : EntityTypeConfiguration<AttendEvent>
    {
        public AttendEventConfiguration()
        {
            Property(g => g.FirstName).HasMaxLength(50);
            Property(g => g.LastName).HasMaxLength(50);
            Property(g => g.Email).IsRequired();
            Property(g => g.Phone).IsRequired();
            Property(g => g.CreatedDate);
            Property(g => g.ChurchGoalId);
        }
    }
}
