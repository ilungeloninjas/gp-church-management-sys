﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using CMS02_Models.Models;

namespace CMS02_Data.Configuration
{
   public class UserActivityConfiguration : EntityTypeConfiguration<User_Activity>
    {
       public UserActivityConfiguration()
       {
           Property(g => g.UserId);
           Property(g => g.LastActivity);
       }
      
    }
}
