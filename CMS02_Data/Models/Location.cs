
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;

namespace CMS02_Data.Models
{
    public class Location
    {
        public int Id { get; set; }
        public DbGeography GeoLocation { get; set; }  
        public string Address { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}